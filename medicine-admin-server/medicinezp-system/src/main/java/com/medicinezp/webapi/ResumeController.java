package com.medicinezp.webapi;

import com.medicinezp.baseconfig.domain.*;
import com.medicinezp.baseconfig.service.*;
import com.medicinezp.common.core.controller.BaseController;
import com.medicinezp.common.core.domain.AjaxResult;
import com.medicinezp.common.core.domain.entity.SysUser;
import com.medicinezp.common.utils.DateUtils;
import com.medicinezp.common.utils.SecurityUtils;
import com.medicinezp.system.service.ISysUserService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 发布简历接口
 *
 * @author waoqi
 * @date 2021-08-18
 */
@RestController
@RequestMapping("/api/resume")
public class ResumeController extends BaseController
{
    private static final Logger logger = LoggerFactory.getLogger(ResumeController.class);

    @Autowired
    private ISysUserService sysUserService;


    @Autowired
    private IBaseUserIntentionService baseUserIntentionService;

    @Autowired
    private IBaseUserResumeService baseUserResumeService;

    @Autowired
    private IBaseUserResumeExperienceService baseUserResumeExperienceService;

    @Autowired
    private IBaseUserResumeProjectService baseUserResumeProjectService;

    @Autowired
    private IBaseUserResumeCertificateService baseUserResumeCertificateService;

    @Autowired
    private IBaseUserResumeEducationService baseUserResumeEducationService;

    @Autowired
    private ISysUserService userService;

    @Autowired
    private IBaseSearchHistoryService baseSearchHistoryService;


    @ApiOperation("查询当前用户从业意向")
    @GetMapping("/getUserIntention")
    public AjaxResult getUserIntention() {
        Long userID = SecurityUtils.getLoginUser().getUser().getUserId();
        BaseUserIntention param=new BaseUserIntention();
        param.setUserId(userID);
        List<BaseUserIntention> intentList= baseUserIntentionService.selectBaseUserIntentionList(param);
        return AjaxResult.success(intentList);
    }

    @GetMapping("/getIntentionDetail")
    public AjaxResult getIntentionDetail(@RequestParam(value="id",required = true) Long id)
    {
        return AjaxResult.success(baseUserIntentionService.selectBaseUserIntentionById(id));
    }

    /**
     * 新增求职意向
     */
    @PostMapping("/addIntentionDetail")
    public AjaxResult addIntentionDetail(@RequestBody BaseUserIntention baseUserIntention)
    {
        Long userID = SecurityUtils.getLoginUser().getUser().getUserId();
        baseUserIntention.setUserId(userID);
        return toAjax(baseUserIntentionService.insertBaseUserIntention(baseUserIntention));
    }

    /**
     * 修改求职意向
     */
    @PostMapping("/editIntentionDetail")
    public AjaxResult editIntentionDetail(@RequestBody BaseUserIntention baseUserIntention)
    {
        return toAjax(baseUserIntentionService.updateBaseUserIntention(baseUserIntention));
    }

    /**
     * 删除求职意向
     */
    @GetMapping("/removeIntentionDetail")
    public AjaxResult removeIntentionDetail(@RequestParam(value="id",required = true) Long id)
    {
        return toAjax(baseUserIntentionService.deleteBaseUserIntentionById(id));
    }

    @GetMapping("/getUserResumeList")
    public AjaxResult getUserResumeList(BaseUserResume baseUserResume)
    {
        Long userID = SecurityUtils.getLoginUser().getUser().getUserId();
        baseUserResume.setUserId(userID);
        List<BaseUserResume> list = baseUserResumeService.selectBaseUserResumeList(baseUserResume);
        return AjaxResult.success(list);
    }

    /**
     * 获取求职简历详细信息
     */
    @GetMapping(value = "getUserResumeDetail")
    public AjaxResult getUserResumeDetail(@RequestParam(value="id",required = true) Long id)
    {
        BaseUserResume detail=baseUserResumeService.selectBaseUserResumeById(id);
        SysUser sysUser= userService.selectUserById(detail.getUserId());
        detail.setAvatar(sysUser.getAvatar());
        detail.setUserName(sysUser.getUserName());
        detail.setSex(sysUser.getSex());
        detail.setBirthDate(sysUser.getBirthDate());
        if(sysUser.getBirthDate()!=null){
           int age= DateUtils.differentDaysByMillisecond(sysUser.getBirthDate(),DateUtils.getNowDate())/365;
            detail.setAge(age);
        }

        BaseUserResumeExperience expParam=new BaseUserResumeExperience();
        expParam.setResumeId(Long.valueOf(detail.getId()));
        List<BaseUserResumeExperience>  experienceList=baseUserResumeExperienceService.selectBaseUserResumeExperienceList(expParam);
        detail.setExperienceList(experienceList);

        BaseUserResumeProject proParam=new BaseUserResumeProject();
        proParam.setResumeId(Long.valueOf(detail.getId()));
        List<BaseUserResumeProject> projectList= baseUserResumeProjectService.selectBaseUserResumeProjectList(proParam);
        detail.setProjectList(projectList);

        BaseUserResumeCertificate certParam=new BaseUserResumeCertificate();
        certParam.setResumeId(Long.valueOf(detail.getId()));
        List<BaseUserResumeCertificate> certList= baseUserResumeCertificateService.selectBaseUserResumeCertificateList(certParam);
        detail.setCertList(certList);

        BaseUserResumeEducation educationParam=new BaseUserResumeEducation();
        educationParam.setResumeId(Long.valueOf(detail.getId()));
        List<BaseUserResumeEducation>  educationList=baseUserResumeEducationService.selectBaseUserResumeEducationList(educationParam);
        detail.setEducationList(educationList);

        return AjaxResult.success(detail);
    }

    /**
     * 新增求职简历
     */
    @PostMapping("/addUserResume")
    public AjaxResult addUserResume(@RequestBody BaseUserResume baseUserResume)
    {
        Long userID = SecurityUtils.getLoginUser().getUser().getUserId();
        baseUserResume.setUserId(userID);
        return toAjax(baseUserResumeService.insertBaseUserResume(baseUserResume));
    }

    /**
     * 修改求职简历
     */
    @PostMapping("/editUserResume")
    public AjaxResult editUserResume(@RequestBody BaseUserResume baseUserResume)
    {
        return toAjax(baseUserResumeService.updateBaseUserResume(baseUserResume));
    }

    /**
     * 删除求职简历
     */
    @GetMapping("/removeUserResume")
    public AjaxResult removeUserResume(@RequestParam(value="id",required = true) Long id)
    {
        return toAjax(baseUserResumeService.deleteBaseUserResumeById(id));
    }

    /**
     * 设置默认求职简历
     */
    @GetMapping("/setUserResumeDefault")
    public AjaxResult setUserResumeDefault(@RequestParam(value="id",required = true) Long id)
    {
        Long userID = SecurityUtils.getLoginUser().getUser().getUserId();
        BaseUserResume baseUserResume=new BaseUserResume();
        baseUserResume.setUserId(userID);
        int result=baseUserResumeService.cancelBaseUserResumeDefault(baseUserResume);
        if(result>0){
            BaseUserResume currentResume= baseUserResumeService.selectBaseUserResumeById(id);
            currentResume.setDefaultFlag("Y");
            currentResume.setUpdateTime(DateUtils.getNowDate());
            result= baseUserResumeService.updateBaseUserResume(currentResume);
        }
        return toAjax(result);
    }

    /**
     * 查询教育经历
     * @param baseUserResumeExperience
     * @return
     */
    @GetMapping("/getUserExprienceList")
    public AjaxResult getUserExprienceList(BaseUserResumeExperience baseUserResumeExperience)
    {
        if(baseUserResumeExperience.getResumeId()==null){
            return AjaxResult.success(new ArrayList<>());
        }
        List<BaseUserResumeExperience> list = baseUserResumeExperienceService.selectBaseUserResumeExperienceList(baseUserResumeExperience);
        return AjaxResult.success(list);
    }


    /**
     * 获取工作经历详细信息
     */
    @GetMapping(value = "getUserExprienceDetail")
    public AjaxResult getUserExprienceDetail(@RequestParam(value="id",required = true) Long id)
    {
        return AjaxResult.success(baseUserResumeExperienceService.selectBaseUserResumeExperienceById(id));
    }

    /**
     * 新增工作经历
     */
    @PostMapping(value = "addUserExprience")
    public AjaxResult addUserExprience(@RequestBody BaseUserResumeExperience baseUserResumeExperience)
    {
        return toAjax(baseUserResumeExperienceService.insertBaseUserResumeExperience(baseUserResumeExperience));
    }

    /**
     * 修改工作经历
     */
    @PostMapping(value = "editUserExprience")
    public AjaxResult edit(@RequestBody BaseUserResumeExperience baseUserResumeExperience)
    {
        return toAjax(baseUserResumeExperienceService.updateBaseUserResumeExperience(baseUserResumeExperience));
    }

    /**
     * 删除工作经历
     */
    @GetMapping("/removeUserExprience")
    public AjaxResult remove(@RequestParam(value="id",required = true) Long id)
    {
        return toAjax(baseUserResumeExperienceService.deleteBaseUserResumeExperienceById(id));
    }

    /**
     * 查询简历项目经历
     * @param baseUserResumeProject
     * @return
     */
    @GetMapping("/getResumeProjectList")
    public AjaxResult getResumeProjectList(BaseUserResumeProject baseUserResumeProject)
    {
        if(baseUserResumeProject.getResumeId()==null){
            return AjaxResult.success(new ArrayList<>());
        }
        List<BaseUserResumeProject> list = baseUserResumeProjectService.selectBaseUserResumeProjectList(baseUserResumeProject);
        return AjaxResult.success(list);
    }


    /**
     * 获取项目经验详细信息
     */
    @GetMapping(value = "/getResumeProjectDetail")
    public AjaxResult getResumeProjectDetail(@RequestParam(value="id",required = true) Long id)
    {
        return AjaxResult.success(baseUserResumeProjectService.selectBaseUserResumeProjectById(id));
    }

    /**
     * 新增项目经验
     */
    @PostMapping(value = "/addResumeProject")
    public AjaxResult addResumeProjuect(@RequestBody BaseUserResumeProject baseUserResumeProject)
    {
        return toAjax(baseUserResumeProjectService.insertBaseUserResumeProject(baseUserResumeProject));
    }

    /**
     * 修改项目经验
     */
    @PostMapping(value = "/editResumeProject")
    public AjaxResult editResumeProject(@RequestBody BaseUserResumeProject baseUserResumeProject)
    {
        return toAjax(baseUserResumeProjectService.updateBaseUserResumeProject(baseUserResumeProject));
    }

    /**
     * 删除项目经验
     */
    @GetMapping("/removeResumeProject")
    public AjaxResult removeResumeProject(@RequestParam(value="id",required = true) Long id)
    {
        return toAjax(baseUserResumeProjectService.deleteBaseUserResumeProjectById(id));
    }

    @GetMapping("/getResumeCertList")
    public AjaxResult getResumeCertList(BaseUserResumeCertificate baseUserResumeCertificate)
    {
        if(baseUserResumeCertificate.getResumeId()==null){
            return AjaxResult.success(new ArrayList<>());
        }
        List<BaseUserResumeCertificate> list = baseUserResumeCertificateService.selectBaseUserResumeCertificateList(baseUserResumeCertificate);
        return AjaxResult.success(list);
    }


    /**
     * 获取职业证书详细信息
     */
    @GetMapping(value = "/getResumeCertDetail")
    public AjaxResult getResumeCertDetail(@RequestParam(value="id",required = true) Long id)
    {
        return AjaxResult.success(baseUserResumeCertificateService.selectBaseUserResumeCertificateById(id));
    }

    /**
     * 新增职业证书
     */
    @PostMapping(value = "/addResumeCertDetail")
    public AjaxResult addResumeCertDetail(@RequestBody BaseUserResumeCertificate baseUserResumeCertificate)
    {
        return toAjax(baseUserResumeCertificateService.insertBaseUserResumeCertificate(baseUserResumeCertificate));
    }

    /**
     * 修改职业证书
     */
    @PostMapping(value = "/editResumeCertDetail")
    public AjaxResult editResumeCertDetail(@RequestBody BaseUserResumeCertificate baseUserResumeCertificate)
    {
        return toAjax(baseUserResumeCertificateService.updateBaseUserResumeCertificate(baseUserResumeCertificate));
    }

    /**
     * 删除职业证书
     */
    @GetMapping("/removeResumeCertDetail")
    public AjaxResult removeResumeCertDetail(@RequestParam(value="id",required = true) Long id)
    {
        return toAjax(baseUserResumeCertificateService.deleteBaseUserResumeCertificateById(id));
    }

    @GetMapping("/getResumeEducationList")
    public AjaxResult getResumeEducationList(BaseUserResumeEducation baseUserResumeEducation)
    {
        if(baseUserResumeEducation.getResumeId()==null){
            return AjaxResult.success(new ArrayList<>());
        }
        List<BaseUserResumeEducation> list = baseUserResumeEducationService.selectBaseUserResumeEducationList(baseUserResumeEducation);
        return AjaxResult.success(list);
    }

    /**
     * 获取教育经历详细信息
     */
    @GetMapping(value = "/getResumeEducationDetail")
    public AjaxResult getResumeEducationDetail(@RequestParam(value="id",required = true) Long id)
    {
        return AjaxResult.success(baseUserResumeEducationService.selectBaseUserResumeEducationById(id));
    }

    /**
     * 新增教育经历
     */
    @PostMapping(value = "/addResumeEducationDetail")
    public AjaxResult addResumeEducationDetail(@RequestBody BaseUserResumeEducation baseUserResumeEducation)
    {
        return toAjax(baseUserResumeEducationService.insertBaseUserResumeEducation(baseUserResumeEducation));
    }

    /**
     * 修改教育经历
     */
    @PostMapping(value = "/editResumeEducationDetail")
    public AjaxResult edit(@RequestBody BaseUserResumeEducation baseUserResumeEducation)
    {
        return toAjax(baseUserResumeEducationService.updateBaseUserResumeEducation(baseUserResumeEducation));
    }

    /**
     * 删除教育经历
     */
    @GetMapping(value = "/removeResumeEducationDetail")
    public AjaxResult removeResumeEducationDetail(@RequestParam(value="id",required = true) Long id)
    {
        return toAjax(baseUserResumeEducationService.deleteBaseUserResumeEducationById(id));
    }

    /**
     * 上传附件简历
     * @param baseUserResume
     * @return
     */
    @PostMapping("/addUserResumeFile")
    public AjaxResult addUserResumeFile(@RequestBody BaseUserResume baseUserResume)
    {
        Long userID = SecurityUtils.getLoginUser().getUser().getUserId();
        baseUserResume.setUserId(userID);
        baseUserResume.setType(2);
        return toAjax(baseUserResumeService.insertBaseUserResume(baseUserResume));
    }

    @GetMapping("/addSearchHistory")
    public AjaxResult addSearchHistory(@RequestParam(value="name",required = true) String name)
    {
        Long userID = SecurityUtils.getLoginUser().getUser().getUserId();
        BaseSearchHistory history=new BaseSearchHistory();
        history.setName(name);
        history.setUserId(userID);
        List<BaseSearchHistory> list= baseSearchHistoryService.selectUserSearchHistoryList(history);
        if(list.size()==0){
            history.setSearchNum(1L);
            history.setCreateTime(DateUtils.getNowDate());
            baseSearchHistoryService.insertBaseSearchHistory(history);
        }else{
            BaseSearchHistory his=list.get(0);
            his.setSearchNum(his.getSearchNum()+1);
            baseSearchHistoryService.updateBaseSearchHistory(his);
        }

        return AjaxResult.success("操作成功");
    }

    @GetMapping("/getSearchHistory")
    public AjaxResult getSearchHistory()
    {
        Long userID = SecurityUtils.getLoginUser().getUser().getUserId();
        BaseSearchHistory history=new BaseSearchHistory();
        history.setUserId(userID);
        List<BaseSearchHistory> list= baseSearchHistoryService.selectBaseSearchHistoryList(history);
        AjaxResult result=AjaxResult.success();
        result.put("searchData",list);

        List<BaseSearchHistory> hotList= baseSearchHistoryService.selectHotSearchHistoryData();
        result.put("hotData",hotList);
        return result;
    }

    @PostMapping("/searchHistoryFilter")
    public AjaxResult searchHistoryFilter(@RequestBody BaseSearchHistory param)
    {
        BaseSearchHistory history=new BaseSearchHistory();
        history.setName(param.getName());
        List<BaseSearchHistory> list= baseSearchHistoryService.selectSearchHelpList(history);
        return AjaxResult.success(list);
    }

    @GetMapping("/clearSearchHistory")
    public AjaxResult clearSearchHistory()
    {
        Long userID = SecurityUtils.getLoginUser().getUser().getUserId();
        int result= baseSearchHistoryService.deleteBaseSearchHistoryByUserId(userID);
        return toAjax(result);
    }

}
