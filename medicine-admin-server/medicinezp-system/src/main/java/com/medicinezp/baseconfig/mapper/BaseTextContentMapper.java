package com.medicinezp.baseconfig.mapper;

import java.util.List;
import com.medicinezp.baseconfig.domain.BaseTextContent;

/**
 * 文案管理Mapper接口
 * 
 * @author medicinezp
 * @date 2022-09-22
 */
public interface BaseTextContentMapper 
{
    /**
     * 查询文案管理
     * 
     * @param id 文案管理主键
     * @return 文案管理
     */
    public BaseTextContent selectBaseTextContentById(Long id);

    /**
     * 查询文案管理列表
     * 
     * @param baseTextContent 文案管理
     * @return 文案管理集合
     */
    public List<BaseTextContent> selectBaseTextContentList(BaseTextContent baseTextContent);

    /**
     * 新增文案管理
     * 
     * @param baseTextContent 文案管理
     * @return 结果
     */
    public int insertBaseTextContent(BaseTextContent baseTextContent);

    /**
     * 修改文案管理
     * 
     * @param baseTextContent 文案管理
     * @return 结果
     */
    public int updateBaseTextContent(BaseTextContent baseTextContent);

    /**
     * 删除文案管理
     * 
     * @param id 文案管理主键
     * @return 结果
     */
    public int deleteBaseTextContentById(Long id);

    /**
     * 批量删除文案管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseTextContentByIds(Long[] ids);
}
