package com.medicinezp.baseconfig.service.impl;

import java.util.List;

import com.medicinezp.baseconfig.domain.BaseJobType;
import com.medicinezp.baseconfig.mapper.BaseJobTypeMapper;
import com.medicinezp.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.medicinezp.baseconfig.mapper.BaseJobCategoryMapper;
import com.medicinezp.baseconfig.domain.BaseJobCategory;
import com.medicinezp.baseconfig.service.IBaseJobCategoryService;

/**
 * 岗位类型Service业务层处理
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@Service
public class BaseJobCategoryServiceImpl implements IBaseJobCategoryService
{
    @Autowired
    private BaseJobCategoryMapper baseJobCategoryMapper;

    @Autowired
    private BaseJobTypeMapper baseJobTypeMapper;

    /**
     * 查询岗位类型
     *
     * @param categoryId 岗位类型主键
     * @return 岗位类型
     */
    @Override
    public BaseJobCategory selectBaseJobCategoryByCategoryId(Long categoryId)
    {
        return baseJobCategoryMapper.selectBaseJobCategoryByCategoryId(categoryId);
    }

    /**
     * 查询岗位类型列表
     *
     * @param baseJobCategory 岗位类型
     * @return 岗位类型
     */
    @Override
    public List<BaseJobCategory> selectBaseJobCategoryList(BaseJobCategory baseJobCategory)
    {
        return baseJobCategoryMapper.selectBaseJobCategoryList(baseJobCategory);
    }

    /**
     * 新增岗位类型
     *
     * @param baseJobCategory 岗位类型
     * @return 结果
     */
    @Override
    public int insertBaseJobCategory(BaseJobCategory baseJobCategory)
    {
        baseJobCategory.setCreateTime(DateUtils.getNowDate());
        return baseJobCategoryMapper.insertBaseJobCategory(baseJobCategory);
    }

    /**
     * 修改岗位类型
     *
     * @param baseJobCategory 岗位类型
     * @return 结果
     */
    @Override
    public int updateBaseJobCategory(BaseJobCategory baseJobCategory)
    {
        int result= baseJobCategoryMapper.updateBaseJobCategory(baseJobCategory);
        BaseJobType jobType=new BaseJobType();
        jobType.setCategoryId(baseJobCategory.getCategoryId());
        baseJobTypeMapper.updateBaseJobTypeCategory(jobType);
        return result;
    }

    /**
     * 批量删除岗位类型
     *
     * @param categoryIds 需要删除的岗位类型主键
     * @return 结果
     */
    @Override
    public int deleteBaseJobCategoryByCategoryIds(Long[] categoryIds)
    {
        return baseJobCategoryMapper.deleteBaseJobCategoryByCategoryIds(categoryIds);
    }

    /**
     * 删除岗位类型信息
     *
     * @param categoryId 岗位类型主键
     * @return 结果
     */
    @Override
    public int deleteBaseJobCategoryByCategoryId(Long categoryId)
    {
        return baseJobCategoryMapper.deleteBaseJobCategoryByCategoryId(categoryId);
    }
}
