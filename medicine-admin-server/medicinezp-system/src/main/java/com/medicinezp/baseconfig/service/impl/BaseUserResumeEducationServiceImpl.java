package com.medicinezp.baseconfig.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.medicinezp.baseconfig.mapper.BaseUserResumeEducationMapper;
import com.medicinezp.baseconfig.domain.BaseUserResumeEducation;
import com.medicinezp.baseconfig.service.IBaseUserResumeEducationService;

/**
 * 教育经历Service业务层处理
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@Service
public class BaseUserResumeEducationServiceImpl implements IBaseUserResumeEducationService
{
    @Autowired
    private BaseUserResumeEducationMapper baseUserResumeEducationMapper;

    /**
     * 查询教育经历
     *
     * @param id 教育经历主键
     * @return 教育经历
     */
    @Override
    public BaseUserResumeEducation selectBaseUserResumeEducationById(Long id)
    {
        return baseUserResumeEducationMapper.selectBaseUserResumeEducationById(id);
    }

    /**
     * 查询教育经历列表
     *
     * @param baseUserResumeEducation 教育经历
     * @return 教育经历
     */
    @Override
    public List<BaseUserResumeEducation> selectBaseUserResumeEducationList(BaseUserResumeEducation baseUserResumeEducation)
    {
        return baseUserResumeEducationMapper.selectBaseUserResumeEducationList(baseUserResumeEducation);
    }

    /**
     * 新增教育经历
     *
     * @param baseUserResumeEducation 教育经历
     * @return 结果
     */
    @Override
    public int insertBaseUserResumeEducation(BaseUserResumeEducation baseUserResumeEducation)
    {
        return baseUserResumeEducationMapper.insertBaseUserResumeEducation(baseUserResumeEducation);
    }

    /**
     * 修改教育经历
     *
     * @param baseUserResumeEducation 教育经历
     * @return 结果
     */
    @Override
    public int updateBaseUserResumeEducation(BaseUserResumeEducation baseUserResumeEducation)
    {
        return baseUserResumeEducationMapper.updateBaseUserResumeEducation(baseUserResumeEducation);
    }

    /**
     * 批量删除教育经历
     *
     * @param ids 需要删除的教育经历主键
     * @return 结果
     */
    @Override
    public int deleteBaseUserResumeEducationByIds(Long[] ids)
    {
        return baseUserResumeEducationMapper.deleteBaseUserResumeEducationByIds(ids);
    }

    /**
     * 删除教育经历信息
     *
     * @param id 教育经历主键
     * @return 结果
     */
    @Override
    public int deleteBaseUserResumeEducationById(Long id)
    {
        return baseUserResumeEducationMapper.deleteBaseUserResumeEducationById(id);
    }
}
