package com.medicinezp.baseconfig.domain;

import com.medicinezp.common.annotation.Excel;
import com.medicinezp.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 搜索记录对象 base_search_history
 *
 * @author medicinezp
 * @date 2022-10-24
 */
public class BaseSearchHistory extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 用户ID */
    @Excel(name = "用户ID")
    private Long userId;

    /** 关键字 */
    @Excel(name = "关键字")
    private String name;

    /** 搜索次数 */
    @Excel(name = "搜索次数")
    private Long searchNum;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
    public void setSearchNum(Long searchNum)
    {
        this.searchNum = searchNum;
    }

    public Long getSearchNum()
    {
        return searchNum;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("userId", getUserId())
                .append("name", getName())
                .append("searchNum", getSearchNum())
                .append("createTime", getCreateTime())
                .toString();
    }
}
