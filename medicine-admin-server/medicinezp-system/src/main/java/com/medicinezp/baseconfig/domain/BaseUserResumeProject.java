package com.medicinezp.baseconfig.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.medicinezp.common.annotation.Excel;
import com.medicinezp.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 项目经验对象 base_user_resume_project
 *
 * @author medicinezp
 * @date 2022-09-22
 */
public class BaseUserResumeProject extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 简历ID */
    @Excel(name = "简历ID")
    private Long resumeId;

    /** 公司名称 */
    @Excel(name = "公司名称")
    private String projectName;

    /** 开始时间 */
    @Excel(name = "开始时间")
    private String startDate;

    /** 结束时间 */
    @Excel(name = "结束时间")
    private String endDate;

    /** 项目描述 */
    @Excel(name = "项目描述")
    private String projectContent;

    /** 创建日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createDate;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setResumeId(Long resumeId)
    {
        this.resumeId = resumeId;
    }

    public Long getResumeId()
    {
        return resumeId;
    }
    public void setProjectName(String projectName)
    {
        this.projectName = projectName;
    }

    public String getProjectName()
    {
        return projectName;
    }
    public void setStartDate(String startDate)
    {
        this.startDate = startDate;
    }

    public String getStartDate()
    {
        return startDate;
    }
    public void setEndDate(String endDate)
    {
        this.endDate = endDate;
    }

    public String getEndDate()
    {
        return endDate;
    }
    public void setProjectContent(String projectContent)
    {
        this.projectContent = projectContent;
    }

    public String getProjectContent()
    {
        return projectContent;
    }
    public void setCreateDate(Date createDate)
    {
        this.createDate = createDate;
    }

    public Date getCreateDate()
    {
        return createDate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("resumeId", getResumeId())
            .append("projectName", getProjectName())
            .append("startDate", getStartDate())
            .append("endDate", getEndDate())
            .append("projectContent", getProjectContent())
            .append("createDate", getCreateDate())
            .toString();
    }
}
