package com.medicinezp.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.medicinezp.common.annotation.Log;
import com.medicinezp.common.core.controller.BaseController;
import com.medicinezp.common.core.domain.AjaxResult;
import com.medicinezp.common.enums.BusinessType;
import com.medicinezp.baseconfig.domain.BaseCompany;
import com.medicinezp.baseconfig.service.IBaseCompanyService;
import com.medicinezp.common.utils.poi.ExcelUtil;
import com.medicinezp.common.core.page.TableDataInfo;

/**
 * 待审核公司Controller
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@RestController
@RequestMapping("/baseconfig/company")
public class BaseCompanyController extends BaseController
{
    @Autowired
    private IBaseCompanyService baseCompanyService;

    /**
     * 查询待审核公司列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:company:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseCompany baseCompany)
    {
        startPage();
        List<BaseCompany> list = baseCompanyService.selectBaseCompanyList(baseCompany);
        return getDataTable(list);
    }

    /**
     * 导出待审核公司列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:company:export')")
    @Log(title = "待审核公司", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseCompany baseCompany)
    {
        List<BaseCompany> list = baseCompanyService.selectBaseCompanyList(baseCompany);
        ExcelUtil<BaseCompany> util = new ExcelUtil<BaseCompany>(BaseCompany.class);
        util.exportExcel(response, list, "待审核公司数据");
    }

    /**
     * 获取待审核公司详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:company:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(baseCompanyService.selectBaseCompanyById(id));
    }

    /**
     * 新增待审核公司
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:company:add')")
    @Log(title = "待审核公司", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseCompany baseCompany)
    {
        return toAjax(baseCompanyService.insertBaseCompany(baseCompany));
    }

    /**
     * 修改待审核公司
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:company:edit')")
    @Log(title = "待审核公司", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseCompany baseCompany)
    {
        return toAjax(baseCompanyService.updateBaseCompany(baseCompany));
    }

    /**
     * 删除待审核公司
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:company:remove')")
    @Log(title = "待审核公司", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseCompanyService.deleteBaseCompanyByIds(ids));
    }
}
