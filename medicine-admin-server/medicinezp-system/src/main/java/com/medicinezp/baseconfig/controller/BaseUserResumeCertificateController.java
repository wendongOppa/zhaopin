package com.medicinezp.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.medicinezp.common.annotation.Log;
import com.medicinezp.common.core.controller.BaseController;
import com.medicinezp.common.core.domain.AjaxResult;
import com.medicinezp.common.enums.BusinessType;
import com.medicinezp.baseconfig.domain.BaseUserResumeCertificate;
import com.medicinezp.baseconfig.service.IBaseUserResumeCertificateService;
import com.medicinezp.common.utils.poi.ExcelUtil;
import com.medicinezp.common.core.page.TableDataInfo;

/**
 * 职业证书Controller
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@RestController
@RequestMapping("/baseconfig/certificate")
public class BaseUserResumeCertificateController extends BaseController
{
    @Autowired
    private IBaseUserResumeCertificateService baseUserResumeCertificateService;

    /**
     * 查询职业证书列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:certificate:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseUserResumeCertificate baseUserResumeCertificate)
    {
        startPage();
        List<BaseUserResumeCertificate> list = baseUserResumeCertificateService.selectBaseUserResumeCertificateList(baseUserResumeCertificate);
        return getDataTable(list);
    }

    /**
     * 导出职业证书列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:certificate:export')")
    @Log(title = "职业证书", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseUserResumeCertificate baseUserResumeCertificate)
    {
        List<BaseUserResumeCertificate> list = baseUserResumeCertificateService.selectBaseUserResumeCertificateList(baseUserResumeCertificate);
        ExcelUtil<BaseUserResumeCertificate> util = new ExcelUtil<BaseUserResumeCertificate>(BaseUserResumeCertificate.class);
        util.exportExcel(response, list, "职业证书数据");
    }

    /**
     * 获取职业证书详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:certificate:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(baseUserResumeCertificateService.selectBaseUserResumeCertificateById(id));
    }

    /**
     * 新增职业证书
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:certificate:add')")
    @Log(title = "职业证书", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseUserResumeCertificate baseUserResumeCertificate)
    {
        return toAjax(baseUserResumeCertificateService.insertBaseUserResumeCertificate(baseUserResumeCertificate));
    }

    /**
     * 修改职业证书
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:certificate:edit')")
    @Log(title = "职业证书", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseUserResumeCertificate baseUserResumeCertificate)
    {
        return toAjax(baseUserResumeCertificateService.updateBaseUserResumeCertificate(baseUserResumeCertificate));
    }

    /**
     * 删除职业证书
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:certificate:remove')")
    @Log(title = "职业证书", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseUserResumeCertificateService.deleteBaseUserResumeCertificateByIds(ids));
    }
}
