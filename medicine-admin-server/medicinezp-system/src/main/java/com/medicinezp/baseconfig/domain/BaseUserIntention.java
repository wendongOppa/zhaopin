package com.medicinezp.baseconfig.domain;

import com.medicinezp.common.annotation.Excel;
import com.medicinezp.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 求职意向对象 base_user_intention
 *
 * @author medicinezp
 * @date 2022-09-22
 */
public class BaseUserIntention extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 用户ID */
    @Excel(name = "用户ID")
    private Long userId;

    /** 岗位编码 */
    @Excel(name = "岗位编码")
    private Long typeId;

    /** 岗位名称 */
    @Excel(name = "岗位名称")
    private String typeName;
    private String province;

    /** 期望工作地点 */
    @Excel(name = "期望工作地点")
    private String workCity;

    /** 最低工资 */
    @Excel(name = "最低工资")
    private Long lowSalary;

    /** 最高工资 */
    @Excel(name = "最高工资")
    private Long highSalary;

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }
    public void setTypeId(Long typeId)
    {
        this.typeId = typeId;
    }

    public Long getTypeId()
    {
        return typeId;
    }
    public void setTypeName(String typeName)
    {
        this.typeName = typeName;
    }

    public String getTypeName()
    {
        return typeName;
    }
    public void setWorkCity(String workCity)
    {
        this.workCity = workCity;
    }

    public String getWorkCity()
    {
        return workCity;
    }
    public void setLowSalary(Long lowSalary)
    {
        this.lowSalary = lowSalary;
    }

    public Long getLowSalary()
    {
        return lowSalary;
    }
    public void setHighSalary(Long highSalary)
    {
        this.highSalary = highSalary;
    }

    public Long getHighSalary()
    {
        return highSalary;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("typeId", getTypeId())
            .append("typeName", getTypeName())
            .append("workCity", getWorkCity())
            .append("lowSalary", getLowSalary())
            .append("highSalary", getHighSalary())
            .append("createTime", getCreateTime())
            .toString();
    }
}
