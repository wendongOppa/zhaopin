package com.medicinezp.baseconfig.service;

import java.util.List;
import com.medicinezp.baseconfig.domain.BaseUserInviteApply;

/**
 * 邀请投递Service接口
 * 
 * @author medicinezp
 * @date 2022-09-22
 */
public interface IBaseUserInviteApplyService 
{
    /**
     * 查询邀请投递
     * 
     * @param id 邀请投递主键
     * @return 邀请投递
     */
    public BaseUserInviteApply selectBaseUserInviteApplyById(Long id);

    /**
     * 查询邀请投递列表
     * 
     * @param baseUserInviteApply 邀请投递
     * @return 邀请投递集合
     */
    public List<BaseUserInviteApply> selectBaseUserInviteApplyList(BaseUserInviteApply baseUserInviteApply);

    /**
     * 新增邀请投递
     * 
     * @param baseUserInviteApply 邀请投递
     * @return 结果
     */
    public int insertBaseUserInviteApply(BaseUserInviteApply baseUserInviteApply);

    /**
     * 修改邀请投递
     * 
     * @param baseUserInviteApply 邀请投递
     * @return 结果
     */
    public int updateBaseUserInviteApply(BaseUserInviteApply baseUserInviteApply);

    /**
     * 批量删除邀请投递
     * 
     * @param ids 需要删除的邀请投递主键集合
     * @return 结果
     */
    public int deleteBaseUserInviteApplyByIds(Long[] ids);

    /**
     * 删除邀请投递信息
     * 
     * @param id 邀请投递主键
     * @return 结果
     */
    public int deleteBaseUserInviteApplyById(Long id);
}
