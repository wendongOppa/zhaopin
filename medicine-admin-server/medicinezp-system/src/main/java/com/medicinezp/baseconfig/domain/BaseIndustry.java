package com.medicinezp.baseconfig.domain;

import com.medicinezp.common.annotation.Excel;
import com.medicinezp.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.List;

/**
 * 行业信息对象 base_industry
 *
 * @author medicinezp
 * @date 2022-09-22
 */
public class BaseIndustry extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 行业名称 */
    @Excel(name = "行业名称")
    private String name;

    /** 行业图标 */
    @Excel(name = "行业图标")
    private String iconImg;

    /** 是否可用Y可用N不可用 */
    @Excel(name = "是否可用Y可用N不可用")
    private String availableFlag;

    /** 对口专业 */
    @Excel(name = "对口专业")
    private String profession;

    /** 行业介绍 */
    @Excel(name = "行业介绍")
    private String introduction;

    /** 排序编号 */
    @Excel(name = "排序编号")
    private Long sortNo;
    private int companyNum;

    public int getCompanyNum() {
        return companyNum;
    }

    public void setCompanyNum(int companyNum) {
        this.companyNum = companyNum;
    }

    private List<BaseCompany> companyList;

    public List<BaseCompany> getCompanyList() {
        return companyList;
    }

    public void setCompanyList(List<BaseCompany> companyList) {
        this.companyList = companyList;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
    public void setIconImg(String iconImg)
    {
        this.iconImg = iconImg;
    }

    public String getIconImg()
    {
        return iconImg;
    }
    public void setAvailableFlag(String availableFlag)
    {
        this.availableFlag = availableFlag;
    }

    public String getAvailableFlag()
    {
        return availableFlag;
    }
    public void setProfession(String profession)
    {
        this.profession = profession;
    }

    public String getProfession()
    {
        return profession;
    }
    public void setIntroduction(String introduction)
    {
        this.introduction = introduction;
    }

    public String getIntroduction()
    {
        return introduction;
    }
    public void setSortNo(Long sortNo)
    {
        this.sortNo = sortNo;
    }

    public Long getSortNo()
    {
        return sortNo;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("iconImg", getIconImg())
            .append("availableFlag", getAvailableFlag())
            .append("profession", getProfession())
            .append("introduction", getIntroduction())
            .append("sortNo", getSortNo())
            .append("createTime", getCreateTime())
            .toString();
    }
}
