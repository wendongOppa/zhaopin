package com.medicinezp.baseconfig.service;

import java.util.List;
import com.medicinezp.baseconfig.domain.BaseSearchHistory;

/**
 * 搜索记录Service接口
 * 
 * @author medicinezp
 * @date 2022-10-24
 */
public interface IBaseSearchHistoryService 
{
    /**
     * 查询搜索记录
     * 
     * @param id 搜索记录主键
     * @return 搜索记录
     */
    public BaseSearchHistory selectBaseSearchHistoryById(Long id);

    /**
     * 查询搜索记录列表
     * 
     * @param baseSearchHistory 搜索记录
     * @return 搜索记录集合
     */
    public List<BaseSearchHistory> selectBaseSearchHistoryList(BaseSearchHistory baseSearchHistory);

    /**
     * 新增搜索记录
     * 
     * @param baseSearchHistory 搜索记录
     * @return 结果
     */
    public int insertBaseSearchHistory(BaseSearchHistory baseSearchHistory);

    /**
     * 修改搜索记录
     * 
     * @param baseSearchHistory 搜索记录
     * @return 结果
     */
    public int updateBaseSearchHistory(BaseSearchHistory baseSearchHistory);

    /**
     * 批量删除搜索记录
     * 
     * @param ids 需要删除的搜索记录主键集合
     * @return 结果
     */
    public int deleteBaseSearchHistoryByIds(Long[] ids);

    /**
     * 删除搜索记录信息
     * 
     * @param id 搜索记录主键
     * @return 结果
     */
    public int deleteBaseSearchHistoryById(Long id);

    List<BaseSearchHistory> selectHotSearchHistoryData();

    List<BaseSearchHistory> selectUserSearchHistoryList(BaseSearchHistory history);

    List<BaseSearchHistory> selectSearchHelpList(BaseSearchHistory history);

    int deleteBaseSearchHistoryByUserId(Long userID);
}
