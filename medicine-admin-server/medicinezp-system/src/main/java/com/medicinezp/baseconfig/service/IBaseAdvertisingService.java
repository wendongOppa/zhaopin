package com.medicinezp.baseconfig.service;

import java.util.List;
import com.medicinezp.baseconfig.domain.BaseAdvertising;

/**
 * 企业端广告Service接口
 * 
 * @author medicinezp
 * @date 2022-09-22
 */
public interface IBaseAdvertisingService 
{
    /**
     * 查询企业端广告
     * 
     * @param id 企业端广告主键
     * @return 企业端广告
     */
    public BaseAdvertising selectBaseAdvertisingById(Long id);

    /**
     * 查询企业端广告列表
     * 
     * @param baseAdvertising 企业端广告
     * @return 企业端广告集合
     */
    public List<BaseAdvertising> selectBaseAdvertisingList(BaseAdvertising baseAdvertising);

    /**
     * 新增企业端广告
     * 
     * @param baseAdvertising 企业端广告
     * @return 结果
     */
    public int insertBaseAdvertising(BaseAdvertising baseAdvertising);

    /**
     * 修改企业端广告
     * 
     * @param baseAdvertising 企业端广告
     * @return 结果
     */
    public int updateBaseAdvertising(BaseAdvertising baseAdvertising);

    /**
     * 批量删除企业端广告
     * 
     * @param ids 需要删除的企业端广告主键集合
     * @return 结果
     */
    public int deleteBaseAdvertisingByIds(Long[] ids);

    /**
     * 删除企业端广告信息
     * 
     * @param id 企业端广告主键
     * @return 结果
     */
    public int deleteBaseAdvertisingById(Long id);
}
