package com.medicinezp.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.medicinezp.common.annotation.Log;
import com.medicinezp.common.core.controller.BaseController;
import com.medicinezp.common.core.domain.AjaxResult;
import com.medicinezp.common.enums.BusinessType;
import com.medicinezp.baseconfig.domain.BaseBanner;
import com.medicinezp.baseconfig.service.IBaseBannerService;
import com.medicinezp.common.utils.poi.ExcelUtil;
import com.medicinezp.common.core.page.TableDataInfo;

/**
 * 轮播图管理Controller
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@RestController
@RequestMapping("/baseconfig/banner")
public class BaseBannerController extends BaseController
{
    @Autowired
    private IBaseBannerService baseBannerService;

    /**
     * 查询轮播图管理列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:banner:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseBanner baseBanner)
    {
        startPage();
        List<BaseBanner> list = baseBannerService.selectBaseBannerList(baseBanner);
        return getDataTable(list);
    }

    /**
     * 导出轮播图管理列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:banner:export')")
    @Log(title = "轮播图管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseBanner baseBanner)
    {
        List<BaseBanner> list = baseBannerService.selectBaseBannerList(baseBanner);
        ExcelUtil<BaseBanner> util = new ExcelUtil<BaseBanner>(BaseBanner.class);
        util.exportExcel(response, list, "轮播图管理数据");
    }

    /**
     * 获取轮播图管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:banner:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(baseBannerService.selectBaseBannerById(id));
    }

    /**
     * 新增轮播图管理
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:banner:add')")
    @Log(title = "轮播图管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseBanner baseBanner)
    {
        return toAjax(baseBannerService.insertBaseBanner(baseBanner));
    }

    /**
     * 修改轮播图管理
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:banner:edit')")
    @Log(title = "轮播图管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseBanner baseBanner)
    {
        return toAjax(baseBannerService.updateBaseBanner(baseBanner));
    }

    /**
     * 删除轮播图管理
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:banner:remove')")
    @Log(title = "轮播图管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseBannerService.deleteBaseBannerByIds(ids));
    }
}
