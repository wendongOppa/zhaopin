package com.medicinezp.baseconfig.service;

import java.util.List;
import com.medicinezp.baseconfig.domain.BaseIndustry;

/**
 * 行业信息Service接口
 * 
 * @author medicinezp
 * @date 2022-09-22
 */
public interface IBaseIndustryService 
{
    /**
     * 查询行业信息
     * 
     * @param id 行业信息主键
     * @return 行业信息
     */
    public BaseIndustry selectBaseIndustryById(Long id);

    /**
     * 查询行业信息列表
     * 
     * @param baseIndustry 行业信息
     * @return 行业信息集合
     */
    public List<BaseIndustry> selectBaseIndustryList(BaseIndustry baseIndustry);

    /**
     * 新增行业信息
     * 
     * @param baseIndustry 行业信息
     * @return 结果
     */
    public int insertBaseIndustry(BaseIndustry baseIndustry);

    /**
     * 修改行业信息
     * 
     * @param baseIndustry 行业信息
     * @return 结果
     */
    public int updateBaseIndustry(BaseIndustry baseIndustry);

    /**
     * 批量删除行业信息
     * 
     * @param ids 需要删除的行业信息主键集合
     * @return 结果
     */
    public int deleteBaseIndustryByIds(Long[] ids);

    /**
     * 删除行业信息信息
     * 
     * @param id 行业信息主键
     * @return 结果
     */
    public int deleteBaseIndustryById(Long id);
}
