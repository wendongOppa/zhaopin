package com.medicinezp.baseconfig.mapper;

import java.util.List;
import com.medicinezp.baseconfig.domain.BaseUserCollect;

/**
 * 用户收藏Mapper接口
 * 
 * @author medicinezp
 * @date 2022-09-22
 */
public interface BaseUserCollectMapper 
{
    /**
     * 查询用户收藏
     * 
     * @param id 用户收藏主键
     * @return 用户收藏
     */
    public BaseUserCollect selectBaseUserCollectById(Long id);

    /**
     * 查询用户收藏列表
     * 
     * @param baseUserCollect 用户收藏
     * @return 用户收藏集合
     */
    public List<BaseUserCollect> selectBaseUserCollectList(BaseUserCollect baseUserCollect);

    /**
     * 新增用户收藏
     * 
     * @param baseUserCollect 用户收藏
     * @return 结果
     */
    public int insertBaseUserCollect(BaseUserCollect baseUserCollect);

    /**
     * 修改用户收藏
     * 
     * @param baseUserCollect 用户收藏
     * @return 结果
     */
    public int updateBaseUserCollect(BaseUserCollect baseUserCollect);

    /**
     * 删除用户收藏
     * 
     * @param id 用户收藏主键
     * @return 结果
     */
    public int deleteBaseUserCollectById(Long id);

    /**
     * 批量删除用户收藏
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseUserCollectByIds(Long[] ids);
}
