package com.medicinezp.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.medicinezp.common.annotation.Log;
import com.medicinezp.common.core.controller.BaseController;
import com.medicinezp.common.core.domain.AjaxResult;
import com.medicinezp.common.enums.BusinessType;
import com.medicinezp.baseconfig.domain.BaseUserResumeEducation;
import com.medicinezp.baseconfig.service.IBaseUserResumeEducationService;
import com.medicinezp.common.utils.poi.ExcelUtil;
import com.medicinezp.common.core.page.TableDataInfo;

/**
 * 教育经历Controller
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@RestController
@RequestMapping("/baseconfig/education")
public class BaseUserResumeEducationController extends BaseController
{
    @Autowired
    private IBaseUserResumeEducationService baseUserResumeEducationService;

    /**
     * 查询教育经历列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:education:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseUserResumeEducation baseUserResumeEducation)
    {
        startPage();
        List<BaseUserResumeEducation> list = baseUserResumeEducationService.selectBaseUserResumeEducationList(baseUserResumeEducation);
        return getDataTable(list);
    }

    /**
     * 导出教育经历列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:education:export')")
    @Log(title = "教育经历", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseUserResumeEducation baseUserResumeEducation)
    {
        List<BaseUserResumeEducation> list = baseUserResumeEducationService.selectBaseUserResumeEducationList(baseUserResumeEducation);
        ExcelUtil<BaseUserResumeEducation> util = new ExcelUtil<BaseUserResumeEducation>(BaseUserResumeEducation.class);
        util.exportExcel(response, list, "教育经历数据");
    }

    /**
     * 获取教育经历详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:education:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(baseUserResumeEducationService.selectBaseUserResumeEducationById(id));
    }

    /**
     * 新增教育经历
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:education:add')")
    @Log(title = "教育经历", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseUserResumeEducation baseUserResumeEducation)
    {
        return toAjax(baseUserResumeEducationService.insertBaseUserResumeEducation(baseUserResumeEducation));
    }

    /**
     * 修改教育经历
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:education:edit')")
    @Log(title = "教育经历", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseUserResumeEducation baseUserResumeEducation)
    {
        return toAjax(baseUserResumeEducationService.updateBaseUserResumeEducation(baseUserResumeEducation));
    }

    /**
     * 删除教育经历
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:education:remove')")
    @Log(title = "教育经历", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseUserResumeEducationService.deleteBaseUserResumeEducationByIds(ids));
    }
}
