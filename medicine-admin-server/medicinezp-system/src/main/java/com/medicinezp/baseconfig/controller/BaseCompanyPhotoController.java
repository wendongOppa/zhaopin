package com.medicinezp.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.medicinezp.common.annotation.Log;
import com.medicinezp.common.core.controller.BaseController;
import com.medicinezp.common.core.domain.AjaxResult;
import com.medicinezp.common.enums.BusinessType;
import com.medicinezp.baseconfig.domain.BaseCompanyPhoto;
import com.medicinezp.baseconfig.service.IBaseCompanyPhotoService;
import com.medicinezp.common.utils.poi.ExcelUtil;
import com.medicinezp.common.core.page.TableDataInfo;

/**
 * 公司相册Controller
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@RestController
@RequestMapping("/baseconfig/companyphoto")
public class BaseCompanyPhotoController extends BaseController
{
    @Autowired
    private IBaseCompanyPhotoService baseCompanyPhotoService;

    /**
     * 查询公司相册列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:companyphoto:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseCompanyPhoto baseCompanyPhoto)
    {
        startPage();
        List<BaseCompanyPhoto> list = baseCompanyPhotoService.selectBaseCompanyPhotoList(baseCompanyPhoto);
        return getDataTable(list);
    }

    /**
     * 导出公司相册列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:companyphoto:export')")
    @Log(title = "公司相册", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseCompanyPhoto baseCompanyPhoto)
    {
        List<BaseCompanyPhoto> list = baseCompanyPhotoService.selectBaseCompanyPhotoList(baseCompanyPhoto);
        ExcelUtil<BaseCompanyPhoto> util = new ExcelUtil<BaseCompanyPhoto>(BaseCompanyPhoto.class);
        util.exportExcel(response, list, "公司相册数据");
    }

    /**
     * 获取公司相册详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:companyphoto:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(baseCompanyPhotoService.selectBaseCompanyPhotoById(id));
    }

    /**
     * 新增公司相册
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:companyphoto:add')")
    @Log(title = "公司相册", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseCompanyPhoto baseCompanyPhoto)
    {
        return toAjax(baseCompanyPhotoService.insertBaseCompanyPhoto(baseCompanyPhoto));
    }

    /**
     * 修改公司相册
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:companyphoto:edit')")
    @Log(title = "公司相册", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseCompanyPhoto baseCompanyPhoto)
    {
        return toAjax(baseCompanyPhotoService.updateBaseCompanyPhoto(baseCompanyPhoto));
    }

    /**
     * 删除公司相册
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:companyphoto:remove')")
    @Log(title = "公司相册", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseCompanyPhotoService.deleteBaseCompanyPhotoByIds(ids));
    }
}
