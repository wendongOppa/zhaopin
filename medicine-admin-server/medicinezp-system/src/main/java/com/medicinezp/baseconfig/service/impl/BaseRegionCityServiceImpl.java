package com.medicinezp.baseconfig.service.impl;

import java.util.List;
import com.medicinezp.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.medicinezp.baseconfig.mapper.BaseRegionCityMapper;
import com.medicinezp.baseconfig.domain.BaseRegionCity;
import com.medicinezp.baseconfig.service.IBaseRegionCityService;

/**
 * 城市Service业务层处理
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@Service
public class BaseRegionCityServiceImpl implements IBaseRegionCityService
{
    @Autowired
    private BaseRegionCityMapper baseRegionCityMapper;

    /**
     * 查询城市
     *
     * @param cityId 城市主键
     * @return 城市
     */
    @Override
    public BaseRegionCity selectBaseRegionCityByCityId(Long cityId)
    {
        return baseRegionCityMapper.selectBaseRegionCityByCityId(cityId);
    }

    /**
     * 查询城市列表
     *
     * @param baseRegionCity 城市
     * @return 城市
     */
    @Override
    public List<BaseRegionCity> selectBaseRegionCityList(BaseRegionCity baseRegionCity)
    {
        return baseRegionCityMapper.selectBaseRegionCityList(baseRegionCity);
    }

    /**
     * 新增城市
     *
     * @param baseRegionCity 城市
     * @return 结果
     */
    @Override
    public int insertBaseRegionCity(BaseRegionCity baseRegionCity)
    {
        baseRegionCity.setCreateTime(DateUtils.getNowDate());
        return baseRegionCityMapper.insertBaseRegionCity(baseRegionCity);
    }

    /**
     * 修改城市
     *
     * @param baseRegionCity 城市
     * @return 结果
     */
    @Override
    public int updateBaseRegionCity(BaseRegionCity baseRegionCity)
    {
        baseRegionCity.setUpdateTime(DateUtils.getNowDate());
        return baseRegionCityMapper.updateBaseRegionCity(baseRegionCity);
    }

    /**
     * 批量删除城市
     *
     * @param cityIds 需要删除的城市主键
     * @return 结果
     */
    @Override
    public int deleteBaseRegionCityByCityIds(Long[] cityIds)
    {
        return baseRegionCityMapper.deleteBaseRegionCityByCityIds(cityIds);
    }

    /**
     * 删除城市信息
     *
     * @param cityId 城市主键
     * @return 结果
     */
    @Override
    public int deleteBaseRegionCityByCityId(Long cityId)
    {
        return baseRegionCityMapper.deleteBaseRegionCityByCityId(cityId);
    }
}
