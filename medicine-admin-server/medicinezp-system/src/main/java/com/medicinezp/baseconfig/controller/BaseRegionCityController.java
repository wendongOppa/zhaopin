package com.medicinezp.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.medicinezp.common.annotation.Log;
import com.medicinezp.common.core.controller.BaseController;
import com.medicinezp.common.core.domain.AjaxResult;
import com.medicinezp.common.enums.BusinessType;
import com.medicinezp.baseconfig.domain.BaseRegionCity;
import com.medicinezp.baseconfig.service.IBaseRegionCityService;
import com.medicinezp.common.utils.poi.ExcelUtil;
import com.medicinezp.common.core.page.TableDataInfo;

/**
 * 城市Controller
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@RestController
@RequestMapping("/baseconfig/city")
public class BaseRegionCityController extends BaseController
{
    @Autowired
    private IBaseRegionCityService baseRegionCityService;

    /**
     * 查询城市列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:city:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseRegionCity baseRegionCity)
    {
        startPage();
        List<BaseRegionCity> list = baseRegionCityService.selectBaseRegionCityList(baseRegionCity);
        return getDataTable(list);
    }

    /**
     * 导出城市列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:city:export')")
    @Log(title = "城市", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseRegionCity baseRegionCity)
    {
        List<BaseRegionCity> list = baseRegionCityService.selectBaseRegionCityList(baseRegionCity);
        ExcelUtil<BaseRegionCity> util = new ExcelUtil<BaseRegionCity>(BaseRegionCity.class);
        util.exportExcel(response, list, "城市数据");
    }

    /**
     * 获取城市详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:city:query')")
    @GetMapping(value = "/{cityId}")
    public AjaxResult getInfo(@PathVariable("cityId") Long cityId)
    {
        return AjaxResult.success(baseRegionCityService.selectBaseRegionCityByCityId(cityId));
    }

    /**
     * 新增城市
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:city:add')")
    @Log(title = "城市", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseRegionCity baseRegionCity)
    {
        return toAjax(baseRegionCityService.insertBaseRegionCity(baseRegionCity));
    }

    /**
     * 修改城市
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:city:edit')")
    @Log(title = "城市", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseRegionCity baseRegionCity)
    {
        return toAjax(baseRegionCityService.updateBaseRegionCity(baseRegionCity));
    }

    /**
     * 删除城市
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:city:remove')")
    @Log(title = "城市", businessType = BusinessType.DELETE)
	@DeleteMapping("/{cityIds}")
    public AjaxResult remove(@PathVariable Long[] cityIds)
    {
        return toAjax(baseRegionCityService.deleteBaseRegionCityByCityIds(cityIds));
    }
}
