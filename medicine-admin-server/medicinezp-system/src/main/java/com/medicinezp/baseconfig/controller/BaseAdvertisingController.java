package com.medicinezp.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.medicinezp.common.annotation.Log;
import com.medicinezp.common.core.controller.BaseController;
import com.medicinezp.common.core.domain.AjaxResult;
import com.medicinezp.common.enums.BusinessType;
import com.medicinezp.baseconfig.domain.BaseAdvertising;
import com.medicinezp.baseconfig.service.IBaseAdvertisingService;
import com.medicinezp.common.utils.poi.ExcelUtil;
import com.medicinezp.common.core.page.TableDataInfo;

/**
 * 企业端广告Controller
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@RestController
@RequestMapping("/baseconfig/advertising")
public class BaseAdvertisingController extends BaseController
{
    @Autowired
    private IBaseAdvertisingService baseAdvertisingService;

    /**
     * 查询企业端广告列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:advertising:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseAdvertising baseAdvertising)
    {
        startPage();
        List<BaseAdvertising> list = baseAdvertisingService.selectBaseAdvertisingList(baseAdvertising);
        return getDataTable(list);
    }

    /**
     * 导出企业端广告列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:advertising:export')")
    @Log(title = "企业端广告", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseAdvertising baseAdvertising)
    {
        List<BaseAdvertising> list = baseAdvertisingService.selectBaseAdvertisingList(baseAdvertising);
        ExcelUtil<BaseAdvertising> util = new ExcelUtil<BaseAdvertising>(BaseAdvertising.class);
        util.exportExcel(response, list, "企业端广告数据");
    }

    /**
     * 获取企业端广告详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:advertising:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(baseAdvertisingService.selectBaseAdvertisingById(id));
    }

    /**
     * 新增企业端广告
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:advertising:add')")
    @Log(title = "企业端广告", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseAdvertising baseAdvertising)
    {
        return toAjax(baseAdvertisingService.insertBaseAdvertising(baseAdvertising));
    }

    /**
     * 修改企业端广告
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:advertising:edit')")
    @Log(title = "企业端广告", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseAdvertising baseAdvertising)
    {
        return toAjax(baseAdvertisingService.updateBaseAdvertising(baseAdvertising));
    }

    /**
     * 删除企业端广告
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:advertising:remove')")
    @Log(title = "企业端广告", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseAdvertisingService.deleteBaseAdvertisingByIds(ids));
    }
}
