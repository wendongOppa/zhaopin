package com.medicinezp.baseconfig.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.medicinezp.baseconfig.mapper.BaseUserInviteApplyMapper;
import com.medicinezp.baseconfig.domain.BaseUserInviteApply;
import com.medicinezp.baseconfig.service.IBaseUserInviteApplyService;

/**
 * 邀请投递Service业务层处理
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@Service
public class BaseUserInviteApplyServiceImpl implements IBaseUserInviteApplyService
{
    @Autowired
    private BaseUserInviteApplyMapper baseUserInviteApplyMapper;

    /**
     * 查询邀请投递
     *
     * @param id 邀请投递主键
     * @return 邀请投递
     */
    @Override
    public BaseUserInviteApply selectBaseUserInviteApplyById(Long id)
    {
        return baseUserInviteApplyMapper.selectBaseUserInviteApplyById(id);
    }

    /**
     * 查询邀请投递列表
     *
     * @param baseUserInviteApply 邀请投递
     * @return 邀请投递
     */
    @Override
    public List<BaseUserInviteApply> selectBaseUserInviteApplyList(BaseUserInviteApply baseUserInviteApply)
    {
        return baseUserInviteApplyMapper.selectBaseUserInviteApplyList(baseUserInviteApply);
    }

    /**
     * 新增邀请投递
     *
     * @param baseUserInviteApply 邀请投递
     * @return 结果
     */
    @Override
    public int insertBaseUserInviteApply(BaseUserInviteApply baseUserInviteApply)
    {
        return baseUserInviteApplyMapper.insertBaseUserInviteApply(baseUserInviteApply);
    }

    /**
     * 修改邀请投递
     *
     * @param baseUserInviteApply 邀请投递
     * @return 结果
     */
    @Override
    public int updateBaseUserInviteApply(BaseUserInviteApply baseUserInviteApply)
    {
        return baseUserInviteApplyMapper.updateBaseUserInviteApply(baseUserInviteApply);
    }

    /**
     * 批量删除邀请投递
     *
     * @param ids 需要删除的邀请投递主键
     * @return 结果
     */
    @Override
    public int deleteBaseUserInviteApplyByIds(Long[] ids)
    {
        return baseUserInviteApplyMapper.deleteBaseUserInviteApplyByIds(ids);
    }

    /**
     * 删除邀请投递信息
     *
     * @param id 邀请投递主键
     * @return 结果
     */
    @Override
    public int deleteBaseUserInviteApplyById(Long id)
    {
        return baseUserInviteApplyMapper.deleteBaseUserInviteApplyById(id);
    }
}
