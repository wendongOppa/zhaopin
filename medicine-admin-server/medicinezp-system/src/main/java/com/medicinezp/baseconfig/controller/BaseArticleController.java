package com.medicinezp.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.medicinezp.common.annotation.Log;
import com.medicinezp.common.core.controller.BaseController;
import com.medicinezp.common.core.domain.AjaxResult;
import com.medicinezp.common.enums.BusinessType;
import com.medicinezp.baseconfig.domain.BaseArticle;
import com.medicinezp.baseconfig.service.IBaseArticleService;
import com.medicinezp.common.utils.poi.ExcelUtil;
import com.medicinezp.common.core.page.TableDataInfo;

/**
 * 资讯动态Controller
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@RestController
@RequestMapping("/baseconfig/article")
public class BaseArticleController extends BaseController
{
    @Autowired
    private IBaseArticleService baseArticleService;

    /**
     * 查询资讯动态列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:article:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseArticle baseArticle)
    {
        startPage();
        List<BaseArticle> list = baseArticleService.selectBaseArticleList(baseArticle);
        return getDataTable(list);
    }

    /**
     * 导出资讯动态列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:article:export')")
    @Log(title = "资讯动态", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseArticle baseArticle)
    {
        List<BaseArticle> list = baseArticleService.selectBaseArticleList(baseArticle);
        ExcelUtil<BaseArticle> util = new ExcelUtil<BaseArticle>(BaseArticle.class);
        util.exportExcel(response, list, "资讯动态数据");
    }

    /**
     * 获取资讯动态详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:article:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(baseArticleService.selectBaseArticleById(id));
    }

    /**
     * 新增资讯动态
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:article:add')")
    @Log(title = "资讯动态", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseArticle baseArticle)
    {
        return toAjax(baseArticleService.insertBaseArticle(baseArticle));
    }

    /**
     * 修改资讯动态
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:article:edit')")
    @Log(title = "资讯动态", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseArticle baseArticle)
    {
        return toAjax(baseArticleService.updateBaseArticle(baseArticle));
    }

    /**
     * 删除资讯动态
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:article:remove')")
    @Log(title = "资讯动态", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseArticleService.deleteBaseArticleByIds(ids));
    }
}
