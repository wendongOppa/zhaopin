package com.medicinezp.baseconfig.service;

import java.util.List;
import com.medicinezp.baseconfig.domain.BaseIndustryCompanyRela;

/**
 * 行业推荐企业Service接口
 * 
 * @author medicinezp
 * @date 2022-09-22
 */
public interface IBaseIndustryCompanyRelaService 
{
    /**
     * 查询行业推荐企业
     * 
     * @param id 行业推荐企业主键
     * @return 行业推荐企业
     */
    public BaseIndustryCompanyRela selectBaseIndustryCompanyRelaById(Long id);

    /**
     * 查询行业推荐企业列表
     * 
     * @param baseIndustryCompanyRela 行业推荐企业
     * @return 行业推荐企业集合
     */
    public List<BaseIndustryCompanyRela> selectBaseIndustryCompanyRelaList(BaseIndustryCompanyRela baseIndustryCompanyRela);

    /**
     * 新增行业推荐企业
     * 
     * @param baseIndustryCompanyRela 行业推荐企业
     * @return 结果
     */
    public int insertBaseIndustryCompanyRela(BaseIndustryCompanyRela baseIndustryCompanyRela);

    /**
     * 修改行业推荐企业
     * 
     * @param baseIndustryCompanyRela 行业推荐企业
     * @return 结果
     */
    public int updateBaseIndustryCompanyRela(BaseIndustryCompanyRela baseIndustryCompanyRela);

    /**
     * 批量删除行业推荐企业
     * 
     * @param ids 需要删除的行业推荐企业主键集合
     * @return 结果
     */
    public int deleteBaseIndustryCompanyRelaByIds(Long[] ids);

    /**
     * 删除行业推荐企业信息
     * 
     * @param id 行业推荐企业主键
     * @return 结果
     */
    public int deleteBaseIndustryCompanyRelaById(Long id);
}
