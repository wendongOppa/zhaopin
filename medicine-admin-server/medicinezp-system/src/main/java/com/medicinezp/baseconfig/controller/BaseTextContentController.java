package com.medicinezp.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.medicinezp.common.annotation.Log;
import com.medicinezp.common.core.controller.BaseController;
import com.medicinezp.common.core.domain.AjaxResult;
import com.medicinezp.common.enums.BusinessType;
import com.medicinezp.baseconfig.domain.BaseTextContent;
import com.medicinezp.baseconfig.service.IBaseTextContentService;
import com.medicinezp.common.utils.poi.ExcelUtil;
import com.medicinezp.common.core.page.TableDataInfo;

/**
 * 文案管理Controller
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@RestController
@RequestMapping("/baseconfig/textcontent")
public class BaseTextContentController extends BaseController
{
    @Autowired
    private IBaseTextContentService baseTextContentService;

    /**
     * 查询文案管理列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:textcontent:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseTextContent baseTextContent)
    {
        startPage();
        List<BaseTextContent> list = baseTextContentService.selectBaseTextContentList(baseTextContent);
        return getDataTable(list);
    }

    /**
     * 导出文案管理列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:textcontent:export')")
    @Log(title = "文案管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseTextContent baseTextContent)
    {
        List<BaseTextContent> list = baseTextContentService.selectBaseTextContentList(baseTextContent);
        ExcelUtil<BaseTextContent> util = new ExcelUtil<BaseTextContent>(BaseTextContent.class);
        util.exportExcel(response, list, "文案管理数据");
    }

    /**
     * 获取文案管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:textcontent:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(baseTextContentService.selectBaseTextContentById(id));
    }

    /**
     * 新增文案管理
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:textcontent:add')")
    @Log(title = "文案管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseTextContent baseTextContent)
    {
        return toAjax(baseTextContentService.insertBaseTextContent(baseTextContent));
    }

    /**
     * 修改文案管理
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:textcontent:edit')")
    @Log(title = "文案管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseTextContent baseTextContent)
    {
        return toAjax(baseTextContentService.updateBaseTextContent(baseTextContent));
    }

    /**
     * 删除文案管理
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:textcontent:remove')")
    @Log(title = "文案管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseTextContentService.deleteBaseTextContentByIds(ids));
    }
}
