package com.medicinezp.baseconfig.service.impl;

import java.util.List;
import com.medicinezp.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.medicinezp.baseconfig.mapper.BaseBannerMapper;
import com.medicinezp.baseconfig.domain.BaseBanner;
import com.medicinezp.baseconfig.service.IBaseBannerService;

/**
 * 轮播图管理Service业务层处理
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@Service
public class BaseBannerServiceImpl implements IBaseBannerService
{
    @Autowired
    private BaseBannerMapper baseBannerMapper;

    /**
     * 查询轮播图管理
     *
     * @param id 轮播图管理主键
     * @return 轮播图管理
     */
    @Override
    public BaseBanner selectBaseBannerById(Long id)
    {
        return baseBannerMapper.selectBaseBannerById(id);
    }

    /**
     * 查询轮播图管理列表
     *
     * @param baseBanner 轮播图管理
     * @return 轮播图管理
     */
    @Override
    public List<BaseBanner> selectBaseBannerList(BaseBanner baseBanner)
    {
        return baseBannerMapper.selectBaseBannerList(baseBanner);
    }

    /**
     * 新增轮播图管理
     *
     * @param baseBanner 轮播图管理
     * @return 结果
     */
    @Override
    public int insertBaseBanner(BaseBanner baseBanner)
    {
        baseBanner.setCreateTime(DateUtils.getNowDate());
        return baseBannerMapper.insertBaseBanner(baseBanner);
    }

    /**
     * 修改轮播图管理
     *
     * @param baseBanner 轮播图管理
     * @return 结果
     */
    @Override
    public int updateBaseBanner(BaseBanner baseBanner)
    {
        baseBanner.setUpdateTime(DateUtils.getNowDate());
        return baseBannerMapper.updateBaseBanner(baseBanner);
    }

    /**
     * 批量删除轮播图管理
     *
     * @param ids 需要删除的轮播图管理主键
     * @return 结果
     */
    @Override
    public int deleteBaseBannerByIds(Long[] ids)
    {
        return baseBannerMapper.deleteBaseBannerByIds(ids);
    }

    /**
     * 删除轮播图管理信息
     *
     * @param id 轮播图管理主键
     * @return 结果
     */
    @Override
    public int deleteBaseBannerById(Long id)
    {
        return baseBannerMapper.deleteBaseBannerById(id);
    }
}
