package com.medicinezp.baseconfig.mapper;

import java.util.List;
import com.medicinezp.baseconfig.domain.BaseUserJobApplyLogs;

/**
 * 用户投递简历日志Mapper接口
 * 
 * @author medicinezp
 * @date 2022-09-30
 */
public interface BaseUserJobApplyLogsMapper 
{
    /**
     * 查询用户投递简历日志
     * 
     * @param id 用户投递简历日志主键
     * @return 用户投递简历日志
     */
    public BaseUserJobApplyLogs selectBaseUserJobApplyLogsById(Long id);

    /**
     * 查询用户投递简历日志列表
     * 
     * @param baseUserJobApplyLogs 用户投递简历日志
     * @return 用户投递简历日志集合
     */
    public List<BaseUserJobApplyLogs> selectBaseUserJobApplyLogsList(BaseUserJobApplyLogs baseUserJobApplyLogs);

    /**
     * 新增用户投递简历日志
     * 
     * @param baseUserJobApplyLogs 用户投递简历日志
     * @return 结果
     */
    public int insertBaseUserJobApplyLogs(BaseUserJobApplyLogs baseUserJobApplyLogs);

    /**
     * 修改用户投递简历日志
     * 
     * @param baseUserJobApplyLogs 用户投递简历日志
     * @return 结果
     */
    public int updateBaseUserJobApplyLogs(BaseUserJobApplyLogs baseUserJobApplyLogs);

    /**
     * 删除用户投递简历日志
     * 
     * @param id 用户投递简历日志主键
     * @return 结果
     */
    public int deleteBaseUserJobApplyLogsById(Long id);

    /**
     * 批量删除用户投递简历日志
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseUserJobApplyLogsByIds(Long[] ids);
}
