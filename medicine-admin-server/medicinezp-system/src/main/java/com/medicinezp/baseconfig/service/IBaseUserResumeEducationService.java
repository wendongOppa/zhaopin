package com.medicinezp.baseconfig.service;

import java.util.List;
import com.medicinezp.baseconfig.domain.BaseUserResumeEducation;

/**
 * 教育经历Service接口
 * 
 * @author medicinezp
 * @date 2022-09-22
 */
public interface IBaseUserResumeEducationService 
{
    /**
     * 查询教育经历
     * 
     * @param id 教育经历主键
     * @return 教育经历
     */
    public BaseUserResumeEducation selectBaseUserResumeEducationById(Long id);

    /**
     * 查询教育经历列表
     * 
     * @param baseUserResumeEducation 教育经历
     * @return 教育经历集合
     */
    public List<BaseUserResumeEducation> selectBaseUserResumeEducationList(BaseUserResumeEducation baseUserResumeEducation);

    /**
     * 新增教育经历
     * 
     * @param baseUserResumeEducation 教育经历
     * @return 结果
     */
    public int insertBaseUserResumeEducation(BaseUserResumeEducation baseUserResumeEducation);

    /**
     * 修改教育经历
     * 
     * @param baseUserResumeEducation 教育经历
     * @return 结果
     */
    public int updateBaseUserResumeEducation(BaseUserResumeEducation baseUserResumeEducation);

    /**
     * 批量删除教育经历
     * 
     * @param ids 需要删除的教育经历主键集合
     * @return 结果
     */
    public int deleteBaseUserResumeEducationByIds(Long[] ids);

    /**
     * 删除教育经历信息
     * 
     * @param id 教育经历主键
     * @return 结果
     */
    public int deleteBaseUserResumeEducationById(Long id);
}
