package com.medicinezp.baseconfig.domain;

import com.medicinezp.common.annotation.Excel;
import com.medicinezp.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 区间设置对象 base_range_config
 *
 * @author medicinezp
 * @date 2022-09-22
 */
public class BaseRangeConfig extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 类型  1工资 */
    @Excel(name = "类型  1工资")
    private String typeUnit;

    /** 类型 */
    @Excel(name = "类型")
    private String typeName;

    /** 最小 */
    @Excel(name = "最小")
    private Long minValue;

    /** 最大值 */
    @Excel(name = "最大值")
    private Long maxValue;

    /** 字符串 */
    @Excel(name = "字符串")
    private String rangeStr;

    /** 排序 */
    @Excel(name = "排序")
    private Long sortNo;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setTypeUnit(String typeUnit)
    {
        this.typeUnit = typeUnit;
    }

    public String getTypeUnit()
    {
        return typeUnit;
    }
    public void setTypeName(String typeName)
    {
        this.typeName = typeName;
    }

    public String getTypeName()
    {
        return typeName;
    }
    public void setMinValue(Long minValue)
    {
        this.minValue = minValue;
    }

    public Long getMinValue()
    {
        return minValue;
    }
    public void setMaxValue(Long maxValue)
    {
        this.maxValue = maxValue;
    }

    public Long getMaxValue()
    {
        return maxValue;
    }
    public void setRangeStr(String rangeStr)
    {
        this.rangeStr = rangeStr;
    }

    public String getRangeStr()
    {
        return rangeStr;
    }
    public void setSortNo(Long sortNo)
    {
        this.sortNo = sortNo;
    }

    public Long getSortNo()
    {
        return sortNo;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("typeUnit", getTypeUnit())
            .append("typeName", getTypeName())
            .append("minValue", getMinValue())
            .append("maxValue", getMaxValue())
            .append("rangeStr", getRangeStr())
            .append("sortNo", getSortNo())
            .toString();
    }
}
