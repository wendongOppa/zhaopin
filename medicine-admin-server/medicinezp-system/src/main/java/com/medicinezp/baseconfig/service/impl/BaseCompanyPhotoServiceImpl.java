package com.medicinezp.baseconfig.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.medicinezp.baseconfig.mapper.BaseCompanyPhotoMapper;
import com.medicinezp.baseconfig.domain.BaseCompanyPhoto;
import com.medicinezp.baseconfig.service.IBaseCompanyPhotoService;

/**
 * 公司相册Service业务层处理
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@Service
public class BaseCompanyPhotoServiceImpl implements IBaseCompanyPhotoService
{
    @Autowired
    private BaseCompanyPhotoMapper baseCompanyPhotoMapper;

    /**
     * 查询公司相册
     *
     * @param id 公司相册主键
     * @return 公司相册
     */
    @Override
    public BaseCompanyPhoto selectBaseCompanyPhotoById(Long id)
    {
        return baseCompanyPhotoMapper.selectBaseCompanyPhotoById(id);
    }

    /**
     * 查询公司相册列表
     *
     * @param baseCompanyPhoto 公司相册
     * @return 公司相册
     */
    @Override
    public List<BaseCompanyPhoto> selectBaseCompanyPhotoList(BaseCompanyPhoto baseCompanyPhoto)
    {
        return baseCompanyPhotoMapper.selectBaseCompanyPhotoList(baseCompanyPhoto);
    }

    /**
     * 新增公司相册
     *
     * @param baseCompanyPhoto 公司相册
     * @return 结果
     */
    @Override
    public int insertBaseCompanyPhoto(BaseCompanyPhoto baseCompanyPhoto)
    {
        return baseCompanyPhotoMapper.insertBaseCompanyPhoto(baseCompanyPhoto);
    }

    /**
     * 修改公司相册
     *
     * @param baseCompanyPhoto 公司相册
     * @return 结果
     */
    @Override
    public int updateBaseCompanyPhoto(BaseCompanyPhoto baseCompanyPhoto)
    {
        return baseCompanyPhotoMapper.updateBaseCompanyPhoto(baseCompanyPhoto);
    }

    /**
     * 批量删除公司相册
     *
     * @param ids 需要删除的公司相册主键
     * @return 结果
     */
    @Override
    public int deleteBaseCompanyPhotoByIds(Long[] ids)
    {
        return baseCompanyPhotoMapper.deleteBaseCompanyPhotoByIds(ids);
    }

    /**
     * 删除公司相册信息
     *
     * @param id 公司相册主键
     * @return 结果
     */
    @Override
    public int deleteBaseCompanyPhotoById(Long id)
    {
        return baseCompanyPhotoMapper.deleteBaseCompanyPhotoById(id);
    }
}
