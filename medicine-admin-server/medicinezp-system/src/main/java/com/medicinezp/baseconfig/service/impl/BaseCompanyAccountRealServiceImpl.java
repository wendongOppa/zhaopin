package com.medicinezp.baseconfig.service.impl;

import java.util.List;
import com.medicinezp.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.medicinezp.baseconfig.mapper.BaseCompanyAccountRealMapper;
import com.medicinezp.baseconfig.domain.BaseCompanyAccountReal;
import com.medicinezp.baseconfig.service.IBaseCompanyAccountRealService;

/**
 * 公司招聘人员Service业务层处理
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@Service
public class BaseCompanyAccountRealServiceImpl implements IBaseCompanyAccountRealService
{
    @Autowired
    private BaseCompanyAccountRealMapper baseCompanyAccountRealMapper;

    /**
     * 查询公司招聘人员
     *
     * @param id 公司招聘人员主键
     * @return 公司招聘人员
     */
    @Override
    public BaseCompanyAccountReal selectBaseCompanyAccountRealById(Long id)
    {
        return baseCompanyAccountRealMapper.selectBaseCompanyAccountRealById(id);
    }

    /**
     * 查询公司招聘人员列表
     *
     * @param baseCompanyAccountReal 公司招聘人员
     * @return 公司招聘人员
     */
    @Override
    public List<BaseCompanyAccountReal> selectBaseCompanyAccountRealList(BaseCompanyAccountReal baseCompanyAccountReal)
    {
        return baseCompanyAccountRealMapper.selectBaseCompanyAccountRealList(baseCompanyAccountReal);
    }

    /**
     * 新增公司招聘人员
     *
     * @param baseCompanyAccountReal 公司招聘人员
     * @return 结果
     */
    @Override
    public int insertBaseCompanyAccountReal(BaseCompanyAccountReal baseCompanyAccountReal)
    {
        baseCompanyAccountReal.setCreateTime(DateUtils.getNowDate());
        return baseCompanyAccountRealMapper.insertBaseCompanyAccountReal(baseCompanyAccountReal);
    }

    /**
     * 修改公司招聘人员
     *
     * @param baseCompanyAccountReal 公司招聘人员
     * @return 结果
     */
    @Override
    public int updateBaseCompanyAccountReal(BaseCompanyAccountReal baseCompanyAccountReal)
    {
        return baseCompanyAccountRealMapper.updateBaseCompanyAccountReal(baseCompanyAccountReal);
    }

    /**
     * 批量删除公司招聘人员
     *
     * @param ids 需要删除的公司招聘人员主键
     * @return 结果
     */
    @Override
    public int deleteBaseCompanyAccountRealByIds(Long[] ids)
    {
        return baseCompanyAccountRealMapper.deleteBaseCompanyAccountRealByIds(ids);
    }

    /**
     * 删除公司招聘人员信息
     *
     * @param id 公司招聘人员主键
     * @return 结果
     */
    @Override
    public int deleteBaseCompanyAccountRealById(Long id)
    {
        return baseCompanyAccountRealMapper.deleteBaseCompanyAccountRealById(id);
    }

    @Override
    public BaseCompanyAccountReal selectBaseCompanyAccountRealByUserId(Long userId) {
        return baseCompanyAccountRealMapper.selectBaseCompanyAccountRealByUserId(userId);
    }
}
