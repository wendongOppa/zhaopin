package com.medicinezp.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.medicinezp.common.annotation.Log;
import com.medicinezp.common.core.controller.BaseController;
import com.medicinezp.common.core.domain.AjaxResult;
import com.medicinezp.common.enums.BusinessType;
import com.medicinezp.baseconfig.domain.BaseJobLogs;
import com.medicinezp.baseconfig.service.IBaseJobLogsService;
import com.medicinezp.common.utils.poi.ExcelUtil;
import com.medicinezp.common.core.page.TableDataInfo;

/**
 * 职位修改日志Controller
 *
 * @author medicinezp
 * @date 2022-10-13
 */
@RestController
@RequestMapping("/baseconfig/joblogs")
public class BaseJobLogsController extends BaseController
{
    @Autowired
    private IBaseJobLogsService baseJobLogsService;

    /**
     * 查询职位修改日志列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:joblogs:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseJobLogs baseJobLogs)
    {
        startPage();
        List<BaseJobLogs> list = baseJobLogsService.selectBaseJobLogsList(baseJobLogs);
        return getDataTable(list);
    }

    @GetMapping("/getLogsByJobId")
    public AjaxResult getLogsByJobId(BaseJobLogs baseJobLogs)
    {
        List<BaseJobLogs> list = baseJobLogsService.selectBaseJobLogsList(baseJobLogs);
        return AjaxResult.success(list);
    }

    /**
     * 导出职位修改日志列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:joblogs:export')")
    @Log(title = "职位修改日志", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseJobLogs baseJobLogs)
    {
        List<BaseJobLogs> list = baseJobLogsService.selectBaseJobLogsList(baseJobLogs);
        ExcelUtil<BaseJobLogs> util = new ExcelUtil<BaseJobLogs>(BaseJobLogs.class);
        util.exportExcel(response, list, "职位修改日志数据");
    }

    /**
     * 获取职位修改日志详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:joblogs:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(baseJobLogsService.selectBaseJobLogsById(id));
    }

    /**
     * 新增职位修改日志
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:joblogs:add')")
    @Log(title = "职位修改日志", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseJobLogs baseJobLogs)
    {
        return toAjax(baseJobLogsService.insertBaseJobLogs(baseJobLogs));
    }

    /**
     * 修改职位修改日志
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:joblogs:edit')")
    @Log(title = "职位修改日志", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseJobLogs baseJobLogs)
    {
        return toAjax(baseJobLogsService.updateBaseJobLogs(baseJobLogs));
    }

    /**
     * 删除职位修改日志
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:joblogs:remove')")
    @Log(title = "职位修改日志", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseJobLogsService.deleteBaseJobLogsByIds(ids));
    }
}
