package com.medicinezp.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.medicinezp.common.annotation.Log;
import com.medicinezp.common.core.controller.BaseController;
import com.medicinezp.common.core.domain.AjaxResult;
import com.medicinezp.common.enums.BusinessType;
import com.medicinezp.baseconfig.domain.BaseRegionProvince;
import com.medicinezp.baseconfig.service.IBaseRegionProvinceService;
import com.medicinezp.common.utils.poi.ExcelUtil;
import com.medicinezp.common.core.page.TableDataInfo;

/**
 * 省份信息Controller
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@RestController
@RequestMapping("/baseconfig/province")
public class BaseRegionProvinceController extends BaseController
{
    @Autowired
    private IBaseRegionProvinceService baseRegionProvinceService;

    /**
     * 查询省份信息列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:province:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseRegionProvince baseRegionProvince)
    {
        startPage();
        List<BaseRegionProvince> list = baseRegionProvinceService.selectBaseRegionProvinceList(baseRegionProvince);
        return getDataTable(list);
    }

    /**
     * 导出省份信息列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:province:export')")
    @Log(title = "省份信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseRegionProvince baseRegionProvince)
    {
        List<BaseRegionProvince> list = baseRegionProvinceService.selectBaseRegionProvinceList(baseRegionProvince);
        ExcelUtil<BaseRegionProvince> util = new ExcelUtil<BaseRegionProvince>(BaseRegionProvince.class);
        util.exportExcel(response, list, "省份信息数据");
    }

    /**
     * 获取省份信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:province:query')")
    @GetMapping(value = "/{provinceId}")
    public AjaxResult getInfo(@PathVariable("provinceId") Long provinceId)
    {
        return AjaxResult.success(baseRegionProvinceService.selectBaseRegionProvinceByProvinceId(provinceId));
    }

    /**
     * 新增省份信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:province:add')")
    @Log(title = "省份信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseRegionProvince baseRegionProvince)
    {
        return toAjax(baseRegionProvinceService.insertBaseRegionProvince(baseRegionProvince));
    }

    /**
     * 修改省份信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:province:edit')")
    @Log(title = "省份信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseRegionProvince baseRegionProvince)
    {
        return toAjax(baseRegionProvinceService.updateBaseRegionProvince(baseRegionProvince));
    }

    /**
     * 删除省份信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:province:remove')")
    @Log(title = "省份信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{provinceIds}")
    public AjaxResult remove(@PathVariable Long[] provinceIds)
    {
        return toAjax(baseRegionProvinceService.deleteBaseRegionProvinceByProvinceIds(provinceIds));
    }
}
