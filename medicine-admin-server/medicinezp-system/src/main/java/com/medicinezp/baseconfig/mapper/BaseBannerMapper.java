package com.medicinezp.baseconfig.mapper;

import java.util.List;
import com.medicinezp.baseconfig.domain.BaseBanner;

/**
 * 轮播图管理Mapper接口
 * 
 * @author medicinezp
 * @date 2022-09-22
 */
public interface BaseBannerMapper 
{
    /**
     * 查询轮播图管理
     * 
     * @param id 轮播图管理主键
     * @return 轮播图管理
     */
    public BaseBanner selectBaseBannerById(Long id);

    /**
     * 查询轮播图管理列表
     * 
     * @param baseBanner 轮播图管理
     * @return 轮播图管理集合
     */
    public List<BaseBanner> selectBaseBannerList(BaseBanner baseBanner);

    /**
     * 新增轮播图管理
     * 
     * @param baseBanner 轮播图管理
     * @return 结果
     */
    public int insertBaseBanner(BaseBanner baseBanner);

    /**
     * 修改轮播图管理
     * 
     * @param baseBanner 轮播图管理
     * @return 结果
     */
    public int updateBaseBanner(BaseBanner baseBanner);

    /**
     * 删除轮播图管理
     * 
     * @param id 轮播图管理主键
     * @return 结果
     */
    public int deleteBaseBannerById(Long id);

    /**
     * 批量删除轮播图管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseBannerByIds(Long[] ids);
}
