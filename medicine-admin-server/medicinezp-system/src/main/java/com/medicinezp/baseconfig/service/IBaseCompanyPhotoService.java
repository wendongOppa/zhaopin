package com.medicinezp.baseconfig.service;

import java.util.List;
import com.medicinezp.baseconfig.domain.BaseCompanyPhoto;

/**
 * 公司相册Service接口
 * 
 * @author medicinezp
 * @date 2022-09-22
 */
public interface IBaseCompanyPhotoService 
{
    /**
     * 查询公司相册
     * 
     * @param id 公司相册主键
     * @return 公司相册
     */
    public BaseCompanyPhoto selectBaseCompanyPhotoById(Long id);

    /**
     * 查询公司相册列表
     * 
     * @param baseCompanyPhoto 公司相册
     * @return 公司相册集合
     */
    public List<BaseCompanyPhoto> selectBaseCompanyPhotoList(BaseCompanyPhoto baseCompanyPhoto);

    /**
     * 新增公司相册
     * 
     * @param baseCompanyPhoto 公司相册
     * @return 结果
     */
    public int insertBaseCompanyPhoto(BaseCompanyPhoto baseCompanyPhoto);

    /**
     * 修改公司相册
     * 
     * @param baseCompanyPhoto 公司相册
     * @return 结果
     */
    public int updateBaseCompanyPhoto(BaseCompanyPhoto baseCompanyPhoto);

    /**
     * 批量删除公司相册
     * 
     * @param ids 需要删除的公司相册主键集合
     * @return 结果
     */
    public int deleteBaseCompanyPhotoByIds(Long[] ids);

    /**
     * 删除公司相册信息
     * 
     * @param id 公司相册主键
     * @return 结果
     */
    public int deleteBaseCompanyPhotoById(Long id);
}
