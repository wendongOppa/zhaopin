package com.medicinezp.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.medicinezp.common.annotation.Log;
import com.medicinezp.common.core.controller.BaseController;
import com.medicinezp.common.core.domain.AjaxResult;
import com.medicinezp.common.enums.BusinessType;
import com.medicinezp.baseconfig.domain.BaseUserInviteApply;
import com.medicinezp.baseconfig.service.IBaseUserInviteApplyService;
import com.medicinezp.common.utils.poi.ExcelUtil;
import com.medicinezp.common.core.page.TableDataInfo;

/**
 * 邀请投递Controller
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@RestController
@RequestMapping("/baseconfig/apply")
public class BaseUserInviteApplyController extends BaseController
{
    @Autowired
    private IBaseUserInviteApplyService baseUserInviteApplyService;

    /**
     * 查询邀请投递列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:apply:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseUserInviteApply baseUserInviteApply)
    {
        startPage();
        List<BaseUserInviteApply> list = baseUserInviteApplyService.selectBaseUserInviteApplyList(baseUserInviteApply);
        return getDataTable(list);
    }

    /**
     * 导出邀请投递列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:apply:export')")
    @Log(title = "邀请投递", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseUserInviteApply baseUserInviteApply)
    {
        List<BaseUserInviteApply> list = baseUserInviteApplyService.selectBaseUserInviteApplyList(baseUserInviteApply);
        ExcelUtil<BaseUserInviteApply> util = new ExcelUtil<BaseUserInviteApply>(BaseUserInviteApply.class);
        util.exportExcel(response, list, "邀请投递数据");
    }

    /**
     * 获取邀请投递详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:apply:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(baseUserInviteApplyService.selectBaseUserInviteApplyById(id));
    }

    /**
     * 新增邀请投递
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:apply:add')")
    @Log(title = "邀请投递", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseUserInviteApply baseUserInviteApply)
    {
        return toAjax(baseUserInviteApplyService.insertBaseUserInviteApply(baseUserInviteApply));
    }

    /**
     * 修改邀请投递
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:apply:edit')")
    @Log(title = "邀请投递", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseUserInviteApply baseUserInviteApply)
    {
        return toAjax(baseUserInviteApplyService.updateBaseUserInviteApply(baseUserInviteApply));
    }

    /**
     * 删除邀请投递
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:apply:remove')")
    @Log(title = "邀请投递", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseUserInviteApplyService.deleteBaseUserInviteApplyByIds(ids));
    }
}
