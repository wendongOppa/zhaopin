package com.medicinezp.baseconfig.service;

import java.util.List;
import com.medicinezp.baseconfig.domain.BaseCompanyLogs;

/**
 * 公司修改日志Service接口
 * 
 * @author medicinezp
 * @date 2022-10-14
 */
public interface IBaseCompanyLogsService 
{
    /**
     * 查询公司修改日志
     * 
     * @param id 公司修改日志主键
     * @return 公司修改日志
     */
    public BaseCompanyLogs selectBaseCompanyLogsById(Long id);

    /**
     * 查询公司修改日志列表
     * 
     * @param baseCompanyLogs 公司修改日志
     * @return 公司修改日志集合
     */
    public List<BaseCompanyLogs> selectBaseCompanyLogsList(BaseCompanyLogs baseCompanyLogs);

    /**
     * 新增公司修改日志
     * 
     * @param baseCompanyLogs 公司修改日志
     * @return 结果
     */
    public int insertBaseCompanyLogs(BaseCompanyLogs baseCompanyLogs);

    /**
     * 修改公司修改日志
     * 
     * @param baseCompanyLogs 公司修改日志
     * @return 结果
     */
    public int updateBaseCompanyLogs(BaseCompanyLogs baseCompanyLogs);

    /**
     * 批量删除公司修改日志
     * 
     * @param ids 需要删除的公司修改日志主键集合
     * @return 结果
     */
    public int deleteBaseCompanyLogsByIds(Long[] ids);

    /**
     * 删除公司修改日志信息
     * 
     * @param id 公司修改日志主键
     * @return 结果
     */
    public int deleteBaseCompanyLogsById(Long id);
}
