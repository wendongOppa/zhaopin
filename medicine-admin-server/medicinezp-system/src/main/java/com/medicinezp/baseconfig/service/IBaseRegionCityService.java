package com.medicinezp.baseconfig.service;

import java.util.List;
import com.medicinezp.baseconfig.domain.BaseRegionCity;

/**
 * 城市Service接口
 * 
 * @author medicinezp
 * @date 2022-09-22
 */
public interface IBaseRegionCityService 
{
    /**
     * 查询城市
     * 
     * @param cityId 城市主键
     * @return 城市
     */
    public BaseRegionCity selectBaseRegionCityByCityId(Long cityId);

    /**
     * 查询城市列表
     * 
     * @param baseRegionCity 城市
     * @return 城市集合
     */
    public List<BaseRegionCity> selectBaseRegionCityList(BaseRegionCity baseRegionCity);

    /**
     * 新增城市
     * 
     * @param baseRegionCity 城市
     * @return 结果
     */
    public int insertBaseRegionCity(BaseRegionCity baseRegionCity);

    /**
     * 修改城市
     * 
     * @param baseRegionCity 城市
     * @return 结果
     */
    public int updateBaseRegionCity(BaseRegionCity baseRegionCity);

    /**
     * 批量删除城市
     * 
     * @param cityIds 需要删除的城市主键集合
     * @return 结果
     */
    public int deleteBaseRegionCityByCityIds(Long[] cityIds);

    /**
     * 删除城市信息
     * 
     * @param cityId 城市主键
     * @return 结果
     */
    public int deleteBaseRegionCityByCityId(Long cityId);
}
