package com.medicinezp.baseconfig.service.impl;

import java.util.List;
import com.medicinezp.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.medicinezp.baseconfig.mapper.BaseSearchHistoryMapper;
import com.medicinezp.baseconfig.domain.BaseSearchHistory;
import com.medicinezp.baseconfig.service.IBaseSearchHistoryService;

/**
 * 搜索记录Service业务层处理
 *
 * @author medicinezp
 * @date 2022-10-24
 */
@Service
public class BaseSearchHistoryServiceImpl implements IBaseSearchHistoryService
{
    @Autowired
    private BaseSearchHistoryMapper baseSearchHistoryMapper;

    /**
     * 查询搜索记录
     *
     * @param id 搜索记录主键
     * @return 搜索记录
     */
    @Override
    public BaseSearchHistory selectBaseSearchHistoryById(Long id)
    {
        return baseSearchHistoryMapper.selectBaseSearchHistoryById(id);
    }

    /**
     * 查询搜索记录列表
     *
     * @param baseSearchHistory 搜索记录
     * @return 搜索记录
     */
    @Override
    public List<BaseSearchHistory> selectBaseSearchHistoryList(BaseSearchHistory baseSearchHistory)
    {
        return baseSearchHistoryMapper.selectBaseSearchHistoryList(baseSearchHistory);
    }

    /**
     * 新增搜索记录
     *
     * @param baseSearchHistory 搜索记录
     * @return 结果
     */
    @Override
    public int insertBaseSearchHistory(BaseSearchHistory baseSearchHistory)
    {
        baseSearchHistory.setCreateTime(DateUtils.getNowDate());
        return baseSearchHistoryMapper.insertBaseSearchHistory(baseSearchHistory);
    }

    /**
     * 修改搜索记录
     *
     * @param baseSearchHistory 搜索记录
     * @return 结果
     */
    @Override
    public int updateBaseSearchHistory(BaseSearchHistory baseSearchHistory)
    {
        return baseSearchHistoryMapper.updateBaseSearchHistory(baseSearchHistory);
    }

    /**
     * 批量删除搜索记录
     *
     * @param ids 需要删除的搜索记录主键
     * @return 结果
     */
    @Override
    public int deleteBaseSearchHistoryByIds(Long[] ids)
    {
        return baseSearchHistoryMapper.deleteBaseSearchHistoryByIds(ids);
    }

    /**
     * 删除搜索记录信息
     *
     * @param id 搜索记录主键
     * @return 结果
     */
    @Override
    public int deleteBaseSearchHistoryById(Long id)
    {
        return baseSearchHistoryMapper.deleteBaseSearchHistoryById(id);
    }

    @Override
    public List<BaseSearchHistory> selectHotSearchHistoryData() {
        return baseSearchHistoryMapper.selectHotSearchHistoryData();
    }

    @Override
    public List<BaseSearchHistory> selectUserSearchHistoryList(BaseSearchHistory history) {
        return baseSearchHistoryMapper.selectUserSearchHistoryList(history);
    }

    @Override
    public List<BaseSearchHistory> selectSearchHelpList(BaseSearchHistory history) {
        return baseSearchHistoryMapper.selectSearchHelpList(history);
    }

    @Override
    public int deleteBaseSearchHistoryByUserId(Long userID) {
        return baseSearchHistoryMapper.deleteBaseSearchHistoryByUserId(userID);
    }
}
