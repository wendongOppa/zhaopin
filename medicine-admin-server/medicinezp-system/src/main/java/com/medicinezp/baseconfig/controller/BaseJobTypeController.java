package com.medicinezp.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.medicinezp.common.annotation.Log;
import com.medicinezp.common.core.controller.BaseController;
import com.medicinezp.common.core.domain.AjaxResult;
import com.medicinezp.common.enums.BusinessType;
import com.medicinezp.baseconfig.domain.BaseJobType;
import com.medicinezp.baseconfig.service.IBaseJobTypeService;
import com.medicinezp.common.utils.poi.ExcelUtil;
import com.medicinezp.common.core.page.TableDataInfo;

/**
 * 职位分类Controller
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@RestController
@RequestMapping("/baseconfig/jobtype")
public class BaseJobTypeController extends BaseController
{
    @Autowired
    private IBaseJobTypeService baseJobTypeService;

    /**
     * 查询职位分类列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:jobtype:list')")
    @GetMapping("/list")
    public AjaxResult list(BaseJobType baseJobType)
    {
        List<BaseJobType> list = baseJobTypeService.selectBaseJobTypeList(baseJobType);
        return AjaxResult.success(list);
    }

    /**
     * 导出职位分类列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:jobtype:export')")
    @Log(title = "职位分类", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseJobType baseJobType)
    {
        List<BaseJobType> list = baseJobTypeService.selectBaseJobTypeList(baseJobType);
        ExcelUtil<BaseJobType> util = new ExcelUtil<BaseJobType>(BaseJobType.class);
        util.exportExcel(response, list, "职位分类数据");
    }

    /**
     * 获取职位分类详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:jobtype:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(baseJobTypeService.selectBaseJobTypeById(id));
    }

    /**
     * 新增职位分类
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:jobtype:add')")
    @Log(title = "职位分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseJobType baseJobType)
    {
        return toAjax(baseJobTypeService.insertBaseJobType(baseJobType));
    }

    /**
     * 修改职位分类
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:jobtype:edit')")
    @Log(title = "职位分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseJobType baseJobType)
    {
        return toAjax(baseJobTypeService.updateBaseJobType(baseJobType));
    }

    /**
     * 删除职位分类
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:jobtype:remove')")
    @Log(title = "职位分类", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseJobTypeService.deleteBaseJobTypeByIds(ids));
    }
}
