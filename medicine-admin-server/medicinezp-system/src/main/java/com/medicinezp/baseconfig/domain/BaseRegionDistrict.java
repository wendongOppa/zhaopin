package com.medicinezp.baseconfig.domain;

import com.medicinezp.common.annotation.Excel;
import com.medicinezp.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 区县信息对象 base_region_district
 *
 * @author medicinezp
 * @date 2022-09-22
 */
public class BaseRegionDistrict extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 区县ID */
    private Long districtId;

    /** 区县名称 */
    @Excel(name = "区县名称")
    private String districtName;

    /** 城市ID */
    @Excel(name = "城市ID")
    private Long cityId;
    private String label;
    private String value;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setDistrictId(Long districtId)
    {
        this.districtId = districtId;
    }

    public Long getDistrictId()
    {
        return districtId;
    }
    public void setDistrictName(String districtName)
    {
        this.districtName = districtName;
    }

    public String getDistrictName()
    {
        return districtName;
    }
    public void setCityId(Long cityId)
    {
        this.cityId = cityId;
    }

    public Long getCityId()
    {
        return cityId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("districtId", getDistrictId())
            .append("districtName", getDistrictName())
            .append("cityId", getCityId())
            .append("updateTime", getUpdateTime())
            .append("createTime", getCreateTime())
            .toString();
    }
}
