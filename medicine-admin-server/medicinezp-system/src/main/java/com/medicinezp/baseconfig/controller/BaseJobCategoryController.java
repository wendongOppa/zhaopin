package com.medicinezp.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.medicinezp.baseconfig.domain.BaseJobType;
import com.medicinezp.baseconfig.service.IBaseJobTypeService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.medicinezp.common.annotation.Log;
import com.medicinezp.common.core.controller.BaseController;
import com.medicinezp.common.core.domain.AjaxResult;
import com.medicinezp.common.enums.BusinessType;
import com.medicinezp.baseconfig.domain.BaseJobCategory;
import com.medicinezp.baseconfig.service.IBaseJobCategoryService;
import com.medicinezp.common.utils.poi.ExcelUtil;
import com.medicinezp.common.core.page.TableDataInfo;

/**
 * 岗位类型Controller
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@RestController
@RequestMapping("/baseconfig/jobcategory")
public class BaseJobCategoryController extends BaseController
{
    @Autowired
    private IBaseJobCategoryService baseJobCategoryService;

    @Autowired
    private IBaseJobTypeService baseJobTypeService;

    /**
     * 查询岗位类型列表
     */
    @GetMapping("/list")
    public AjaxResult list(BaseJobCategory baseJobCategory)
    {
        List<BaseJobCategory> list = baseJobCategoryService.selectBaseJobCategoryList(baseJobCategory);
        return AjaxResult.success(list);
    }

    /**
     * 导出岗位类型列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:jobcategory:export')")
    @Log(title = "岗位类型", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseJobCategory baseJobCategory)
    {
        List<BaseJobCategory> list = baseJobCategoryService.selectBaseJobCategoryList(baseJobCategory);
        ExcelUtil<BaseJobCategory> util = new ExcelUtil<BaseJobCategory>(BaseJobCategory.class);
        util.exportExcel(response, list, "岗位类型数据");
    }

    /**
     * 获取岗位类型详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:jobcategory:query')")
    @GetMapping(value = "/{categoryId}")
    public AjaxResult getInfo(@PathVariable("categoryId") Long categoryId)
    {
        return AjaxResult.success(baseJobCategoryService.selectBaseJobCategoryByCategoryId(categoryId));
    }

    /**
     * 新增岗位类型
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:jobcategory:add')")
    @Log(title = "岗位类型", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseJobCategory baseJobCategory)
    {
        return toAjax(baseJobCategoryService.insertBaseJobCategory(baseJobCategory));
    }

    /**
     * 修改岗位类型
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:jobcategory:edit')")
    @Log(title = "岗位类型", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseJobCategory baseJobCategory)
    {
        return toAjax(baseJobCategoryService.updateBaseJobCategory(baseJobCategory));
    }

    /**
     * 删除岗位类型
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:jobcategory:remove')")
    @Log(title = "岗位类型", businessType = BusinessType.DELETE)
	@DeleteMapping("/{categoryIds}")
    public AjaxResult remove(@PathVariable Long categoryIds)
    {
        BaseJobType type=new BaseJobType();
        type.setCategoryId(categoryIds);
        List<BaseJobType>  typeList=baseJobTypeService.selectBaseJobTypeList(type);
        if(typeList.size()>0){
            return AjaxResult.error("职位分类下还存在子分类！");
        }
        int result=baseJobCategoryService.deleteBaseJobCategoryByCategoryId(categoryIds);
        return toAjax(result);
    }
}
