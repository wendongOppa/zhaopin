package com.medicinezp.baseconfig.controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.medicinezp.common.core.domain.entity.SysUser;
import com.medicinezp.common.utils.SecurityUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.medicinezp.common.annotation.Log;
import com.medicinezp.common.core.controller.BaseController;
import com.medicinezp.common.core.domain.AjaxResult;
import com.medicinezp.common.enums.BusinessType;
import com.medicinezp.baseconfig.domain.BaseJobs;
import com.medicinezp.baseconfig.service.IBaseJobsService;
import com.medicinezp.common.utils.poi.ExcelUtil;
import com.medicinezp.common.core.page.TableDataInfo;

/**
 * 岗位Controller
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@RestController
@RequestMapping("/baseconfig/jobs")
public class BaseJobsController extends BaseController
{
    @Autowired
    private IBaseJobsService baseJobsService;

    /**
     * 查询岗位列表
     */
    @GetMapping("/list")
    public TableDataInfo list(BaseJobs baseJobs)
    {
        startPage();
        List<BaseJobs> list = baseJobsService.selectBaseJobsList(baseJobs);
        return getDataTable(list);
    }

    @GetMapping("/queryAllJobList")
    public TableDataInfo queryAllJobList(BaseJobs baseJobs)
    {
        startPage();
        List<BaseJobs> list = baseJobsService.selectAllJobsList(baseJobs);
        return getDataTable(list);
    }

    @GetMapping("/queryAllApproveJobList")
    public TableDataInfo queryAllApproveJobList(BaseJobs baseJobs)
    {
        startPage();
        List<BaseJobs> list = baseJobsService.selectAllJobsList(baseJobs);
        return getDataTable(list);
    }

    /**
     * 导出岗位列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:jobs:export')")
    @Log(title = "岗位", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseJobs baseJobs)
    {
        List<BaseJobs> list = baseJobsService.selectBaseJobsList(baseJobs);
        ExcelUtil<BaseJobs> util = new ExcelUtil<BaseJobs>(BaseJobs.class);
        util.exportExcel(response, list, "岗位数据");
    }

    /**
     * 获取岗位详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(baseJobsService.selectBaseJobsById(id));
    }

    /**
     * 新增岗位
     */
    @Log(title = "岗位", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseJobs baseJobs)
    {
        return toAjax(baseJobsService.insertBaseJobs(baseJobs));
    }

    /**
     * 修改岗位
     */
    @Log(title = "岗位", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseJobs baseJobs)
    {
        return toAjax(baseJobsService.updateBaseJobs(baseJobs));
    }

    /**
     * 删除岗位
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:jobs:remove')")
    @Log(title = "岗位", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseJobsService.deleteBaseJobsByIds(ids));
    }
}
