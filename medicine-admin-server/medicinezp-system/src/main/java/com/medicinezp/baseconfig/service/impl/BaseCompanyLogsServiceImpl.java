package com.medicinezp.baseconfig.service.impl;

import java.util.List;
import com.medicinezp.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.medicinezp.baseconfig.mapper.BaseCompanyLogsMapper;
import com.medicinezp.baseconfig.domain.BaseCompanyLogs;
import com.medicinezp.baseconfig.service.IBaseCompanyLogsService;

/**
 * 公司修改日志Service业务层处理
 *
 * @author medicinezp
 * @date 2022-10-14
 */
@Service
public class BaseCompanyLogsServiceImpl implements IBaseCompanyLogsService
{
    @Autowired
    private BaseCompanyLogsMapper baseCompanyLogsMapper;

    /**
     * 查询公司修改日志
     *
     * @param id 公司修改日志主键
     * @return 公司修改日志
     */
    @Override
    public BaseCompanyLogs selectBaseCompanyLogsById(Long id)
    {
        return baseCompanyLogsMapper.selectBaseCompanyLogsById(id);
    }

    /**
     * 查询公司修改日志列表
     *
     * @param baseCompanyLogs 公司修改日志
     * @return 公司修改日志
     */
    @Override
    public List<BaseCompanyLogs> selectBaseCompanyLogsList(BaseCompanyLogs baseCompanyLogs)
    {
        return baseCompanyLogsMapper.selectBaseCompanyLogsList(baseCompanyLogs);
    }

    /**
     * 新增公司修改日志
     *
     * @param baseCompanyLogs 公司修改日志
     * @return 结果
     */
    @Override
    public int insertBaseCompanyLogs(BaseCompanyLogs baseCompanyLogs)
    {
        baseCompanyLogs.setCreateTime(DateUtils.getNowDate());
        return baseCompanyLogsMapper.insertBaseCompanyLogs(baseCompanyLogs);
    }

    /**
     * 修改公司修改日志
     *
     * @param baseCompanyLogs 公司修改日志
     * @return 结果
     */
    @Override
    public int updateBaseCompanyLogs(BaseCompanyLogs baseCompanyLogs)
    {
        return baseCompanyLogsMapper.updateBaseCompanyLogs(baseCompanyLogs);
    }

    /**
     * 批量删除公司修改日志
     *
     * @param ids 需要删除的公司修改日志主键
     * @return 结果
     */
    @Override
    public int deleteBaseCompanyLogsByIds(Long[] ids)
    {
        return baseCompanyLogsMapper.deleteBaseCompanyLogsByIds(ids);
    }

    /**
     * 删除公司修改日志信息
     *
     * @param id 公司修改日志主键
     * @return 结果
     */
    @Override
    public int deleteBaseCompanyLogsById(Long id)
    {
        return baseCompanyLogsMapper.deleteBaseCompanyLogsById(id);
    }
}
