package com.medicinezp.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.medicinezp.common.annotation.Log;
import com.medicinezp.common.core.controller.BaseController;
import com.medicinezp.common.core.domain.AjaxResult;
import com.medicinezp.common.enums.BusinessType;
import com.medicinezp.baseconfig.domain.BaseRegionDistrict;
import com.medicinezp.baseconfig.service.IBaseRegionDistrictService;
import com.medicinezp.common.utils.poi.ExcelUtil;
import com.medicinezp.common.core.page.TableDataInfo;

/**
 * 区县信息Controller
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@RestController
@RequestMapping("/baseconfig/district")
public class BaseRegionDistrictController extends BaseController
{
    @Autowired
    private IBaseRegionDistrictService baseRegionDistrictService;

    /**
     * 查询区县信息列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:district:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseRegionDistrict baseRegionDistrict)
    {
        startPage();
        List<BaseRegionDistrict> list = baseRegionDistrictService.selectBaseRegionDistrictList(baseRegionDistrict);
        return getDataTable(list);
    }

    /**
     * 导出区县信息列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:district:export')")
    @Log(title = "区县信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseRegionDistrict baseRegionDistrict)
    {
        List<BaseRegionDistrict> list = baseRegionDistrictService.selectBaseRegionDistrictList(baseRegionDistrict);
        ExcelUtil<BaseRegionDistrict> util = new ExcelUtil<BaseRegionDistrict>(BaseRegionDistrict.class);
        util.exportExcel(response, list, "区县信息数据");
    }

    /**
     * 获取区县信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:district:query')")
    @GetMapping(value = "/{districtId}")
    public AjaxResult getInfo(@PathVariable("districtId") Long districtId)
    {
        return AjaxResult.success(baseRegionDistrictService.selectBaseRegionDistrictByDistrictId(districtId));
    }

    /**
     * 新增区县信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:district:add')")
    @Log(title = "区县信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseRegionDistrict baseRegionDistrict)
    {
        return toAjax(baseRegionDistrictService.insertBaseRegionDistrict(baseRegionDistrict));
    }

    /**
     * 修改区县信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:district:edit')")
    @Log(title = "区县信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseRegionDistrict baseRegionDistrict)
    {
        return toAjax(baseRegionDistrictService.updateBaseRegionDistrict(baseRegionDistrict));
    }

    /**
     * 删除区县信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:district:remove')")
    @Log(title = "区县信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{districtIds}")
    public AjaxResult remove(@PathVariable Long[] districtIds)
    {
        return toAjax(baseRegionDistrictService.deleteBaseRegionDistrictByDistrictIds(districtIds));
    }
}
