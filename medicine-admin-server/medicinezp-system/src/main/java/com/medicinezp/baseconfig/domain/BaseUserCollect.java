package com.medicinezp.baseconfig.domain;

import com.medicinezp.common.annotation.Excel;
import com.medicinezp.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 用户收藏对象 base_user_collect
 *
 * @author medicinezp
 * @date 2022-09-22
 */
public class BaseUserCollect extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 收藏类型01职位02公司 */
    @Excel(name = "收藏类型01职位02公司")
    private String type;

    /** 岗位id */
    @Excel(name = "岗位id")
    private Long collectId;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }
    public void setType(String type)
    {
        this.type = type;
    }

    public String getType()
    {
        return type;
    }
    public void setCollectId(Long collectId)
    {
        this.collectId = collectId;
    }

    public Long getCollectId()
    {
        return collectId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("type", getType())
            .append("collectId", getCollectId())
            .append("createTime", getCreateTime())
            .toString();
    }
}
