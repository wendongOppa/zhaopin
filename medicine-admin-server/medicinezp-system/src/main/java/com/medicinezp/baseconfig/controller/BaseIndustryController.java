package com.medicinezp.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.medicinezp.common.annotation.Log;
import com.medicinezp.common.core.controller.BaseController;
import com.medicinezp.common.core.domain.AjaxResult;
import com.medicinezp.common.enums.BusinessType;
import com.medicinezp.baseconfig.domain.BaseIndustry;
import com.medicinezp.baseconfig.service.IBaseIndustryService;
import com.medicinezp.common.utils.poi.ExcelUtil;
import com.medicinezp.common.core.page.TableDataInfo;

/**
 * 行业信息Controller
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@RestController
@RequestMapping("/baseconfig/industry")
public class BaseIndustryController extends BaseController
{
    @Autowired
    private IBaseIndustryService baseIndustryService;

    /**
     * 查询行业信息列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:industry:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseIndustry baseIndustry)
    {
        startPage();
        List<BaseIndustry> list = baseIndustryService.selectBaseIndustryList(baseIndustry);
        return getDataTable(list);
    }

    @GetMapping("/getAllIndustryList")
    public AjaxResult getAllIndustryList(BaseIndustry baseIndustry)
    {
        List<BaseIndustry> list = baseIndustryService.selectBaseIndustryList(baseIndustry);
        return AjaxResult.success(list);
    }

    /**
     * 导出行业信息列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:industry:export')")
    @Log(title = "行业信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseIndustry baseIndustry)
    {
        List<BaseIndustry> list = baseIndustryService.selectBaseIndustryList(baseIndustry);
        ExcelUtil<BaseIndustry> util = new ExcelUtil<BaseIndustry>(BaseIndustry.class);
        util.exportExcel(response, list, "行业信息数据");
    }

    /**
     * 获取行业信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:industry:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(baseIndustryService.selectBaseIndustryById(id));
    }

    /**
     * 新增行业信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:industry:add')")
    @Log(title = "行业信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseIndustry baseIndustry)
    {
        BaseIndustry param=new BaseIndustry();
        param.setName(baseIndustry.getName());
        List<BaseIndustry> list=baseIndustryService.selectBaseIndustryList(param);
        if(list.size()>0){
            return AjaxResult.error(baseIndustry.getName()+"已经存在");
        }
        return toAjax(baseIndustryService.insertBaseIndustry(baseIndustry));
    }

    /**
     * 修改行业信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:industry:edit')")
    @Log(title = "行业信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseIndustry baseIndustry)
    {
        return toAjax(baseIndustryService.updateBaseIndustry(baseIndustry));
    }

    /**
     * 删除行业信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:industry:remove')")
    @Log(title = "行业信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseIndustryService.deleteBaseIndustryByIds(ids));
    }
}
