package com.medicinezp.baseconfig.mapper;

import java.util.List;
import com.medicinezp.baseconfig.domain.BaseUserBrowser;

/**
 * 用户浏览记录Mapper接口
 * 
 * @author medicinezp
 * @date 2022-09-22
 */
public interface BaseUserBrowserMapper 
{
    /**
     * 查询用户浏览记录
     * 
     * @param id 用户浏览记录主键
     * @return 用户浏览记录
     */
    public BaseUserBrowser selectBaseUserBrowserById(Long id);

    /**
     * 查询用户浏览记录列表
     * 
     * @param baseUserBrowser 用户浏览记录
     * @return 用户浏览记录集合
     */
    public List<BaseUserBrowser> selectBaseUserBrowserList(BaseUserBrowser baseUserBrowser);

    /**
     * 新增用户浏览记录
     * 
     * @param baseUserBrowser 用户浏览记录
     * @return 结果
     */
    public int insertBaseUserBrowser(BaseUserBrowser baseUserBrowser);

    /**
     * 修改用户浏览记录
     * 
     * @param baseUserBrowser 用户浏览记录
     * @return 结果
     */
    public int updateBaseUserBrowser(BaseUserBrowser baseUserBrowser);

    /**
     * 删除用户浏览记录
     * 
     * @param id 用户浏览记录主键
     * @return 结果
     */
    public int deleteBaseUserBrowserById(Long id);

    /**
     * 批量删除用户浏览记录
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseUserBrowserByIds(Long[] ids);
}
