package com.medicinezp.baseconfig.service;

import java.util.List;
import com.medicinezp.baseconfig.domain.BaseUserJobApply;

/**
 * 用户投递简历申请Service接口
 *
 * @author medicinezp
 * @date 2022-09-22
 */
public interface IBaseUserJobApplyService
{
    /**
     * 查询用户投递简历申请
     *
     * @param id 用户投递简历申请主键
     * @return 用户投递简历申请
     */
    public BaseUserJobApply selectBaseUserJobApplyById(Long id);

    /**
     * 查询用户投递简历申请列表
     *
     * @param baseUserJobApply 用户投递简历申请
     * @return 用户投递简历申请集合
     */
    public List<BaseUserJobApply> selectBaseUserJobApplyList(BaseUserJobApply baseUserJobApply);

    /**
     * 新增用户投递简历申请
     *
     * @param baseUserJobApply 用户投递简历申请
     * @return 结果
     */
    public int insertBaseUserJobApply(BaseUserJobApply baseUserJobApply);

    /**
     * 修改用户投递简历申请
     *
     * @param baseUserJobApply 用户投递简历申请
     * @return 结果
     */
    public int updateBaseUserJobApply(BaseUserJobApply baseUserJobApply);

    /**
     * 批量删除用户投递简历申请
     *
     * @param ids 需要删除的用户投递简历申请主键集合
     * @return 结果
     */
    public int deleteBaseUserJobApplyByIds(Long[] ids);

    /**
     * 删除用户投递简历申请信息
     *
     * @param id 用户投递简历申请主键
     * @return 结果
     */
    public int deleteBaseUserJobApplyById(Long id);

    List<BaseUserJobApply> selectCompanyUserJobApplyList(BaseUserJobApply jobApply);

    int approveUserJobApply(BaseUserJobApply baseUserJobApply);

    List<BaseUserJobApply> selectAllUserJobApplyList(BaseUserJobApply baseUserJobApply);
}
