package com.medicinezp.baseconfig.service.impl;

import java.util.List;
import com.medicinezp.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.medicinezp.baseconfig.mapper.BaseAdvertisingMapper;
import com.medicinezp.baseconfig.domain.BaseAdvertising;
import com.medicinezp.baseconfig.service.IBaseAdvertisingService;

/**
 * 企业端广告Service业务层处理
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@Service
public class BaseAdvertisingServiceImpl implements IBaseAdvertisingService
{
    @Autowired
    private BaseAdvertisingMapper baseAdvertisingMapper;

    /**
     * 查询企业端广告
     *
     * @param id 企业端广告主键
     * @return 企业端广告
     */
    @Override
    public BaseAdvertising selectBaseAdvertisingById(Long id)
    {
        return baseAdvertisingMapper.selectBaseAdvertisingById(id);
    }

    /**
     * 查询企业端广告列表
     *
     * @param baseAdvertising 企业端广告
     * @return 企业端广告
     */
    @Override
    public List<BaseAdvertising> selectBaseAdvertisingList(BaseAdvertising baseAdvertising)
    {
        return baseAdvertisingMapper.selectBaseAdvertisingList(baseAdvertising);
    }

    /**
     * 新增企业端广告
     *
     * @param baseAdvertising 企业端广告
     * @return 结果
     */
    @Override
    public int insertBaseAdvertising(BaseAdvertising baseAdvertising)
    {
        baseAdvertising.setCreateTime(DateUtils.getNowDate());
        return baseAdvertisingMapper.insertBaseAdvertising(baseAdvertising);
    }

    /**
     * 修改企业端广告
     *
     * @param baseAdvertising 企业端广告
     * @return 结果
     */
    @Override
    public int updateBaseAdvertising(BaseAdvertising baseAdvertising)
    {
        baseAdvertising.setUpdateTime(DateUtils.getNowDate());
        return baseAdvertisingMapper.updateBaseAdvertising(baseAdvertising);
    }

    /**
     * 批量删除企业端广告
     *
     * @param ids 需要删除的企业端广告主键
     * @return 结果
     */
    @Override
    public int deleteBaseAdvertisingByIds(Long[] ids)
    {
        return baseAdvertisingMapper.deleteBaseAdvertisingByIds(ids);
    }

    /**
     * 删除企业端广告信息
     *
     * @param id 企业端广告主键
     * @return 结果
     */
    @Override
    public int deleteBaseAdvertisingById(Long id)
    {
        return baseAdvertisingMapper.deleteBaseAdvertisingById(id);
    }
}
