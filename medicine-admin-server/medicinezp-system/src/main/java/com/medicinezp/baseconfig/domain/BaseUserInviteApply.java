package com.medicinezp.baseconfig.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.medicinezp.common.annotation.Excel;
import com.medicinezp.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 邀请投递对象 base_user_invite_apply
 *
 * @author medicinezp
 * @date 2022-09-22
 */
public class BaseUserInviteApply extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 邀请人ID */
    @Excel(name = "邀请人ID")
    private Long userId;

    /** 岗位ID */
    @Excel(name = "岗位ID")
    private Long jobId;

    /** 岗位名称 */
    @Excel(name = "岗位名称")
    private String jobName;

    /** 邀请时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "邀请时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date inviteTime;

    /** 邀请状态 01已邀请02已投递 */
    @Excel(name = "邀请状态 01已邀请02已投递")
    private String inviteStatus;

    /** 邀请人ID */
    @Excel(name = "邀请人ID")
    private Long inviteUserId;



    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }
    public void setJobId(Long jobId)
    {
        this.jobId = jobId;
    }

    public Long getJobId()
    {
        return jobId;
    }
    public void setJobName(String jobName)
    {
        this.jobName = jobName;
    }

    public String getJobName()
    {
        return jobName;
    }
    public void setInviteTime(Date inviteTime)
    {
        this.inviteTime = inviteTime;
    }

    public Date getInviteTime()
    {
        return inviteTime;
    }
    public void setInviteStatus(String inviteStatus)
    {
        this.inviteStatus = inviteStatus;
    }

    public String getInviteStatus()
    {
        return inviteStatus;
    }
    public void setInviteUserId(Long inviteUserId)
    {
        this.inviteUserId = inviteUserId;
    }

    public Long getInviteUserId()
    {
        return inviteUserId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("jobId", getJobId())
            .append("jobName", getJobName())
            .append("inviteTime", getInviteTime())
            .append("inviteStatus", getInviteStatus())
            .append("inviteUserId", getInviteUserId())
            .toString();
    }
}
