package com.medicinezp.baseconfig.mapper;

import java.util.List;
import com.medicinezp.baseconfig.domain.BaseJobCategory;

/**
 * 岗位类型Mapper接口
 * 
 * @author medicinezp
 * @date 2022-09-22
 */
public interface BaseJobCategoryMapper 
{
    /**
     * 查询岗位类型
     * 
     * @param categoryId 岗位类型主键
     * @return 岗位类型
     */
    public BaseJobCategory selectBaseJobCategoryByCategoryId(Long categoryId);

    /**
     * 查询岗位类型列表
     * 
     * @param baseJobCategory 岗位类型
     * @return 岗位类型集合
     */
    public List<BaseJobCategory> selectBaseJobCategoryList(BaseJobCategory baseJobCategory);

    /**
     * 新增岗位类型
     * 
     * @param baseJobCategory 岗位类型
     * @return 结果
     */
    public int insertBaseJobCategory(BaseJobCategory baseJobCategory);

    /**
     * 修改岗位类型
     * 
     * @param baseJobCategory 岗位类型
     * @return 结果
     */
    public int updateBaseJobCategory(BaseJobCategory baseJobCategory);

    /**
     * 删除岗位类型
     * 
     * @param categoryId 岗位类型主键
     * @return 结果
     */
    public int deleteBaseJobCategoryByCategoryId(Long categoryId);

    /**
     * 批量删除岗位类型
     * 
     * @param categoryIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseJobCategoryByCategoryIds(Long[] categoryIds);
}
