package com.medicinezp.baseconfig.controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.medicinezp.baseconfig.domain.BaseCompany;
import com.medicinezp.baseconfig.service.IBaseCompanyService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.medicinezp.common.annotation.Log;
import com.medicinezp.common.core.controller.BaseController;
import com.medicinezp.common.core.domain.AjaxResult;
import com.medicinezp.common.enums.BusinessType;
import com.medicinezp.baseconfig.domain.BaseBenchmarkCompany;
import com.medicinezp.baseconfig.service.IBaseBenchmarkCompanyService;
import com.medicinezp.common.utils.poi.ExcelUtil;
import com.medicinezp.common.core.page.TableDataInfo;

/**
 * 标杆企业Controller
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@RestController
@RequestMapping("/baseconfig/benchmark")
public class BaseBenchmarkCompanyController extends BaseController
{
    @Autowired
    private IBaseBenchmarkCompanyService baseBenchmarkCompanyService;

    @Autowired
    private IBaseCompanyService baseCompanyService;

    /**
     * 查询标杆企业列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:benchmark:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseCompany company)
    {
        startPage();
        List<BaseCompany> list = baseCompanyService.selectBrenchmarkCompanyList(company);
        return getDataTable(list);
    }

    @GetMapping("/queryCompanyData")
    public AjaxResult queryCompanyData(@RequestParam(value="name",required = true) String name)
    {
        List<BaseCompany> list = baseCompanyService.selectAvailableCompanyList(name);
        return AjaxResult.success(list);
    }

    /**
     * 导出标杆企业列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:benchmark:export')")
    @Log(title = "标杆企业", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseBenchmarkCompany baseBenchmarkCompany)
    {
        List<BaseBenchmarkCompany> list = baseBenchmarkCompanyService.selectBaseBenchmarkCompanyList(baseBenchmarkCompany);
        ExcelUtil<BaseBenchmarkCompany> util = new ExcelUtil<BaseBenchmarkCompany>(BaseBenchmarkCompany.class);
        util.exportExcel(response, list, "标杆企业数据");
    }

    /**
     * 获取标杆企业详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:benchmark:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(baseBenchmarkCompanyService.selectBaseBenchmarkCompanyById(id));
    }

    /**
     * 新增标杆企业
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:benchmark:add')")
    @Log(title = "标杆企业", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseBenchmarkCompany baseBenchmarkCompany)
    {
        BaseCompany company= baseCompanyService.selectBaseCompanyById(baseBenchmarkCompany.getCompanyId());
        if(company==null){
            return AjaxResult.error("您添加的公司不存在");
        }
        BaseBenchmarkCompany param=new BaseBenchmarkCompany();
        param.setCompanyId(baseBenchmarkCompany.getCompanyId());
        List<BaseBenchmarkCompany> list=baseBenchmarkCompanyService.selectBaseBenchmarkCompanyList(param);
        if(list.size()>0){
            return AjaxResult.error("您添加的公司已经是标杆企业");
        }
        baseBenchmarkCompany.setCompanyName(company.getCompanyName());
        return toAjax(baseBenchmarkCompanyService.insertBaseBenchmarkCompany(baseBenchmarkCompany));
    }

    /**
     * 修改标杆企业
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:benchmark:edit')")
    @Log(title = "标杆企业", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseBenchmarkCompany baseBenchmarkCompany)
    {
        return toAjax(baseBenchmarkCompanyService.updateBaseBenchmarkCompany(baseBenchmarkCompany));
    }

    /**
     * 删除标杆企业
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:benchmark:remove')")
    @Log(title = "标杆企业", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseBenchmarkCompanyService.deleteBaseBenchmarkCompanyByIds(ids));
    }
}
