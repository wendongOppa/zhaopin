package com.medicinezp.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.medicinezp.common.annotation.Log;
import com.medicinezp.common.core.controller.BaseController;
import com.medicinezp.common.core.domain.AjaxResult;
import com.medicinezp.common.enums.BusinessType;
import com.medicinezp.baseconfig.domain.BaseUserIntention;
import com.medicinezp.baseconfig.service.IBaseUserIntentionService;
import com.medicinezp.common.utils.poi.ExcelUtil;
import com.medicinezp.common.core.page.TableDataInfo;

/**
 * 求职意向Controller
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@RestController
@RequestMapping("/baseconfig/intention")
public class BaseUserIntentionController extends BaseController
{
    @Autowired
    private IBaseUserIntentionService baseUserIntentionService;

    /**
     * 查询求职意向列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:intention:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseUserIntention baseUserIntention)
    {
        startPage();
        List<BaseUserIntention> list = baseUserIntentionService.selectBaseUserIntentionList(baseUserIntention);
        return getDataTable(list);
    }

    /**
     * 导出求职意向列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:intention:export')")
    @Log(title = "求职意向", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseUserIntention baseUserIntention)
    {
        List<BaseUserIntention> list = baseUserIntentionService.selectBaseUserIntentionList(baseUserIntention);
        ExcelUtil<BaseUserIntention> util = new ExcelUtil<BaseUserIntention>(BaseUserIntention.class);
        util.exportExcel(response, list, "求职意向数据");
    }

    /**
     * 获取求职意向详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:intention:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(baseUserIntentionService.selectBaseUserIntentionById(id));
    }

    /**
     * 新增求职意向
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:intention:add')")
    @Log(title = "求职意向", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseUserIntention baseUserIntention)
    {
        return toAjax(baseUserIntentionService.insertBaseUserIntention(baseUserIntention));
    }

    /**
     * 修改求职意向
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:intention:edit')")
    @Log(title = "求职意向", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseUserIntention baseUserIntention)
    {
        return toAjax(baseUserIntentionService.updateBaseUserIntention(baseUserIntention));
    }

    /**
     * 删除求职意向
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:intention:remove')")
    @Log(title = "求职意向", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseUserIntentionService.deleteBaseUserIntentionByIds(ids));
    }
}
