package com.medicinezp.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.medicinezp.common.annotation.Log;
import com.medicinezp.common.core.controller.BaseController;
import com.medicinezp.common.core.domain.AjaxResult;
import com.medicinezp.common.enums.BusinessType;
import com.medicinezp.baseconfig.domain.BaseUserJobApply;
import com.medicinezp.baseconfig.service.IBaseUserJobApplyService;
import com.medicinezp.common.utils.poi.ExcelUtil;
import com.medicinezp.common.core.page.TableDataInfo;

/**
 * 用户投递简历申请Controller
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@RestController
@RequestMapping("/baseconfig/jobapply")
public class BaseUserJobApplyController extends BaseController
{
    @Autowired
    private IBaseUserJobApplyService baseUserJobApplyService;

    /**
     * 查询用户投递简历申请列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:jobapply:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseUserJobApply baseUserJobApply)
    {
        startPage();
        List<BaseUserJobApply> list = baseUserJobApplyService.selectAllUserJobApplyList(baseUserJobApply);
        return getDataTable(list);
    }

    /**
     * 导出用户投递简历申请列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:jobapply:export')")
    @Log(title = "用户投递简历申请", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseUserJobApply baseUserJobApply)
    {
        List<BaseUserJobApply> list = baseUserJobApplyService.selectBaseUserJobApplyList(baseUserJobApply);
        ExcelUtil<BaseUserJobApply> util = new ExcelUtil<BaseUserJobApply>(BaseUserJobApply.class);
        util.exportExcel(response, list, "用户投递简历申请数据");
    }

    /**
     * 获取用户投递简历申请详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:jobapply:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(baseUserJobApplyService.selectBaseUserJobApplyById(id));
    }

    /**
     * 新增用户投递简历申请
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:jobapply:add')")
    @Log(title = "用户投递简历申请", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseUserJobApply baseUserJobApply)
    {
        return toAjax(baseUserJobApplyService.insertBaseUserJobApply(baseUserJobApply));
    }

    /**
     * 修改用户投递简历申请
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:jobapply:edit')")
    @Log(title = "用户投递简历申请", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseUserJobApply baseUserJobApply)
    {
        return toAjax(baseUserJobApplyService.updateBaseUserJobApply(baseUserJobApply));
    }

    /**
     * 删除用户投递简历申请
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:jobapply:remove')")
    @Log(title = "用户投递简历申请", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseUserJobApplyService.deleteBaseUserJobApplyByIds(ids));
    }
}
