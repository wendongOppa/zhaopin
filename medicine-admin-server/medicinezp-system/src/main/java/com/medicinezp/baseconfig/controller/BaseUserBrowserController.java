package com.medicinezp.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.medicinezp.common.annotation.Log;
import com.medicinezp.common.core.controller.BaseController;
import com.medicinezp.common.core.domain.AjaxResult;
import com.medicinezp.common.enums.BusinessType;
import com.medicinezp.baseconfig.domain.BaseUserBrowser;
import com.medicinezp.baseconfig.service.IBaseUserBrowserService;
import com.medicinezp.common.utils.poi.ExcelUtil;
import com.medicinezp.common.core.page.TableDataInfo;

/**
 * 用户浏览记录Controller
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@RestController
@RequestMapping("/baseconfig/browser")
public class BaseUserBrowserController extends BaseController
{
    @Autowired
    private IBaseUserBrowserService baseUserBrowserService;

    /**
     * 查询用户浏览记录列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:browser:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseUserBrowser baseUserBrowser)
    {
        startPage();
        List<BaseUserBrowser> list = baseUserBrowserService.selectBaseUserBrowserList(baseUserBrowser);
        return getDataTable(list);
    }

    /**
     * 导出用户浏览记录列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:browser:export')")
    @Log(title = "用户浏览记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseUserBrowser baseUserBrowser)
    {
        List<BaseUserBrowser> list = baseUserBrowserService.selectBaseUserBrowserList(baseUserBrowser);
        ExcelUtil<BaseUserBrowser> util = new ExcelUtil<BaseUserBrowser>(BaseUserBrowser.class);
        util.exportExcel(response, list, "用户浏览记录数据");
    }

    /**
     * 获取用户浏览记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:browser:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(baseUserBrowserService.selectBaseUserBrowserById(id));
    }

    /**
     * 新增用户浏览记录
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:browser:add')")
    @Log(title = "用户浏览记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseUserBrowser baseUserBrowser)
    {
        return toAjax(baseUserBrowserService.insertBaseUserBrowser(baseUserBrowser));
    }

    /**
     * 修改用户浏览记录
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:browser:edit')")
    @Log(title = "用户浏览记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseUserBrowser baseUserBrowser)
    {
        return toAjax(baseUserBrowserService.updateBaseUserBrowser(baseUserBrowser));
    }

    /**
     * 删除用户浏览记录
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:browser:remove')")
    @Log(title = "用户浏览记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseUserBrowserService.deleteBaseUserBrowserByIds(ids));
    }
}
