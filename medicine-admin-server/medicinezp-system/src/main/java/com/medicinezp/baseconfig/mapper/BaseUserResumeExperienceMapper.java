package com.medicinezp.baseconfig.mapper;

import java.util.List;
import com.medicinezp.baseconfig.domain.BaseUserResumeExperience;

/**
 * 工作经历Mapper接口
 * 
 * @author medicinezp
 * @date 2022-09-22
 */
public interface BaseUserResumeExperienceMapper 
{
    /**
     * 查询工作经历
     * 
     * @param id 工作经历主键
     * @return 工作经历
     */
    public BaseUserResumeExperience selectBaseUserResumeExperienceById(Long id);

    /**
     * 查询工作经历列表
     * 
     * @param baseUserResumeExperience 工作经历
     * @return 工作经历集合
     */
    public List<BaseUserResumeExperience> selectBaseUserResumeExperienceList(BaseUserResumeExperience baseUserResumeExperience);

    /**
     * 新增工作经历
     * 
     * @param baseUserResumeExperience 工作经历
     * @return 结果
     */
    public int insertBaseUserResumeExperience(BaseUserResumeExperience baseUserResumeExperience);

    /**
     * 修改工作经历
     * 
     * @param baseUserResumeExperience 工作经历
     * @return 结果
     */
    public int updateBaseUserResumeExperience(BaseUserResumeExperience baseUserResumeExperience);

    /**
     * 删除工作经历
     * 
     * @param id 工作经历主键
     * @return 结果
     */
    public int deleteBaseUserResumeExperienceById(Long id);

    /**
     * 批量删除工作经历
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseUserResumeExperienceByIds(Long[] ids);
}
