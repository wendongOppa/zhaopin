package com.medicinezp.baseconfig.service;

import java.util.List;
import com.medicinezp.baseconfig.domain.BaseCompanyAccountReal;

/**
 * 公司招聘人员Service接口
 *
 * @author medicinezp
 * @date 2022-09-22
 */
public interface IBaseCompanyAccountRealService
{
    /**
     * 查询公司招聘人员
     *
     * @param id 公司招聘人员主键
     * @return 公司招聘人员
     */
    public BaseCompanyAccountReal selectBaseCompanyAccountRealById(Long id);

    /**
     * 查询公司招聘人员列表
     *
     * @param baseCompanyAccountReal 公司招聘人员
     * @return 公司招聘人员集合
     */
    public List<BaseCompanyAccountReal> selectBaseCompanyAccountRealList(BaseCompanyAccountReal baseCompanyAccountReal);

    /**
     * 新增公司招聘人员
     *
     * @param baseCompanyAccountReal 公司招聘人员
     * @return 结果
     */
    public int insertBaseCompanyAccountReal(BaseCompanyAccountReal baseCompanyAccountReal);

    /**
     * 修改公司招聘人员
     *
     * @param baseCompanyAccountReal 公司招聘人员
     * @return 结果
     */
    public int updateBaseCompanyAccountReal(BaseCompanyAccountReal baseCompanyAccountReal);

    /**
     * 批量删除公司招聘人员
     *
     * @param ids 需要删除的公司招聘人员主键集合
     * @return 结果
     */
    public int deleteBaseCompanyAccountRealByIds(Long[] ids);

    /**
     * 删除公司招聘人员信息
     *
     * @param id 公司招聘人员主键
     * @return 结果
     */
    public int deleteBaseCompanyAccountRealById(Long id);

    BaseCompanyAccountReal selectBaseCompanyAccountRealByUserId(Long userId);
}
