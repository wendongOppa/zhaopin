package com.medicinezp.baseconfig.mapper;

import java.util.List;
import com.medicinezp.baseconfig.domain.BaseSearchHistory;

/**
 * 搜索记录Mapper接口
 *
 * @author medicinezp
 * @date 2022-10-24
 */
public interface BaseSearchHistoryMapper
{
    /**
     * 查询搜索记录
     *
     * @param id 搜索记录主键
     * @return 搜索记录
     */
    public BaseSearchHistory selectBaseSearchHistoryById(Long id);

    /**
     * 查询搜索记录列表
     *
     * @param baseSearchHistory 搜索记录
     * @return 搜索记录集合
     */
    public List<BaseSearchHistory> selectBaseSearchHistoryList(BaseSearchHistory baseSearchHistory);

    /**
     * 新增搜索记录
     *
     * @param baseSearchHistory 搜索记录
     * @return 结果
     */
    public int insertBaseSearchHistory(BaseSearchHistory baseSearchHistory);

    /**
     * 修改搜索记录
     *
     * @param baseSearchHistory 搜索记录
     * @return 结果
     */
    public int updateBaseSearchHistory(BaseSearchHistory baseSearchHistory);

    /**
     * 删除搜索记录
     *
     * @param id 搜索记录主键
     * @return 结果
     */
    public int deleteBaseSearchHistoryById(Long id);

    /**
     * 批量删除搜索记录
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseSearchHistoryByIds(Long[] ids);

    List<BaseSearchHistory> selectHotSearchHistoryData();

    List<BaseSearchHistory> selectUserSearchHistoryList(BaseSearchHistory history);

    List<BaseSearchHistory> selectSearchHelpList(BaseSearchHistory history);

    int deleteBaseSearchHistoryByUserId(Long id);
}
