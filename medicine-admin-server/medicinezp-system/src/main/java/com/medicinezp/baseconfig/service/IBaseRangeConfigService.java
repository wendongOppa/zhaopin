package com.medicinezp.baseconfig.service;

import java.util.List;
import com.medicinezp.baseconfig.domain.BaseRangeConfig;

/**
 * 区间设置Service接口
 * 
 * @author medicinezp
 * @date 2022-09-22
 */
public interface IBaseRangeConfigService 
{
    /**
     * 查询区间设置
     * 
     * @param id 区间设置主键
     * @return 区间设置
     */
    public BaseRangeConfig selectBaseRangeConfigById(Long id);

    /**
     * 查询区间设置列表
     * 
     * @param baseRangeConfig 区间设置
     * @return 区间设置集合
     */
    public List<BaseRangeConfig> selectBaseRangeConfigList(BaseRangeConfig baseRangeConfig);

    /**
     * 新增区间设置
     * 
     * @param baseRangeConfig 区间设置
     * @return 结果
     */
    public int insertBaseRangeConfig(BaseRangeConfig baseRangeConfig);

    /**
     * 修改区间设置
     * 
     * @param baseRangeConfig 区间设置
     * @return 结果
     */
    public int updateBaseRangeConfig(BaseRangeConfig baseRangeConfig);

    /**
     * 批量删除区间设置
     * 
     * @param ids 需要删除的区间设置主键集合
     * @return 结果
     */
    public int deleteBaseRangeConfigByIds(Long[] ids);

    /**
     * 删除区间设置信息
     * 
     * @param id 区间设置主键
     * @return 结果
     */
    public int deleteBaseRangeConfigById(Long id);
}
