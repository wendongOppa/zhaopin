package com.medicinezp.baseconfig.service.impl;

import java.util.List;
import com.medicinezp.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.medicinezp.baseconfig.mapper.BaseJobTypeMapper;
import com.medicinezp.baseconfig.domain.BaseJobType;
import com.medicinezp.baseconfig.service.IBaseJobTypeService;

/**
 * 职位分类Service业务层处理
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@Service
public class BaseJobTypeServiceImpl implements IBaseJobTypeService
{
    @Autowired
    private BaseJobTypeMapper baseJobTypeMapper;

    /**
     * 查询职位分类
     *
     * @param id 职位分类主键
     * @return 职位分类
     */
    @Override
    public BaseJobType selectBaseJobTypeById(Long id)
    {
        return baseJobTypeMapper.selectBaseJobTypeById(id);
    }

    /**
     * 查询职位分类列表
     *
     * @param baseJobType 职位分类
     * @return 职位分类
     */
    @Override
    public List<BaseJobType> selectBaseJobTypeList(BaseJobType baseJobType)
    {
        return baseJobTypeMapper.selectBaseJobTypeList(baseJobType);
    }

    /**
     * 新增职位分类
     *
     * @param baseJobType 职位分类
     * @return 结果
     */
    @Override
    public int insertBaseJobType(BaseJobType baseJobType)
    {
        baseJobType.setCreateTime(DateUtils.getNowDate());
        return baseJobTypeMapper.insertBaseJobType(baseJobType);
    }

    /**
     * 修改职位分类
     *
     * @param baseJobType 职位分类
     * @return 结果
     */
    @Override
    public int updateBaseJobType(BaseJobType baseJobType)
    {
        return baseJobTypeMapper.updateBaseJobType(baseJobType);
    }

    /**
     * 批量删除职位分类
     *
     * @param ids 需要删除的职位分类主键
     * @return 结果
     */
    @Override
    public int deleteBaseJobTypeByIds(Long[] ids)
    {
        return baseJobTypeMapper.deleteBaseJobTypeByIds(ids);
    }

    /**
     * 删除职位分类信息
     *
     * @param id 职位分类主键
     * @return 结果
     */
    @Override
    public int deleteBaseJobTypeById(Long id)
    {
        return baseJobTypeMapper.deleteBaseJobTypeById(id);
    }

    @Override
    public List<BaseJobType> selectJobTypeListByCategroy(BaseJobType baseJobType) {
        return baseJobTypeMapper.selectJobTypeListByCategroy(baseJobType);
    }

    @Override
    public List<BaseJobType> selectCollectJobTypeList(BaseJobType jobType) {
        return baseJobTypeMapper.selectCollectJobTypeList(jobType);
    }
}
