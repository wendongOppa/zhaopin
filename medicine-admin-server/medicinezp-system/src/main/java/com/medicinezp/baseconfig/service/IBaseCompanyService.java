package com.medicinezp.baseconfig.service;

import java.util.List;
import com.medicinezp.baseconfig.domain.BaseCompany;

/**
 * 待审核公司Service接口
 *
 * @author medicinezp
 * @date 2022-09-22
 */
public interface IBaseCompanyService
{
    /**
     * 查询待审核公司
     *
     * @param id 待审核公司主键
     * @return 待审核公司
     */
    public BaseCompany selectBaseCompanyById(Long id);

    /**
     * 查询待审核公司列表
     *
     * @param baseCompany 待审核公司
     * @return 待审核公司集合
     */
    public List<BaseCompany> selectBaseCompanyList(BaseCompany baseCompany);

    /**
     * 新增待审核公司
     *
     * @param baseCompany 待审核公司
     * @return 结果
     */
    public int insertBaseCompany(BaseCompany baseCompany);

    /**
     * 修改待审核公司
     *
     * @param baseCompany 待审核公司
     * @return 结果
     */
    public int updateBaseCompany(BaseCompany baseCompany);

    /**
     * 批量删除待审核公司
     *
     * @param ids 需要删除的待审核公司主键集合
     * @return 结果
     */
    public int deleteBaseCompanyByIds(Long[] ids);

    /**
     * 删除待审核公司信息
     *
     * @param id 待审核公司主键
     * @return 结果
     */
    public int deleteBaseCompanyById(Long id);

    List<BaseCompany> selectBrenchCompanyList(Long id);

    List<BaseCompany> selectCompanyDataList(BaseCompany company);

    List<BaseCompany> selectCollectCompanyList(BaseCompany company);

    List<BaseCompany> selectBrenchmarkCompanyList(BaseCompany company);

    List<BaseCompany> selectAvailableCompanyList(String name);

    int approveBaseCompany(BaseCompany baseCompany);

    List<BaseCompany> selectBaseCompanyExtistsList(BaseCompany param);
}
