package com.medicinezp.baseconfig.domain;

import com.medicinezp.common.annotation.Excel;
import com.medicinezp.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.List;

/**
 * 岗位对象 base_jobs
 *
 * @author medicinezp
 * @date 2022-09-22
 */
public class BaseJobs extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 岗位名称 */
    @Excel(name = "岗位名称")
    private String jobName;

    /** 职位类型 */

    private Long typeId;
    @Excel(name = "职位类型")
    private String typeName;



    /** 职位标签 */
    @Excel(name = "职位标签")
    private String jobLabels;

    /** 经验 */
    @Excel(name = "经验")
    private String workExperience;

    /** 学历 */
    @Excel(name = "学历")
    private String education;

    /** 最低薪资 */
    @Excel(name = "最低薪资")
    private Long minSalary;

    /** 最高薪资 */
    @Excel(name = "最高薪资")
    private Long maxSalary;

    /** 地址ID */
    @Excel(name = "地址ID")
    private Long addrId;

    /** 公司ID */
    @Excel(name = "公司ID")
    private Long companyId;

    /** 公司名称 */
    @Excel(name = "公司名称")
    private String companyName;

    /** 职位描述 */
    @Excel(name = "职位描述")
    private String intro;

    /** 发布人id */
    @Excel(name = "发布人id")
    private Long createUserId;

    /** 状态01正常02关闭 */
    @Excel(name = "状态01正常02关闭")
    private String status;

    private String cityName;

    private List<String> jobTypeLabels;
    private List<String> educationLabels;
    private List<String> experienceLabels;
    private Long intentionId;
    private String province;
    private String city;
    private String area;
    private String size;
    private String address;
    private BaseCompanyAddress companyAddress;

    private int type;

    private int newNum;
    private int contactNum;
    private int interviewNum;
    private int jobCount;

    private String referrerFlag;
    private int applyNum;
    private String categoryName;
    private String backReason;
    private String logo;

    private boolean collectFlag;
    private boolean applyFlag;

    public boolean isApplyFlag() {
        return applyFlag;
    }

    public void setApplyFlag(boolean applyFlag) {
        this.applyFlag = applyFlag;
    }

    public boolean isCollectFlag() {
        return collectFlag;
    }

    public void setCollectFlag(boolean collectFlag) {
        this.collectFlag = collectFlag;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getBackReason() {
        return backReason;
    }

    public void setBackReason(String backReason) {
        this.backReason = backReason;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getReferrerFlag() {
        return referrerFlag;
    }

    public void setReferrerFlag(String referrerFlag) {
        this.referrerFlag = referrerFlag;
    }

    public int getApplyNum() {
        return applyNum;
    }

    public void setApplyNum(int applyNum) {
        this.applyNum = applyNum;
    }

    public BaseCompanyAddress getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(BaseCompanyAddress companyAddress) {
        this.companyAddress = companyAddress;
    }

    public int getJobCount() {
        return jobCount;
    }

    public void setJobCount(int jobCount) {
        this.jobCount = jobCount;
    }

    public int getNewNum() {
        return newNum;
    }

    public void setNewNum(int newNum) {
        this.newNum = newNum;
    }

    public int getContactNum() {
        return contactNum;
    }

    public void setContactNum(int contactNum) {
        this.contactNum = contactNum;
    }

    public int getInterviewNum() {
        return interviewNum;
    }

    public void setInterviewNum(int interviewNum) {
        this.interviewNum = interviewNum;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getIntentionId() {
        return intentionId;
    }

    public void setIntentionId(Long intentionId) {
        this.intentionId = intentionId;
    }


    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public List<String> getJobTypeLabels() {
        return jobTypeLabels;
    }

    public void setJobTypeLabels(List<String> jobTypeLabels) {
        this.jobTypeLabels = jobTypeLabels;
    }

    public List<String> getEducationLabels() {
        return educationLabels;
    }

    public void setEducationLabels(List<String> educationLabels) {
        this.educationLabels = educationLabels;
    }

    public List<String> getExperienceLabels() {
        return experienceLabels;
    }

    public void setExperienceLabels(List<String> experienceLabels) {
        this.experienceLabels = experienceLabels;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setJobName(String jobName)
    {
        this.jobName = jobName;
    }

    public String getJobName()
    {
        return jobName;
    }
    public void setTypeId(Long typeId)
    {
        this.typeId = typeId;
    }

    public Long getTypeId()
    {
        return typeId;
    }
    public void setJobLabels(String jobLabels)
    {
        this.jobLabels = jobLabels;
    }

    public String getJobLabels()
    {
        return jobLabels;
    }
    public void setWorkExperience(String workExperience)
    {
        this.workExperience = workExperience;
    }

    public String getWorkExperience()
    {
        return workExperience;
    }
    public void setEducation(String education)
    {
        this.education = education;
    }

    public String getEducation()
    {
        return education;
    }
    public void setMinSalary(Long minSalary)
    {
        this.minSalary = minSalary;
    }

    public Long getMinSalary()
    {
        return minSalary;
    }
    public void setMaxSalary(Long maxSalary)
    {
        this.maxSalary = maxSalary;
    }

    public Long getMaxSalary()
    {
        return maxSalary;
    }
    public void setAddrId(Long addrId)
    {
        this.addrId = addrId;
    }

    public Long getAddrId()
    {
        return addrId;
    }
    public void setCompanyId(Long companyId)
    {
        this.companyId = companyId;
    }

    public Long getCompanyId()
    {
        return companyId;
    }
    public void setCompanyName(String companyName)
    {
        this.companyName = companyName;
    }

    public String getCompanyName()
    {
        return companyName;
    }
    public void setIntro(String intro)
    {
        this.intro = intro;
    }

    public String getIntro()
    {
        return intro;
    }
    public void setCreateUserId(Long createUserId)
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId()
    {
        return createUserId;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("jobName", getJobName())
            .append("typeId", getTypeId())
            .append("jobLabels", getJobLabels())
            .append("workExperience", getWorkExperience())
            .append("education", getEducation())
            .append("minSalary", getMinSalary())
            .append("maxSalary", getMaxSalary())
            .append("addrId", getAddrId())
            .append("companyId", getCompanyId())
            .append("companyName", getCompanyName())
            .append("intro", getIntro())
            .append("createUserId", getCreateUserId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("status", getStatus())
            .toString();
    }
}
