package com.medicinezp.baseconfig.service.impl;

import java.util.List;
import com.medicinezp.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.medicinezp.baseconfig.mapper.BaseUserJobApplyLogsMapper;
import com.medicinezp.baseconfig.domain.BaseUserJobApplyLogs;
import com.medicinezp.baseconfig.service.IBaseUserJobApplyLogsService;

/**
 * 用户投递简历日志Service业务层处理
 *
 * @author medicinezp
 * @date 2022-09-30
 */
@Service
public class BaseUserJobApplyLogsServiceImpl implements IBaseUserJobApplyLogsService
{
    @Autowired
    private BaseUserJobApplyLogsMapper baseUserJobApplyLogsMapper;

    /**
     * 查询用户投递简历日志
     *
     * @param id 用户投递简历日志主键
     * @return 用户投递简历日志
     */
    @Override
    public BaseUserJobApplyLogs selectBaseUserJobApplyLogsById(Long id)
    {
        return baseUserJobApplyLogsMapper.selectBaseUserJobApplyLogsById(id);
    }

    /**
     * 查询用户投递简历日志列表
     *
     * @param baseUserJobApplyLogs 用户投递简历日志
     * @return 用户投递简历日志
     */
    @Override
    public List<BaseUserJobApplyLogs> selectBaseUserJobApplyLogsList(BaseUserJobApplyLogs baseUserJobApplyLogs)
    {
        return baseUserJobApplyLogsMapper.selectBaseUserJobApplyLogsList(baseUserJobApplyLogs);
    }

    /**
     * 新增用户投递简历日志
     *
     * @param baseUserJobApplyLogs 用户投递简历日志
     * @return 结果
     */
    @Override
    public int insertBaseUserJobApplyLogs(BaseUserJobApplyLogs baseUserJobApplyLogs)
    {
        baseUserJobApplyLogs.setCreateTime(DateUtils.getNowDate());
        return baseUserJobApplyLogsMapper.insertBaseUserJobApplyLogs(baseUserJobApplyLogs);
    }

    /**
     * 修改用户投递简历日志
     *
     * @param baseUserJobApplyLogs 用户投递简历日志
     * @return 结果
     */
    @Override
    public int updateBaseUserJobApplyLogs(BaseUserJobApplyLogs baseUserJobApplyLogs)
    {
        return baseUserJobApplyLogsMapper.updateBaseUserJobApplyLogs(baseUserJobApplyLogs);
    }

    /**
     * 批量删除用户投递简历日志
     *
     * @param ids 需要删除的用户投递简历日志主键
     * @return 结果
     */
    @Override
    public int deleteBaseUserJobApplyLogsByIds(Long[] ids)
    {
        return baseUserJobApplyLogsMapper.deleteBaseUserJobApplyLogsByIds(ids);
    }

    /**
     * 删除用户投递简历日志信息
     *
     * @param id 用户投递简历日志主键
     * @return 结果
     */
    @Override
    public int deleteBaseUserJobApplyLogsById(Long id)
    {
        return baseUserJobApplyLogsMapper.deleteBaseUserJobApplyLogsById(id);
    }
}
