package com.medicinezp.baseconfig.service.impl;

import java.util.List;
import com.medicinezp.common.utils.DateUtils;
import com.medicinezp.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.medicinezp.baseconfig.mapper.BaseFeedbackMapper;
import com.medicinezp.baseconfig.domain.BaseFeedback;
import com.medicinezp.baseconfig.service.IBaseFeedbackService;

/**
 * 意见反馈Service业务层处理
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@Service
public class BaseFeedbackServiceImpl implements IBaseFeedbackService
{
    @Autowired
    private BaseFeedbackMapper baseFeedbackMapper;

    /**
     * 查询意见反馈
     *
     * @param id 意见反馈主键
     * @return 意见反馈
     */
    @Override
    public BaseFeedback selectBaseFeedbackById(Long id)
    {
        return baseFeedbackMapper.selectBaseFeedbackById(id);
    }

    /**
     * 查询意见反馈列表
     *
     * @param baseFeedback 意见反馈
     * @return 意见反馈
     */
    @Override
    public List<BaseFeedback> selectBaseFeedbackList(BaseFeedback baseFeedback)
    {
        return baseFeedbackMapper.selectBaseFeedbackList(baseFeedback);
    }

    /**
     * 新增意见反馈
     *
     * @param baseFeedback 意见反馈
     * @return 结果
     */
    @Override
    public int insertBaseFeedback(BaseFeedback baseFeedback)
    {
        baseFeedback.setCreateTime(DateUtils.getNowDate());
        return baseFeedbackMapper.insertBaseFeedback(baseFeedback);
    }

    /**
     * 修改意见反馈
     *
     * @param baseFeedback 意见反馈
     * @return 结果
     */
    @Override
    public int updateBaseFeedback(BaseFeedback baseFeedback)
    {
        baseFeedback.setUpdateBy(SecurityUtils.getUsername());
        baseFeedback.setUpdateTime(DateUtils.getNowDate());
        return baseFeedbackMapper.updateBaseFeedback(baseFeedback);
    }

    /**
     * 批量删除意见反馈
     *
     * @param ids 需要删除的意见反馈主键
     * @return 结果
     */
    @Override
    public int deleteBaseFeedbackByIds(Long[] ids)
    {
        return baseFeedbackMapper.deleteBaseFeedbackByIds(ids);
    }

    /**
     * 删除意见反馈信息
     *
     * @param id 意见反馈主键
     * @return 结果
     */
    @Override
    public int deleteBaseFeedbackById(Long id)
    {
        return baseFeedbackMapper.deleteBaseFeedbackById(id);
    }
}
