package com.medicinezp.baseconfig.domain;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.medicinezp.common.annotation.Excel;
import com.medicinezp.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 待审核公司对象 base_company
 *
 * @author medicinezp
 * @date 2022-09-22
 */
public class BaseCompany extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 公司ID */
    private Long id;
    private Long benchmarkId;

    /** 公司简称 */
    @Excel(name = "公司简称")
    private String shortName;

    /** 公司全称 */
    @Excel(name = "公司全称")
    private String companyName;

    /** 行业 */
    @Excel(name = "行业")
    private String industry;

    /** 统一社会信用代码 */
    private String uniformSocialCreditCode;

    /** 营业执照 */
    private String businessLicense;

    /** 企业LOGO */
    private String logo;



    /** 企业性质(/01国企/02民营)   */
    private String companyType;

    /** 规模 */
    @Excel(name = "规模")
    private String size;

    /** 信誉 */
    private Long credit;

    /** 公司简介 */
    private String intro;

    @Excel(name = "在招职位")
    private int jobNum;

    @Excel(name = "HR账号")
    private int HRNum;



    /** 联系电话 */
    @Excel(name = "联系电话")
    private String contactPhone;

    /** 审核时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "注册时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date auditTime;


    /** 状态 Y正常N禁用 */
    @Excel(name = "状态",readConverterExp = "Y=正常,N=禁用")
    private String availableFlag;

    /** 审核状态01 待审核  02 审核通过 03 审核不通过 */
    private String auditStatus;


    /** 驳回原因 */
    private String auditRemark;

    /** 联系人 */
    private String contactName;



    /** 证明材料 */
    private String evidentiaryFile;

    private String jobNames;


    private List<BaseCompanyAddress> addressList;
    private List<BaseCompanyPhoto> photoList;

    private String cityName;
    private String companyPhotos;

    //公司规模
    private List<String> scaleLabels;

    private int sortNo;

    private boolean collectFlag;

    private String province;
    private String city;
    private String area;

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public Long getBenchmarkId() {
        return benchmarkId;
    }

    public void setBenchmarkId(Long benchmarkId) {
        this.benchmarkId = benchmarkId;
    }

    public String getCompanyPhotos() {
        return companyPhotos;
    }

    public void setCompanyPhotos(String companyPhotos) {
        this.companyPhotos = companyPhotos;
    }

    public boolean isCollectFlag() {
        return collectFlag;
    }

    public void setCollectFlag(boolean collectFlag) {
        this.collectFlag = collectFlag;
    }

    public int getHRNum() {
        return HRNum;
    }

    public void setHRNum(int HRNum) {
        this.HRNum = HRNum;
    }

    public int getSortNo() {
        return sortNo;
    }

    public void setSortNo(int sortNo) {
        this.sortNo = sortNo;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public List<String> getScaleLabels() {
        return scaleLabels;
    }

    public void setScaleLabels(List<String> scaleLabels) {
        this.scaleLabels = scaleLabels;
    }

    public List<BaseCompanyAddress> getAddressList() {
        return addressList;
    }

    public void setAddressList(List<BaseCompanyAddress> addressList) {
        this.addressList = addressList;
    }

    public List<BaseCompanyPhoto> getPhotoList() {
        return photoList;
    }

    public void setPhotoList(List<BaseCompanyPhoto> photoList) {
        this.photoList = photoList;
    }

    public String getJobNames() {
        return jobNames;
    }

    public void setJobNames(String jobNames) {
        this.jobNames = jobNames;
    }

    public int getJobNum() {
        return jobNum;
    }

    public void setJobNum(int jobNum) {
        this.jobNum = jobNum;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setShortName(String shortName)
    {
        this.shortName = shortName;
    }

    public String getShortName()
    {
        return shortName;
    }
    public void setCompanyName(String companyName)
    {
        this.companyName = companyName;
    }

    public String getCompanyName()
    {
        return companyName;
    }
    public void setUniformSocialCreditCode(String uniformSocialCreditCode)
    {
        this.uniformSocialCreditCode = uniformSocialCreditCode;
    }

    public String getUniformSocialCreditCode()
    {
        return uniformSocialCreditCode;
    }
    public void setBusinessLicense(String businessLicense)
    {
        this.businessLicense = businessLicense;
    }

    public String getBusinessLicense()
    {
        return businessLicense;
    }
    public void setLogo(String logo)
    {
        this.logo = logo;
    }

    public String getLogo()
    {
        return logo;
    }
    public void setIndustry(String industry)
    {
        this.industry = industry;
    }

    public String getIndustry()
    {
        return industry;
    }
    public void setCompanyType(String companyType)
    {
        this.companyType = companyType;
    }

    public String getCompanyType()
    {
        return companyType;
    }
    public void setSize(String size)
    {
        this.size = size;
    }

    public String getSize()
    {
        return size;
    }
    public void setCredit(Long credit)
    {
        this.credit = credit;
    }

    public Long getCredit()
    {
        return credit;
    }
    public void setIntro(String intro)
    {
        this.intro = intro;
    }

    public String getIntro()
    {
        return intro;
    }
    public void setAvailableFlag(String availableFlag)
    {
        this.availableFlag = availableFlag;
    }

    public String getAvailableFlag()
    {
        return availableFlag;
    }
    public void setAuditStatus(String auditStatus)
    {
        this.auditStatus = auditStatus;
    }

    public String getAuditStatus()
    {
        return auditStatus;
    }
    public void setAuditTime(Date auditTime)
    {
        this.auditTime = auditTime;
    }

    public Date getAuditTime()
    {
        return auditTime;
    }
    public void setAuditRemark(String auditRemark)
    {
        this.auditRemark = auditRemark;
    }

    public String getAuditRemark()
    {
        return auditRemark;
    }
    public void setContactName(String contactName)
    {
        this.contactName = contactName;
    }

    public String getContactName()
    {
        return contactName;
    }
    public void setContactPhone(String contactPhone)
    {
        this.contactPhone = contactPhone;
    }

    public String getContactPhone()
    {
        return contactPhone;
    }
    public void setEvidentiaryFile(String evidentiaryFile)
    {
        this.evidentiaryFile = evidentiaryFile;
    }

    public String getEvidentiaryFile()
    {
        return evidentiaryFile;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("shortName", getShortName())
            .append("companyName", getCompanyName())
            .append("uniformSocialCreditCode", getUniformSocialCreditCode())
            .append("businessLicense", getBusinessLicense())
            .append("logo", getLogo())
            .append("industry", getIndustry())
            .append("companyType", getCompanyType())
            .append("size", getSize())
            .append("credit", getCredit())
            .append("intro", getIntro())
            .append("availableFlag", getAvailableFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("auditStatus", getAuditStatus())
            .append("auditTime", getAuditTime())
            .append("auditRemark", getAuditRemark())
            .append("contactName", getContactName())
            .append("contactPhone", getContactPhone())
            .append("evidentiaryFile", getEvidentiaryFile())
            .toString();
    }
}
