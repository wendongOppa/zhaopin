package com.medicinezp.baseconfig.mapper;

import java.util.List;
import com.medicinezp.baseconfig.domain.BaseArticle;

/**
 * 资讯动态Mapper接口
 * 
 * @author medicinezp
 * @date 2022-09-22
 */
public interface BaseArticleMapper 
{
    /**
     * 查询资讯动态
     * 
     * @param id 资讯动态主键
     * @return 资讯动态
     */
    public BaseArticle selectBaseArticleById(Long id);

    /**
     * 查询资讯动态列表
     * 
     * @param baseArticle 资讯动态
     * @return 资讯动态集合
     */
    public List<BaseArticle> selectBaseArticleList(BaseArticle baseArticle);

    /**
     * 新增资讯动态
     * 
     * @param baseArticle 资讯动态
     * @return 结果
     */
    public int insertBaseArticle(BaseArticle baseArticle);

    /**
     * 修改资讯动态
     * 
     * @param baseArticle 资讯动态
     * @return 结果
     */
    public int updateBaseArticle(BaseArticle baseArticle);

    /**
     * 删除资讯动态
     * 
     * @param id 资讯动态主键
     * @return 结果
     */
    public int deleteBaseArticleById(Long id);

    /**
     * 批量删除资讯动态
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseArticleByIds(Long[] ids);
}
