package com.medicinezp.baseconfig.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.medicinezp.baseconfig.domain.BaseRegionCity;
import com.medicinezp.baseconfig.domain.BaseRegionDistrict;
import com.medicinezp.baseconfig.mapper.BaseRegionDistrictMapper;
import com.medicinezp.baseconfig.service.IBaseRegionCityService;
import com.medicinezp.common.constant.CacheConstants;
import com.medicinezp.common.core.redis.RedisCache;
import com.medicinezp.common.utils.DateUtils;
import com.medicinezp.common.utils.StringUtils;
import com.medicinezp.common.utils.spring.SpringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.medicinezp.baseconfig.mapper.BaseRegionProvinceMapper;
import com.medicinezp.baseconfig.domain.BaseRegionProvince;
import com.medicinezp.baseconfig.service.IBaseRegionProvinceService;

/**
 * 省份信息Service业务层处理
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@Service
public class BaseRegionProvinceServiceImpl implements IBaseRegionProvinceService
{
    @Autowired
    private BaseRegionProvinceMapper baseRegionProvinceMapper;

    @Autowired
    private BaseRegionDistrictMapper baseRegionDistrictMapper;

    @Autowired
    private IBaseRegionCityService baseRegionCityService;



    /**
     * 查询省份信息
     *
     * @param provinceId 省份信息主键
     * @return 省份信息
     */
    @Override
    public BaseRegionProvince selectBaseRegionProvinceByProvinceId(Long provinceId)
    {
        return baseRegionProvinceMapper.selectBaseRegionProvinceByProvinceId(provinceId);
    }

    /**
     * 查询省份信息列表
     *
     * @param baseRegionProvince 省份信息
     * @return 省份信息
     */
    @Override
    public List<BaseRegionProvince> selectBaseRegionProvinceList(BaseRegionProvince baseRegionProvince)
    {
        return baseRegionProvinceMapper.selectBaseRegionProvinceList(baseRegionProvince);
    }

    /**
     * 新增省份信息
     *
     * @param baseRegionProvince 省份信息
     * @return 结果
     */
    @Override
    public int insertBaseRegionProvince(BaseRegionProvince baseRegionProvince)
    {
        baseRegionProvince.setCreateTime(DateUtils.getNowDate());
        return baseRegionProvinceMapper.insertBaseRegionProvince(baseRegionProvince);
    }

    /**
     * 修改省份信息
     *
     * @param baseRegionProvince 省份信息
     * @return 结果
     */
    @Override
    public int updateBaseRegionProvince(BaseRegionProvince baseRegionProvince)
    {
        baseRegionProvince.setUpdateTime(DateUtils.getNowDate());
        return baseRegionProvinceMapper.updateBaseRegionProvince(baseRegionProvince);
    }

    /**
     * 批量删除省份信息
     *
     * @param provinceIds 需要删除的省份信息主键
     * @return 结果
     */
    @Override
    public int deleteBaseRegionProvinceByProvinceIds(Long[] provinceIds)
    {
        return baseRegionProvinceMapper.deleteBaseRegionProvinceByProvinceIds(provinceIds);
    }

    /**
     * 删除省份信息信息
     *
     * @param provinceId 省份信息主键
     * @return 结果
     */
    @Override
    public int deleteBaseRegionProvinceByProvinceId(Long provinceId)
    {
        return baseRegionProvinceMapper.deleteBaseRegionProvinceByProvinceId(provinceId);
    }

    @Override
    public List<BaseRegionProvince> selectBaseRegionProvinceData(BaseRegionProvince baseRegionProvince) {
        RedisCache cache= SpringUtils.getBean(RedisCache.class);
        List<BaseRegionProvince> data =cache.getCacheList(CacheConstants.PROVINCE_DATA_KEY);
        if (StringUtils.isNotEmpty(data))
        {
            return data;
        }
        List<BaseRegionProvince> provinceList= baseRegionProvinceMapper.selectBaseRegionProvinceList(new BaseRegionProvince());
        List<BaseRegionCity> cityList=baseRegionCityService.selectBaseRegionCityList(new BaseRegionCity());
        List<BaseRegionDistrict> districtList= baseRegionDistrictMapper.selectBaseRegionDistrictList(new BaseRegionDistrict());
        provinceList.stream().forEach(province->{
            List<BaseRegionCity> childList=cityList.stream().filter(city->city.getProvinceId().intValue()==province.getProvinceId().intValue()).collect(Collectors.toList());
            childList.forEach(city->{
                List<BaseRegionDistrict> districtChildList=districtList.stream().filter(dist->dist.getCityId().intValue()==city.getCityId().intValue()).collect(Collectors.toList());
                city.setChildren(districtChildList);
            });
            province.setChildren(childList);
        });
        if (provinceList!=null&& provinceList.size()>0)
        {
            cache.setCacheList(CacheConstants.PROVINCE_DATA_KEY, provinceList);
        }
        return provinceList;
    }
}
