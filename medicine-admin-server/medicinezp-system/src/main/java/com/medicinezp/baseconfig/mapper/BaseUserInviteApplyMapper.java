package com.medicinezp.baseconfig.mapper;

import java.util.List;
import com.medicinezp.baseconfig.domain.BaseUserInviteApply;

/**
 * 邀请投递Mapper接口
 * 
 * @author medicinezp
 * @date 2022-09-22
 */
public interface BaseUserInviteApplyMapper 
{
    /**
     * 查询邀请投递
     * 
     * @param id 邀请投递主键
     * @return 邀请投递
     */
    public BaseUserInviteApply selectBaseUserInviteApplyById(Long id);

    /**
     * 查询邀请投递列表
     * 
     * @param baseUserInviteApply 邀请投递
     * @return 邀请投递集合
     */
    public List<BaseUserInviteApply> selectBaseUserInviteApplyList(BaseUserInviteApply baseUserInviteApply);

    /**
     * 新增邀请投递
     * 
     * @param baseUserInviteApply 邀请投递
     * @return 结果
     */
    public int insertBaseUserInviteApply(BaseUserInviteApply baseUserInviteApply);

    /**
     * 修改邀请投递
     * 
     * @param baseUserInviteApply 邀请投递
     * @return 结果
     */
    public int updateBaseUserInviteApply(BaseUserInviteApply baseUserInviteApply);

    /**
     * 删除邀请投递
     * 
     * @param id 邀请投递主键
     * @return 结果
     */
    public int deleteBaseUserInviteApplyById(Long id);

    /**
     * 批量删除邀请投递
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseUserInviteApplyByIds(Long[] ids);
}
