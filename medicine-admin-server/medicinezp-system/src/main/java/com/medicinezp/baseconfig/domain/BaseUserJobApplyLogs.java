package com.medicinezp.baseconfig.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.medicinezp.common.annotation.Excel;
import com.medicinezp.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 用户投递简历日志对象 base_user_job_apply_logs
 *
 * @author medicinezp
 * @date 2022-09-30
 */
public class BaseUserJobApplyLogs extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 用户申请岗位ID */
    @Excel(name = "用户申请岗位ID")
    private Long applyId;

    /** 内容 */
    @Excel(name = "内容")
    private String content;

    /** 处理人 */
    @Excel(name = "处理人")
    private Long dealUserid;

    /** 处理时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "处理时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dealTime;

    /** 处理人姓名 */
    @Excel(name = "处理人姓名")
    private String dealUsername;

    /** 处理人头像 */
    @Excel(name = "处理人头像")
    private String dealAvatar;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setApplyId(Long applyId)
    {
        this.applyId = applyId;
    }

    public Long getApplyId()
    {
        return applyId;
    }
    public void setContent(String content)
    {
        this.content = content;
    }

    public String getContent()
    {
        return content;
    }
    public void setDealUserid(Long dealUserid)
    {
        this.dealUserid = dealUserid;
    }

    public Long getDealUserid()
    {
        return dealUserid;
    }
    public void setDealTime(Date dealTime)
    {
        this.dealTime = dealTime;
    }

    public Date getDealTime()
    {
        return dealTime;
    }
    public void setDealUsername(String dealUsername)
    {
        this.dealUsername = dealUsername;
    }

    public String getDealUsername()
    {
        return dealUsername;
    }
    public void setDealAvatar(String dealAvatar)
    {
        this.dealAvatar = dealAvatar;
    }

    public String getDealAvatar()
    {
        return dealAvatar;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("applyId", getApplyId())
            .append("content", getContent())
            .append("remark", getRemark())
            .append("dealUserid", getDealUserid())
            .append("dealTime", getDealTime())
            .append("createTime", getCreateTime())
            .append("createBy", getCreateBy())
            .append("dealUsername", getDealUsername())
            .append("dealAvatar", getDealAvatar())
            .toString();
    }
}
