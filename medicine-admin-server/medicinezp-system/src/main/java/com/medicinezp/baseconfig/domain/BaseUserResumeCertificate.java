package com.medicinezp.baseconfig.domain;

import com.medicinezp.common.annotation.Excel;
import com.medicinezp.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 职业证书对象 base_user_resume_certificate
 *
 * @author medicinezp
 * @date 2022-09-22
 */
public class BaseUserResumeCertificate extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 简历ID */
    @Excel(name = "简历ID")
    private Long resumeId;

    /** 证书名称 */
    @Excel(name = "证书名称")
    private String certificateName;

    /** 获得时间 */
    @Excel(name = "获得时间")
    private String inceptDate;

    /** 证件照片 */
    @Excel(name = "证件照片")
    private String certificateImage;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setResumeId(Long resumeId)
    {
        this.resumeId = resumeId;
    }

    public Long getResumeId()
    {
        return resumeId;
    }
    public void setCertificateName(String certificateName)
    {
        this.certificateName = certificateName;
    }

    public String getCertificateName()
    {
        return certificateName;
    }
    public void setInceptDate(String inceptDate)
    {
        this.inceptDate = inceptDate;
    }

    public String getInceptDate()
    {
        return inceptDate;
    }
    public void setCertificateImage(String certificateImage)
    {
        this.certificateImage = certificateImage;
    }

    public String getCertificateImage()
    {
        return certificateImage;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("resumeId", getResumeId())
            .append("certificateName", getCertificateName())
            .append("inceptDate", getInceptDate())
            .append("certificateImage", getCertificateImage())
            .toString();
    }
}
