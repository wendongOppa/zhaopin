package com.medicinezp.baseconfig.domain;

import com.medicinezp.common.annotation.Excel;
import com.medicinezp.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.List;

/**
 * 岗位类型对象 base_job_category
 *
 * @author medicinezp
 * @date 2022-09-22
 */
public class BaseJobCategory extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long categoryId;

    /** 类型名称 */
    @Excel(name = "类型名称")
    private String categoryName;

    private List<BaseJobType> childList;

    private int sortNo;

    public int getSortNo() {
        return sortNo;
    }

    public void setSortNo(int sortNo) {
        this.sortNo = sortNo;
    }

    public List<BaseJobType> getChildList() {
        return childList;
    }

    public void setChildList(List<BaseJobType> childList) {
        this.childList = childList;
    }

    public void setCategoryId(Long categoryId)
    {
        this.categoryId = categoryId;
    }

    public Long getCategoryId()
    {
        return categoryId;
    }
    public void setCategoryName(String categoryName)
    {
        this.categoryName = categoryName;
    }

    public String getCategoryName()
    {
        return categoryName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("categoryId", getCategoryId())
            .append("categoryName", getCategoryName())
            .append("createTime", getCreateTime())
            .append("createBy", getCreateBy())
            .toString();
    }
}
