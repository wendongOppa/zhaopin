package com.medicinezp.baseconfig.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.medicinezp.common.annotation.Excel;
import com.medicinezp.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 意见反馈对象 base_feedback
 *
 * @author medicinezp
 * @date 2022-09-22
 */
public class BaseFeedback extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 形象ID */
    @Excel(name = "形象ID")
    private Long userId;

    /** 手机号 */
    @Excel(name = "手机号")
    private String phone;

    /** 反馈类型 */
    @Excel(name = "反馈类型")
    private String type;

    /** 反馈内容 */
    @Excel(name = "反馈内容")
    private String content;

    /** 反馈图片 */
    @Excel(name = "反馈图片")
    private String imageUrl;

    /** 处理状态 */
    @Excel(name = "处理状态")
    private String status;

    /** 处理时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "处理时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dealTime;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }
    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public String getPhone()
    {
        return phone;
    }
    public void setType(String type)
    {
        this.type = type;
    }

    public String getType()
    {
        return type;
    }
    public void setContent(String content)
    {
        this.content = content;
    }

    public String getContent()
    {
        return content;
    }
    public void setImageUrl(String imageUrl)
    {
        this.imageUrl = imageUrl;
    }

    public String getImageUrl()
    {
        return imageUrl;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }
    public void setDealTime(Date dealTime)
    {
        this.dealTime = dealTime;
    }

    public Date getDealTime()
    {
        return dealTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("phone", getPhone())
            .append("type", getType())
            .append("content", getContent())
            .append("imageUrl", getImageUrl())
            .append("status", getStatus())
            .append("createTime", getCreateTime())
            .append("dealTime", getDealTime())
            .toString();
    }
}
