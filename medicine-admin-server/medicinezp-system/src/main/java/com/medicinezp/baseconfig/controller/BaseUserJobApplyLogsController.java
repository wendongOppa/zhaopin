package com.medicinezp.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.medicinezp.common.annotation.Log;
import com.medicinezp.common.core.controller.BaseController;
import com.medicinezp.common.core.domain.AjaxResult;
import com.medicinezp.common.enums.BusinessType;
import com.medicinezp.baseconfig.domain.BaseUserJobApplyLogs;
import com.medicinezp.baseconfig.service.IBaseUserJobApplyLogsService;
import com.medicinezp.common.utils.poi.ExcelUtil;
import com.medicinezp.common.core.page.TableDataInfo;

/**
 * 用户投递简历日志Controller
 *
 * @author medicinezp
 * @date 2022-09-30
 */
@RestController
@RequestMapping("/baseconfig/applylogs")
public class BaseUserJobApplyLogsController extends BaseController
{
    @Autowired
    private IBaseUserJobApplyLogsService baseUserJobApplyLogsService;

    /**
     * 查询用户投递简历日志列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:applylogs:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseUserJobApplyLogs baseUserJobApplyLogs)
    {
        startPage();
        List<BaseUserJobApplyLogs> list = baseUserJobApplyLogsService.selectBaseUserJobApplyLogsList(baseUserJobApplyLogs);
        return getDataTable(list);
    }

    /**
     * 导出用户投递简历日志列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:applylogs:export')")
    @Log(title = "用户投递简历日志", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseUserJobApplyLogs baseUserJobApplyLogs)
    {
        List<BaseUserJobApplyLogs> list = baseUserJobApplyLogsService.selectBaseUserJobApplyLogsList(baseUserJobApplyLogs);
        ExcelUtil<BaseUserJobApplyLogs> util = new ExcelUtil<BaseUserJobApplyLogs>(BaseUserJobApplyLogs.class);
        util.exportExcel(response, list, "用户投递简历日志数据");
    }

    /**
     * 获取用户投递简历日志详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:applylogs:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(baseUserJobApplyLogsService.selectBaseUserJobApplyLogsById(id));
    }

    /**
     * 新增用户投递简历日志
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:applylogs:add')")
    @Log(title = "用户投递简历日志", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseUserJobApplyLogs baseUserJobApplyLogs)
    {
        return toAjax(baseUserJobApplyLogsService.insertBaseUserJobApplyLogs(baseUserJobApplyLogs));
    }

    /**
     * 修改用户投递简历日志
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:applylogs:edit')")
    @Log(title = "用户投递简历日志", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseUserJobApplyLogs baseUserJobApplyLogs)
    {
        return toAjax(baseUserJobApplyLogsService.updateBaseUserJobApplyLogs(baseUserJobApplyLogs));
    }

    /**
     * 删除用户投递简历日志
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:applylogs:remove')")
    @Log(title = "用户投递简历日志", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseUserJobApplyLogsService.deleteBaseUserJobApplyLogsByIds(ids));
    }
}
