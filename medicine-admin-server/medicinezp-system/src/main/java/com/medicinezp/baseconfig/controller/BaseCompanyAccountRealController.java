package com.medicinezp.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.medicinezp.common.constant.UserConstants;
import com.medicinezp.common.core.domain.entity.SysUser;
import com.medicinezp.common.utils.DateUtils;
import com.medicinezp.common.utils.SecurityUtils;
import com.medicinezp.common.utils.StringUtils;
import com.medicinezp.system.service.ISysConfigService;
import com.medicinezp.system.service.ISysUserService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.medicinezp.common.annotation.Log;
import com.medicinezp.common.core.controller.BaseController;
import com.medicinezp.common.core.domain.AjaxResult;
import com.medicinezp.common.enums.BusinessType;
import com.medicinezp.baseconfig.domain.BaseCompanyAccountReal;
import com.medicinezp.baseconfig.service.IBaseCompanyAccountRealService;
import com.medicinezp.common.utils.poi.ExcelUtil;
import com.medicinezp.common.core.page.TableDataInfo;

/**
 * 公司招聘人员Controller
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@RestController
@RequestMapping("/baseconfig/companyaccont")
public class BaseCompanyAccountRealController extends BaseController
{
    @Autowired
    private IBaseCompanyAccountRealService baseCompanyAccountRealService;

    @Autowired
    private ISysUserService userService;


    @Autowired
    private ISysConfigService configService;

    /**
     * 查询公司招聘人员列表
     */
    @GetMapping("/list")
    public TableDataInfo list(BaseCompanyAccountReal baseCompanyAccountReal)
    {
        startPage();
        List<BaseCompanyAccountReal> list = baseCompanyAccountRealService.selectBaseCompanyAccountRealList(baseCompanyAccountReal);
        return getDataTable(list);
    }

    /**
     * 导出公司招聘人员列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:companyaccont:export')")
    @Log(title = "公司招聘人员", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseCompanyAccountReal baseCompanyAccountReal)
    {
        List<BaseCompanyAccountReal> list = baseCompanyAccountRealService.selectBaseCompanyAccountRealList(baseCompanyAccountReal);
        ExcelUtil<BaseCompanyAccountReal> util = new ExcelUtil<BaseCompanyAccountReal>(BaseCompanyAccountReal.class);
        util.exportExcel(response, list, "公司招聘人员数据");
    }

    /**
     * 获取公司招聘人员详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(baseCompanyAccountRealService.selectBaseCompanyAccountRealById(id));
    }

    /**
     * 新增公司招聘人员
     */
    @Log(title = "公司招聘人员", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseCompanyAccountReal baseCompanyAccountReal)
    {
        SysUser currentUser= SecurityUtils.getLoginUser().getUser();
        if(currentUser.getCurrentCompanyid()==null){
            return AjaxResult.error("当前用户没有分配公司");
        }
        SysUser user=new SysUser();
        user.setUserName(baseCompanyAccountReal.getUserName());
        user.setNickName(baseCompanyAccountReal.getUserName());
        user.setPhonenumber(baseCompanyAccountReal.getUserPhone());
        user.setRemark(baseCompanyAccountReal.getRemark());
        user.setCreateTime(DateUtils.getNowDate());
        user.setCreateBy(currentUser.getUserName());
        if (UserConstants.NOT_UNIQUE.equals(userService.checkUserNameUnique(user)))
        {
            return AjaxResult.error("新增用户'" + user.getUserName() + "'失败，登录账号已存在");
        }
        else if (StringUtils.isNotEmpty(user.getPhonenumber())
                && UserConstants.NOT_UNIQUE.equals(userService.checkPhoneUnique(user)))
        {
            return AjaxResult.error("新增用户'" + user.getUserName() + "'失败，手机号码已存在");
        }

        user.setCreateBy(getUsername());
        String password = configService.selectConfigByKey("sys.user.initPassword");
        user.setPassword(SecurityUtils.encryptPassword(password));
        int result= userService.insertUser(user);
        if(result>0){
            baseCompanyAccountReal.setCompanyId(currentUser.getCurrentCompanyid());
            baseCompanyAccountReal.setUserId(user.getUserId());
            baseCompanyAccountReal.setIsMain("N");
            baseCompanyAccountReal.setStatus("Y");
            baseCompanyAccountReal.setCreateTime(DateUtils.getNowDate());
            baseCompanyAccountReal.setCreateBy(currentUser.getUserName());
            result+=baseCompanyAccountRealService.insertBaseCompanyAccountReal(baseCompanyAccountReal);
        }
        return toAjax(result);
    }

    /**
     * 修改公司招聘人员
     */
    @Log(title = "公司招聘人员", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseCompanyAccountReal baseCompanyAccountReal)
    {
        return toAjax(baseCompanyAccountRealService.updateBaseCompanyAccountReal(baseCompanyAccountReal));
    }

    /**
     * 删除公司招聘人员
     */
    @Log(title = "公司招聘人员", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseCompanyAccountRealService.deleteBaseCompanyAccountRealByIds(ids));
    }
}
