package com.medicinezp.baseconfig.service.impl;

import java.util.List;
import com.medicinezp.common.utils.DateUtils;
import com.medicinezp.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.medicinezp.baseconfig.mapper.BaseIndustryMapper;
import com.medicinezp.baseconfig.domain.BaseIndustry;
import com.medicinezp.baseconfig.service.IBaseIndustryService;

/**
 * 行业信息Service业务层处理
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@Service
public class BaseIndustryServiceImpl implements IBaseIndustryService
{
    @Autowired
    private BaseIndustryMapper baseIndustryMapper;

    /**
     * 查询行业信息
     *
     * @param id 行业信息主键
     * @return 行业信息
     */
    @Override
    public BaseIndustry selectBaseIndustryById(Long id)
    {
        return baseIndustryMapper.selectBaseIndustryById(id);
    }

    /**
     * 查询行业信息列表
     *
     * @param baseIndustry 行业信息
     * @return 行业信息
     */
    @Override
    public List<BaseIndustry> selectBaseIndustryList(BaseIndustry baseIndustry)
    {
        return baseIndustryMapper.selectBaseIndustryList(baseIndustry);
    }

    /**
     * 新增行业信息
     *
     * @param baseIndustry 行业信息
     * @return 结果
     */
    @Override
    public int insertBaseIndustry(BaseIndustry baseIndustry)
    {
        baseIndustry.setCreateTime(DateUtils.getNowDate());
        baseIndustry.setCreateBy(SecurityUtils.getUsername());
        return baseIndustryMapper.insertBaseIndustry(baseIndustry);
    }

    /**
     * 修改行业信息
     *
     * @param baseIndustry 行业信息
     * @return 结果
     */
    @Override
    public int updateBaseIndustry(BaseIndustry baseIndustry)
    {
        baseIndustry.setUpdateTime(DateUtils.getNowDate());
        baseIndustry.setUpdateBy(SecurityUtils.getUsername());
        return baseIndustryMapper.updateBaseIndustry(baseIndustry);
    }

    /**
     * 批量删除行业信息
     *
     * @param ids 需要删除的行业信息主键
     * @return 结果
     */
    @Override
    public int deleteBaseIndustryByIds(Long[] ids)
    {
        return baseIndustryMapper.deleteBaseIndustryByIds(ids);
    }

    /**
     * 删除行业信息信息
     *
     * @param id 行业信息主键
     * @return 结果
     */
    @Override
    public int deleteBaseIndustryById(Long id)
    {
        return baseIndustryMapper.deleteBaseIndustryById(id);
    }
}
