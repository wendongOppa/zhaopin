package com.medicinezp.baseconfig.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.medicinezp.baseconfig.domain.BaseJobLogs;
import com.medicinezp.baseconfig.mapper.BaseJobLogsMapper;
import com.medicinezp.common.utils.DateUtils;
import com.medicinezp.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.medicinezp.baseconfig.mapper.BaseJobsMapper;
import com.medicinezp.baseconfig.domain.BaseJobs;
import com.medicinezp.baseconfig.service.IBaseJobsService;

/**
 * 岗位Service业务层处理
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@Service
public class BaseJobsServiceImpl implements IBaseJobsService
{
    @Autowired
    private BaseJobsMapper baseJobsMapper;

    @Autowired
    private BaseJobLogsMapper baseJobLogsMapper;

    /**
     * 查询岗位
     *
     * @param id 岗位主键
     * @return 岗位
     */
    @Override
    public BaseJobs selectBaseJobsById(Long id)
    {
        return baseJobsMapper.selectBaseJobsById(id);
    }

    /**
     * 查询岗位列表
     *
     * @param baseJobs 岗位
     * @return 岗位
     */
    @Override
    public List<BaseJobs> selectBaseJobsList(BaseJobs baseJobs)
    {
        return baseJobsMapper.selectBaseJobsList(baseJobs);
    }

    /**
     * 新增岗位
     *
     * @param baseJobs 岗位
     * @return 结果
     */
    @Override
    public int insertBaseJobs(BaseJobs baseJobs)
    {
        baseJobs.setCreateTime(DateUtils.getNowDate());
        baseJobs.setCreateBy(SecurityUtils.getUsername());
        int result= baseJobsMapper.insertBaseJobs(baseJobs);
        if(result>0){
            BaseJobLogs log=new BaseJobLogs();
            log.setJobId(baseJobs.getId());
            log.setContent("发布职位");
            log.setCreateTime(DateUtils.getNowDate());
            log.setCreateBy(SecurityUtils.getUsername());
            log.setCreateAvatar(SecurityUtils.getLoginUser().getUser().getAvatar());
            baseJobLogsMapper.insertBaseJobLogs(log);
        }
        return result;
    }

    /**
     * 修改岗位
     *
     * @param baseJobs 岗位
     * @return 结果
     */
    @Override
    public int updateBaseJobs(BaseJobs baseJobs)
    {
        baseJobs.setUpdateBy(SecurityUtils.getUsername());
        baseJobs.setUpdateTime(DateUtils.getNowDate());
        int result= baseJobsMapper.updateBaseJobs(baseJobs);
        if(result>0){
            BaseJobLogs log=new BaseJobLogs();
            log.setJobId(baseJobs.getId());
            log.setContent("修改职位");
            log.setCreateTime(DateUtils.getNowDate());
            log.setCreateBy(SecurityUtils.getUsername());
            log.setCreateAvatar(SecurityUtils.getLoginUser().getUser().getAvatar());
            baseJobLogsMapper.insertBaseJobLogs(log);
        }
        return result;
    }

    @Override
    public int approveBaseJobs(BaseJobs baseJobs) {
        baseJobs.setUpdateBy(SecurityUtils.getUsername());
        baseJobs.setUpdateTime(DateUtils.getNowDate());
        int result= baseJobsMapper.updateBaseJobs(baseJobs);
        if(result>0){
            BaseJobLogs log=new BaseJobLogs();
            log.setJobId(baseJobs.getId());
            log.setContent("审核"+("02".equals(baseJobs.getStatus())?"通过":"不通过"));
            log.setCreateTime(DateUtils.getNowDate());
            log.setCreateBy(SecurityUtils.getUsername());
            log.setRemark(baseJobs.getBackReason());
            log.setCreateAvatar(SecurityUtils.getLoginUser().getUser().getAvatar());
            baseJobLogsMapper.insertBaseJobLogs(log);
        }
        return  result;
    }

    @Override
    public void refreshBaseJobs(Long currentCompanyid) {
        baseJobsMapper.refreshBaseJobs(currentCompanyid);
    }

    @Override
    public int updateBaseJobsInfo(BaseJobs baseJobs) {
        baseJobs.setUpdateBy(SecurityUtils.getUsername());
        baseJobs.setUpdateTime(DateUtils.getNowDate());
        int result= baseJobsMapper.updateBaseJobs(baseJobs);
        return result;
    }

    /**
     * 批量删除岗位
     *
     * @param ids 需要删除的岗位主键
     * @return 结果
     */
    @Override
    public int deleteBaseJobsByIds(Long[] ids)
    {
        return baseJobsMapper.deleteBaseJobsByIds(ids);
    }

    /**
     * 删除岗位信息
     *
     * @param id 岗位主键
     * @return 结果
     */
    @Override
    public int deleteBaseJobsById(Long id)
    {
        return baseJobsMapper.deleteBaseJobsById(id);
    }

    @Override
    public List<BaseJobs> selectJobsData(BaseJobs jobParam) {
        return baseJobsMapper.selectJobsData(jobParam);
    }

    @Override
    public List<BaseJobs> selectQueryJobsList(BaseJobs jobVo) {
        if(jobVo.getEducationLabels()!=null && jobVo.getEducationLabels().size()>0 && jobVo.getEducationLabels().contains("不限")){
            jobVo.setEducationLabels(new ArrayList<String>());
        }
        if(jobVo.getExperienceLabels()!=null && jobVo.getExperienceLabels().size()>0 && jobVo.getExperienceLabels().contains("不限")){
            jobVo.setExperienceLabels(new ArrayList<String>());
        }
        return baseJobsMapper.selectQueryJobsList(jobVo);
    }

    @Override
    public List<BaseJobs> selectUserCollectJobsList(BaseJobs param) {
        return baseJobsMapper.selectUserCollectJobsList(param);
    }

    @Override
    public List<BaseJobs> selectUserBrowserJobsList(BaseJobs param) {
        return baseJobsMapper.selectUserBrowserJobsList(param);
    }

    @Override
    public List<BaseJobs> selectMyApplyJobsList(BaseJobs param) {
        return baseJobsMapper.selectMyApplyJobsList(param);
    }

    @Override
    public List<BaseJobs> selectMyInviteJobsList(BaseJobs param) {
        return baseJobsMapper.selectMyInviteJobsList(param);
    }

    @Override
    public List<BaseJobs> selectMyAllJobsList(BaseJobs param) {
        return baseJobsMapper.selectMyAllJobsList(param);
    }

    @Override
    public List<BaseJobs> selectQueryNewJobsList(BaseJobs param) {
        return baseJobsMapper.selectQueryNewJobsList(param);
    }

    @Override
    public List<BaseJobs> selectMyCompanyJobsList(BaseJobs baseJobs) {
        return baseJobsMapper.selectMyCompanyJobsList(baseJobs);
    }

    @Override
    public List<BaseJobs> selectBaseJobsCountList(BaseJobs baseJobs) {
        return baseJobsMapper.selectBaseJobsCountList(baseJobs);
    }

    @Override
    public List<BaseJobs> selectAllJobsList(BaseJobs baseJobs) {
        return baseJobsMapper.selectAllJobsList(baseJobs);
    }



}
