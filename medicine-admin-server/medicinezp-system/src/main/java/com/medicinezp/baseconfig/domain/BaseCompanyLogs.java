package com.medicinezp.baseconfig.domain;

import com.medicinezp.common.annotation.Excel;
import com.medicinezp.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
/**
 * 公司修改日志对象 base_company_logs
 *
 * @author medicinezp
 * @date 2022-10-14
 */
public class BaseCompanyLogs extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 公司ID */
    @Excel(name = "公司ID")
    private Long companyId;

    /** 内容 */
    @Excel(name = "内容")
    private String content;

    /** 创建人id */
    @Excel(name = "创建人id")
    private Long createUserid;

    /** 创建人头像 */
    @Excel(name = "创建人头像")
    private String createAvatar;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setCompanyId(Long companyId)
    {
        this.companyId = companyId;
    }

    public Long getCompanyId()
    {
        return companyId;
    }
    public void setContent(String content)
    {
        this.content = content;
    }

    public String getContent()
    {
        return content;
    }
    public void setCreateUserid(Long createUserid)
    {
        this.createUserid = createUserid;
    }

    public Long getCreateUserid()
    {
        return createUserid;
    }
    public void setCreateAvatar(String createAvatar)
    {
        this.createAvatar = createAvatar;
    }

    public String getCreateAvatar()
    {
        return createAvatar;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("companyId", getCompanyId())
            .append("content", getContent())
            .append("remark", getRemark())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("createUserid", getCreateUserid())
            .append("createAvatar", getCreateAvatar())
            .toString();
    }
}
