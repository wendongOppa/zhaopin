package com.medicinezp.baseconfig.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.medicinezp.baseconfig.mapper.BaseUserResumeProjectMapper;
import com.medicinezp.baseconfig.domain.BaseUserResumeProject;
import com.medicinezp.baseconfig.service.IBaseUserResumeProjectService;

/**
 * 项目经验Service业务层处理
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@Service
public class BaseUserResumeProjectServiceImpl implements IBaseUserResumeProjectService
{
    @Autowired
    private BaseUserResumeProjectMapper baseUserResumeProjectMapper;

    /**
     * 查询项目经验
     *
     * @param id 项目经验主键
     * @return 项目经验
     */
    @Override
    public BaseUserResumeProject selectBaseUserResumeProjectById(Long id)
    {
        return baseUserResumeProjectMapper.selectBaseUserResumeProjectById(id);
    }

    /**
     * 查询项目经验列表
     *
     * @param baseUserResumeProject 项目经验
     * @return 项目经验
     */
    @Override
    public List<BaseUserResumeProject> selectBaseUserResumeProjectList(BaseUserResumeProject baseUserResumeProject)
    {
        return baseUserResumeProjectMapper.selectBaseUserResumeProjectList(baseUserResumeProject);
    }

    /**
     * 新增项目经验
     *
     * @param baseUserResumeProject 项目经验
     * @return 结果
     */
    @Override
    public int insertBaseUserResumeProject(BaseUserResumeProject baseUserResumeProject)
    {
        return baseUserResumeProjectMapper.insertBaseUserResumeProject(baseUserResumeProject);
    }

    /**
     * 修改项目经验
     *
     * @param baseUserResumeProject 项目经验
     * @return 结果
     */
    @Override
    public int updateBaseUserResumeProject(BaseUserResumeProject baseUserResumeProject)
    {
        return baseUserResumeProjectMapper.updateBaseUserResumeProject(baseUserResumeProject);
    }

    /**
     * 批量删除项目经验
     *
     * @param ids 需要删除的项目经验主键
     * @return 结果
     */
    @Override
    public int deleteBaseUserResumeProjectByIds(Long[] ids)
    {
        return baseUserResumeProjectMapper.deleteBaseUserResumeProjectByIds(ids);
    }

    /**
     * 删除项目经验信息
     *
     * @param id 项目经验主键
     * @return 结果
     */
    @Override
    public int deleteBaseUserResumeProjectById(Long id)
    {
        return baseUserResumeProjectMapper.deleteBaseUserResumeProjectById(id);
    }
}
