package com.medicinezp.baseconfig.mapper;

import java.util.List;
import com.medicinezp.baseconfig.domain.BaseCompany;
import org.apache.ibatis.annotations.Param;

/**
 * 待审核公司Mapper接口
 *
 * @author medicinezp
 * @date 2022-09-22
 */
public interface BaseCompanyMapper
{
    /**
     * 查询待审核公司
     *
     * @param id 待审核公司主键
     * @return 待审核公司
     */
    public BaseCompany selectBaseCompanyById(Long id);

    /**
     * 查询待审核公司列表
     *
     * @param baseCompany 待审核公司
     * @return 待审核公司集合
     */
    public List<BaseCompany> selectBaseCompanyList(BaseCompany baseCompany);

    /**
     * 新增待审核公司
     *
     * @param baseCompany 待审核公司
     * @return 结果
     */
    public int insertBaseCompany(BaseCompany baseCompany);

    /**
     * 修改待审核公司
     *
     * @param baseCompany 待审核公司
     * @return 结果
     */
    public int updateBaseCompany(BaseCompany baseCompany);

    /**
     * 删除待审核公司
     *
     * @param id 待审核公司主键
     * @return 结果
     */
    public int deleteBaseCompanyById(Long id);

    /**
     * 批量删除待审核公司
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseCompanyByIds(Long[] ids);

    List<BaseCompany> selectBrenchCompanyList(Long id);

    List<BaseCompany> selectCompanyDataList(BaseCompany company);

    List<BaseCompany> selectCollectCompanyList(BaseCompany company);

    List<BaseCompany> selectBrenchmarkCompanyList(BaseCompany company);

    List<BaseCompany> selectAvailableCompanyList(@Param("name") String name);

    List<BaseCompany> selectBaseCompanyExtistsList(BaseCompany param);
}
