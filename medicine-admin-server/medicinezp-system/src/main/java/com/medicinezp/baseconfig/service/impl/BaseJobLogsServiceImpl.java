package com.medicinezp.baseconfig.service.impl;

import java.util.List;
import com.medicinezp.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.medicinezp.baseconfig.mapper.BaseJobLogsMapper;
import com.medicinezp.baseconfig.domain.BaseJobLogs;
import com.medicinezp.baseconfig.service.IBaseJobLogsService;

/**
 * 职位修改日志Service业务层处理
 *
 * @author medicinezp
 * @date 2022-10-13
 */
@Service
public class BaseJobLogsServiceImpl implements IBaseJobLogsService
{
    @Autowired
    private BaseJobLogsMapper baseJobLogsMapper;

    /**
     * 查询职位修改日志
     *
     * @param id 职位修改日志主键
     * @return 职位修改日志
     */
    @Override
    public BaseJobLogs selectBaseJobLogsById(Long id)
    {
        return baseJobLogsMapper.selectBaseJobLogsById(id);
    }

    /**
     * 查询职位修改日志列表
     *
     * @param baseJobLogs 职位修改日志
     * @return 职位修改日志
     */
    @Override
    public List<BaseJobLogs> selectBaseJobLogsList(BaseJobLogs baseJobLogs)
    {
        return baseJobLogsMapper.selectBaseJobLogsList(baseJobLogs);
    }

    /**
     * 新增职位修改日志
     *
     * @param baseJobLogs 职位修改日志
     * @return 结果
     */
    @Override
    public int insertBaseJobLogs(BaseJobLogs baseJobLogs)
    {
        baseJobLogs.setCreateTime(DateUtils.getNowDate());
        return baseJobLogsMapper.insertBaseJobLogs(baseJobLogs);
    }

    /**
     * 修改职位修改日志
     *
     * @param baseJobLogs 职位修改日志
     * @return 结果
     */
    @Override
    public int updateBaseJobLogs(BaseJobLogs baseJobLogs)
    {
        return baseJobLogsMapper.updateBaseJobLogs(baseJobLogs);
    }

    /**
     * 批量删除职位修改日志
     *
     * @param ids 需要删除的职位修改日志主键
     * @return 结果
     */
    @Override
    public int deleteBaseJobLogsByIds(Long[] ids)
    {
        return baseJobLogsMapper.deleteBaseJobLogsByIds(ids);
    }

    /**
     * 删除职位修改日志信息
     *
     * @param id 职位修改日志主键
     * @return 结果
     */
    @Override
    public int deleteBaseJobLogsById(Long id)
    {
        return baseJobLogsMapper.deleteBaseJobLogsById(id);
    }
}
