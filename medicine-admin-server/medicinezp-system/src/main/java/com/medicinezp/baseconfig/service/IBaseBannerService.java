package com.medicinezp.baseconfig.service;

import java.util.List;
import com.medicinezp.baseconfig.domain.BaseBanner;

/**
 * 轮播图管理Service接口
 * 
 * @author medicinezp
 * @date 2022-09-22
 */
public interface IBaseBannerService 
{
    /**
     * 查询轮播图管理
     * 
     * @param id 轮播图管理主键
     * @return 轮播图管理
     */
    public BaseBanner selectBaseBannerById(Long id);

    /**
     * 查询轮播图管理列表
     * 
     * @param baseBanner 轮播图管理
     * @return 轮播图管理集合
     */
    public List<BaseBanner> selectBaseBannerList(BaseBanner baseBanner);

    /**
     * 新增轮播图管理
     * 
     * @param baseBanner 轮播图管理
     * @return 结果
     */
    public int insertBaseBanner(BaseBanner baseBanner);

    /**
     * 修改轮播图管理
     * 
     * @param baseBanner 轮播图管理
     * @return 结果
     */
    public int updateBaseBanner(BaseBanner baseBanner);

    /**
     * 批量删除轮播图管理
     * 
     * @param ids 需要删除的轮播图管理主键集合
     * @return 结果
     */
    public int deleteBaseBannerByIds(Long[] ids);

    /**
     * 删除轮播图管理信息
     * 
     * @param id 轮播图管理主键
     * @return 结果
     */
    public int deleteBaseBannerById(Long id);
}
