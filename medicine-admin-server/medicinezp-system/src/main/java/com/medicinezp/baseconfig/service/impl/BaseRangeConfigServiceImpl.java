package com.medicinezp.baseconfig.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.medicinezp.baseconfig.mapper.BaseRangeConfigMapper;
import com.medicinezp.baseconfig.domain.BaseRangeConfig;
import com.medicinezp.baseconfig.service.IBaseRangeConfigService;

/**
 * 区间设置Service业务层处理
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@Service
public class BaseRangeConfigServiceImpl implements IBaseRangeConfigService
{
    @Autowired
    private BaseRangeConfigMapper baseRangeConfigMapper;

    /**
     * 查询区间设置
     *
     * @param id 区间设置主键
     * @return 区间设置
     */
    @Override
    public BaseRangeConfig selectBaseRangeConfigById(Long id)
    {
        return baseRangeConfigMapper.selectBaseRangeConfigById(id);
    }

    /**
     * 查询区间设置列表
     *
     * @param baseRangeConfig 区间设置
     * @return 区间设置
     */
    @Override
    public List<BaseRangeConfig> selectBaseRangeConfigList(BaseRangeConfig baseRangeConfig)
    {
        return baseRangeConfigMapper.selectBaseRangeConfigList(baseRangeConfig);
    }

    /**
     * 新增区间设置
     *
     * @param baseRangeConfig 区间设置
     * @return 结果
     */
    @Override
    public int insertBaseRangeConfig(BaseRangeConfig baseRangeConfig)
    {
        return baseRangeConfigMapper.insertBaseRangeConfig(baseRangeConfig);
    }

    /**
     * 修改区间设置
     *
     * @param baseRangeConfig 区间设置
     * @return 结果
     */
    @Override
    public int updateBaseRangeConfig(BaseRangeConfig baseRangeConfig)
    {
        return baseRangeConfigMapper.updateBaseRangeConfig(baseRangeConfig);
    }

    /**
     * 批量删除区间设置
     *
     * @param ids 需要删除的区间设置主键
     * @return 结果
     */
    @Override
    public int deleteBaseRangeConfigByIds(Long[] ids)
    {
        return baseRangeConfigMapper.deleteBaseRangeConfigByIds(ids);
    }

    /**
     * 删除区间设置信息
     *
     * @param id 区间设置主键
     * @return 结果
     */
    @Override
    public int deleteBaseRangeConfigById(Long id)
    {
        return baseRangeConfigMapper.deleteBaseRangeConfigById(id);
    }
}
