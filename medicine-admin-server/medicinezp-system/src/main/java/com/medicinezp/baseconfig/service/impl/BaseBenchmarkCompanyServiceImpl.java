package com.medicinezp.baseconfig.service.impl;

import java.util.List;
import com.medicinezp.common.utils.DateUtils;
import com.medicinezp.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.medicinezp.baseconfig.mapper.BaseBenchmarkCompanyMapper;
import com.medicinezp.baseconfig.domain.BaseBenchmarkCompany;
import com.medicinezp.baseconfig.service.IBaseBenchmarkCompanyService;

/**
 * 标杆企业Service业务层处理
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@Service
public class BaseBenchmarkCompanyServiceImpl implements IBaseBenchmarkCompanyService
{
    @Autowired
    private BaseBenchmarkCompanyMapper baseBenchmarkCompanyMapper;

    /**
     * 查询标杆企业
     *
     * @param id 标杆企业主键
     * @return 标杆企业
     */
    @Override
    public BaseBenchmarkCompany selectBaseBenchmarkCompanyById(Long id)
    {
        return baseBenchmarkCompanyMapper.selectBaseBenchmarkCompanyById(id);
    }

    /**
     * 查询标杆企业列表
     *
     * @param baseBenchmarkCompany 标杆企业
     * @return 标杆企业
     */
    @Override
    public List<BaseBenchmarkCompany> selectBaseBenchmarkCompanyList(BaseBenchmarkCompany baseBenchmarkCompany)
    {
        return baseBenchmarkCompanyMapper.selectBaseBenchmarkCompanyList(baseBenchmarkCompany);
    }

    /**
     * 新增标杆企业
     *
     * @param baseBenchmarkCompany 标杆企业
     * @return 结果
     */
    @Override
    public int insertBaseBenchmarkCompany(BaseBenchmarkCompany baseBenchmarkCompany)
    {
        baseBenchmarkCompany.setCreateTime(DateUtils.getNowDate());
        baseBenchmarkCompany.setCreateBy(SecurityUtils.getUsername());
        return baseBenchmarkCompanyMapper.insertBaseBenchmarkCompany(baseBenchmarkCompany);
    }

    /**
     * 修改标杆企业
     *
     * @param baseBenchmarkCompany 标杆企业
     * @return 结果
     */
    @Override
    public int updateBaseBenchmarkCompany(BaseBenchmarkCompany baseBenchmarkCompany)
    {
        return baseBenchmarkCompanyMapper.updateBaseBenchmarkCompany(baseBenchmarkCompany);
    }

    /**
     * 批量删除标杆企业
     *
     * @param ids 需要删除的标杆企业主键
     * @return 结果
     */
    @Override
    public int deleteBaseBenchmarkCompanyByIds(Long[] ids)
    {
        return baseBenchmarkCompanyMapper.deleteBaseBenchmarkCompanyByIds(ids);
    }

    /**
     * 删除标杆企业信息
     *
     * @param id 标杆企业主键
     * @return 结果
     */
    @Override
    public int deleteBaseBenchmarkCompanyById(Long id)
    {
        return baseBenchmarkCompanyMapper.deleteBaseBenchmarkCompanyById(id);
    }
}
