package com.medicinezp.baseconfig.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.medicinezp.common.annotation.Excel;
import com.medicinezp.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;
import java.util.List;

/**
 * 求职简历对象 base_user_resume
 *
 * @author medicinezp
 * @date 2022-09-22
 */
public class BaseUserResume extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 用户ID */
    @Excel(name = "用户ID")
    private Long userId;

    /** 简历标题 */
    @Excel(name = "简历标题")
    private String title;

    /** 简历类型1在线简历2附件简历 */
    @Excel(name = "简历类型1在线简历2附件简历")
    private Integer type;

    /** 个人优势 */
    @Excel(name = "个人优势")
    private String advantage;

    /** 附件地址 */
    @Excel(name = "附件地址")
    private String resumeUrl;

    private String pdfBase64;



    /** 默认简历 */
    @Excel(name = "默认简历")
    private String defaultFlag;

    private String avatar;
    private String userName;
    private String phonenumber;

    private String sex;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birthDate;

    private int age;

    private double fileSize;
    public String getPdfBase64() {
        return pdfBase64;
    }

    public void setPdfBase64(String pdfBase64) {
        this.pdfBase64 = pdfBase64;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public double getFileSize() {
        return fileSize;
    }

    public void setFileSize(double fileSize) {
        this.fileSize = fileSize;
    }

    private List<BaseUserResumeExperience> experienceList;
    private List<BaseUserResumeProject> projectList;
    private List<BaseUserResumeCertificate> certList;
    private List<BaseUserResumeEducation> educationList;
    private List<BaseUserIntention>  intentionList;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public List<BaseUserIntention> getIntentionList() {
        return intentionList;
    }

    public void setIntentionList(List<BaseUserIntention> intentionList) {
        this.intentionList = intentionList;
    }

    public List<BaseUserResumeExperience> getExperienceList() {
        return experienceList;
    }

    public void setExperienceList(List<BaseUserResumeExperience> experienceList) {
        this.experienceList = experienceList;
    }

    public List<BaseUserResumeProject> getProjectList() {
        return projectList;
    }

    public void setProjectList(List<BaseUserResumeProject> projectList) {
        this.projectList = projectList;
    }

    public List<BaseUserResumeCertificate> getCertList() {
        return certList;
    }

    public void setCertList(List<BaseUserResumeCertificate> certList) {
        this.certList = certList;
    }

    public List<BaseUserResumeEducation> getEducationList() {
        return educationList;
    }

    public void setEducationList(List<BaseUserResumeEducation> educationList) {
        this.educationList = educationList;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getTitle()
    {
        return title;
    }
    public void setType(Integer type)
    {
        this.type = type;
    }

    public Integer getType()
    {
        return type;
    }
    public void setAdvantage(String advantage)
    {
        this.advantage = advantage;
    }

    public String getAdvantage()
    {
        return advantage;
    }
    public void setResumeUrl(String resumeUrl)
    {
        this.resumeUrl = resumeUrl;
    }

    public String getResumeUrl()
    {
        return resumeUrl;
    }
    public void setDefaultFlag(String defaultFlag)
    {
        this.defaultFlag = defaultFlag;
    }

    public String getDefaultFlag()
    {
        return defaultFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("title", getTitle())
            .append("type", getType())
            .append("educationList", getEducationList())
            .append("advantage", getAdvantage())
            .append("resumeUrl", getResumeUrl())
            .append("defaultFlag", getDefaultFlag())
            .append("createTime", getCreateTime())
            .append("createBy", getCreateBy())
            .toString();
    }
}
