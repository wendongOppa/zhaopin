package com.medicinezp.baseconfig.service;

import java.util.List;
import com.medicinezp.baseconfig.domain.BaseJobType;

/**
 * 职位分类Service接口
 *
 * @author medicinezp
 * @date 2022-09-22
 */
public interface IBaseJobTypeService
{
    /**
     * 查询职位分类
     *
     * @param id 职位分类主键
     * @return 职位分类
     */
    public BaseJobType selectBaseJobTypeById(Long id);

    /**
     * 查询职位分类列表
     *
     * @param baseJobType 职位分类
     * @return 职位分类集合
     */
    public List<BaseJobType> selectBaseJobTypeList(BaseJobType baseJobType);

    /**
     * 新增职位分类
     *
     * @param baseJobType 职位分类
     * @return 结果
     */
    public int insertBaseJobType(BaseJobType baseJobType);

    /**
     * 修改职位分类
     *
     * @param baseJobType 职位分类
     * @return 结果
     */
    public int updateBaseJobType(BaseJobType baseJobType);

    /**
     * 批量删除职位分类
     *
     * @param ids 需要删除的职位分类主键集合
     * @return 结果
     */
    public int deleteBaseJobTypeByIds(Long[] ids);

    /**
     * 删除职位分类信息
     *
     * @param id 职位分类主键
     * @return 结果
     */
    public int deleteBaseJobTypeById(Long id);

    List<BaseJobType> selectJobTypeListByCategroy(BaseJobType baseJobType);

    List<BaseJobType> selectCollectJobTypeList(BaseJobType jobType);
}
