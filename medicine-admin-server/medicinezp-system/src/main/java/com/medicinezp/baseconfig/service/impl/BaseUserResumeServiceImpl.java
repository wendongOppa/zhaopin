package com.medicinezp.baseconfig.service.impl;

import java.io.InputStream;
import java.util.List;

import cn.hutool.core.codec.Base64;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import com.medicinezp.baseconfig.domain.*;
import com.medicinezp.baseconfig.mapper.*;
import com.medicinezp.common.core.domain.entity.SysUser;
import com.medicinezp.common.utils.DateUtils;
import com.medicinezp.common.utils.SecurityUtils;
import com.medicinezp.common.utils.StringUtils;
import com.medicinezp.system.mapper.SysUserMapper;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;
import com.medicinezp.baseconfig.service.IBaseUserResumeService;

/**
 * 求职简历Service业务层处理
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@Service
public class BaseUserResumeServiceImpl implements IBaseUserResumeService
{
    @Autowired
    private BaseUserResumeMapper baseUserResumeMapper;

    @Autowired
    private SysUserMapper userMapper;

    @Autowired
    private BaseUserResumeCertificateMapper baseUserResumeCertificateMapper;

    @Autowired
    private BaseUserResumeEducationMapper baseUserResumeEducationMapper;
    @Autowired
    private BaseUserResumeExperienceMapper baseUserResumeExperienceMapper;
    @Autowired
    private BaseUserResumeProjectMapper baseUserResumeProjectMapper;

    @Autowired
    private BaseUserIntentionMapper baseUserIntentionMapper;


    /**
     * 查询求职简历
     *
     * @param id 求职简历主键
     * @return 求职简历
     */
    @Override
    public BaseUserResume selectBaseUserResumeById(Long id)
    {
        return baseUserResumeMapper.selectBaseUserResumeById(id);
    }

    /**
     * 查询求职简历列表
     *
     * @param baseUserResume 求职简历
     * @return 求职简历
     */
    @Override
    public List<BaseUserResume> selectBaseUserResumeList(BaseUserResume baseUserResume)
    {
        return baseUserResumeMapper.selectBaseUserResumeList(baseUserResume);
    }

    /**
     * 新增求职简历
     *
     * @param baseUserResume 求职简历
     * @return 结果
     */
    @Override
    public int insertBaseUserResume(BaseUserResume baseUserResume)
    {
        baseUserResume.setCreateTime(DateUtils.getNowDate());
        baseUserResume.setCreateBy(SecurityUtils.getUsername());
        return baseUserResumeMapper.insertBaseUserResume(baseUserResume);
    }

    /**
     * 修改求职简历
     *
     * @param baseUserResume 求职简历
     * @return 结果
     */
    @Override
    public int updateBaseUserResume(BaseUserResume baseUserResume)
    {
        SysUser user=new SysUser();
        user.setUserId(baseUserResume.getUserId());
        user.setAvatar(baseUserResume.getAvatar());
        user.setUserName(baseUserResume.getUserName());
        user.setSex(baseUserResume.getSex());
        user.setBirthDate(baseUserResume.getBirthDate());
        userMapper.updateUser(user);
        return baseUserResumeMapper.updateBaseUserResume(baseUserResume);
    }

    /**
     * 批量删除求职简历
     *
     * @param ids 需要删除的求职简历主键
     * @return 结果
     */
    @Override
    public int deleteBaseUserResumeByIds(Long[] ids)
    {
        return baseUserResumeMapper.deleteBaseUserResumeByIds(ids);
    }

    /**
     * 删除求职简历信息
     *
     * @param id 求职简历主键
     * @return 结果
     */
    @Override
    public int deleteBaseUserResumeById(Long id)
    {
        return baseUserResumeMapper.deleteBaseUserResumeById(id);
    }

    @Override
    public int cancelBaseUserResumeDefault(BaseUserResume baseUserResume) {
        baseUserResume.setUpdateTime(DateUtils.getNowDate());
        return baseUserResumeMapper.cancelBaseUserResumeDefault(baseUserResume);
    }

    @Override
    public BaseUserResume getUserResume(Long userId) {
        BaseUserResume param=new BaseUserResume();
        param.setUserId(userId);
        param.setType(1);
        BaseUserResume resume=new BaseUserResume();
        List<BaseUserResume>  resumeList=selectBaseUserResumeList(param);
        if(resumeList.size()>0){
            resume=resumeList.get(0);
            SysUser sysUser= userMapper.selectUserById(resume.getUserId());
            resume.setAvatar(sysUser.getAvatar());
            resume.setPhonenumber(sysUser.getPhonenumber());
            resume.setUserName(sysUser.getUserName());
            resume.setSex(sysUser.getSex());
            resume.setBirthDate(sysUser.getBirthDate());
            if(sysUser.getBirthDate()!=null){
                int age= DateUtils.differentDaysByMillisecond(sysUser.getBirthDate(),DateUtils.getNowDate())/365;
                resume.setAge(age);
            }
            BaseUserResumeEducation eduParam=new BaseUserResumeEducation();
            eduParam.setResumeId(Long.valueOf(resume.getId()));
            List<BaseUserResumeEducation>  eduList=baseUserResumeEducationMapper.selectBaseUserResumeEducationList(eduParam);
            resume.setEducationList(eduList);

            BaseUserResumeExperience exparam=new BaseUserResumeExperience();
            exparam.setResumeId(Long.valueOf(resume.getId()));
            List<BaseUserResumeExperience> explist = baseUserResumeExperienceMapper.selectBaseUserResumeExperienceList(exparam);
            resume.setExperienceList(explist);

            BaseUserResumeCertificate certParam=new BaseUserResumeCertificate();
            certParam.setResumeId(Long.valueOf(resume.getId()));
            List<BaseUserResumeCertificate> certlist = baseUserResumeCertificateMapper.selectBaseUserResumeCertificateList(certParam);
            resume.setCertList(certlist);

            BaseUserResumeProject proParam=new BaseUserResumeProject();
            proParam.setResumeId(Long.valueOf(resume.getId()));
            List<BaseUserResumeProject> proList = baseUserResumeProjectMapper.selectBaseUserResumeProjectList(proParam);
            resume.setProjectList(proList);

            BaseUserIntention intentionParam = new BaseUserIntention();
            intentionParam.setUserId(resume.getUserId());
            List<BaseUserIntention> intentionList = baseUserIntentionMapper.selectBaseUserIntentionList(intentionParam);
            resume.setIntentionList(intentionList);
        }
        return resume;
    }

    @Override
    public BaseUserResume selectBaseUserResumeDetail(Long resumeId) {
        BaseUserResume resume=selectBaseUserResumeById(resumeId);
        if(resume==null){
            return new BaseUserResume();
        }
        if (resume!=null && StringUtils.isNotEmpty(resume.getResumeUrl())) {
            HttpRequest get = HttpUtil.createGet(resume.getResumeUrl());
            HttpResponse execute = get.execute();
            InputStream inputStream = execute.bodyStream();
            String encode = Base64.encode(inputStream);
            resume.setPdfBase64("data:application/pdf;base64," + encode);

        }
        SysUser sysUser= userMapper.selectUserById(resume.getUserId());
        if(sysUser!=null) {
            resume.setAvatar(sysUser.getAvatar());
            resume.setUserName(sysUser.getUserName());
            resume.setSex(sysUser.getSex());
            resume.setBirthDate(sysUser.getBirthDate());
            if (sysUser.getBirthDate() != null) {
                int age = DateUtils.differentDaysByMillisecond(sysUser.getBirthDate(), DateUtils.getNowDate()) / 365;
                resume.setAge(age);
            }
        }
        if(resume.getType().intValue()==1) {
            BaseUserResumeEducation eduParam = new BaseUserResumeEducation();
            eduParam.setResumeId(Long.valueOf(resume.getId()));
            List<BaseUserResumeEducation> eduList = baseUserResumeEducationMapper.selectBaseUserResumeEducationList(eduParam);
            resume.setEducationList(eduList);

            BaseUserResumeExperience exparam = new BaseUserResumeExperience();
            exparam.setResumeId(Long.valueOf(resume.getId()));
            List<BaseUserResumeExperience> explist = baseUserResumeExperienceMapper.selectBaseUserResumeExperienceList(exparam);
            resume.setExperienceList(explist);

            BaseUserResumeCertificate certParam = new BaseUserResumeCertificate();
            certParam.setResumeId(Long.valueOf(resume.getId()));
            List<BaseUserResumeCertificate> certlist = baseUserResumeCertificateMapper.selectBaseUserResumeCertificateList(certParam);
            resume.setCertList(certlist);

            BaseUserResumeProject proParam = new BaseUserResumeProject();
            proParam.setResumeId(Long.valueOf(resume.getId()));
            List<BaseUserResumeProject> proList = baseUserResumeProjectMapper.selectBaseUserResumeProjectList(proParam);
            resume.setProjectList(proList);
        }

        BaseUserIntention intentionParam = new BaseUserIntention();
        intentionParam.setUserId(resume.getUserId());
        List<BaseUserIntention> intentionList = baseUserIntentionMapper.selectBaseUserIntentionList(intentionParam);
        resume.setIntentionList(intentionList);
        return resume;
    }
}
