package com.medicinezp.baseconfig.service.impl;

import java.util.List;
import com.medicinezp.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.medicinezp.baseconfig.mapper.BaseRegionDistrictMapper;
import com.medicinezp.baseconfig.domain.BaseRegionDistrict;
import com.medicinezp.baseconfig.service.IBaseRegionDistrictService;

/**
 * 区县信息Service业务层处理
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@Service
public class BaseRegionDistrictServiceImpl implements IBaseRegionDistrictService
{
    @Autowired
    private BaseRegionDistrictMapper baseRegionDistrictMapper;

    /**
     * 查询区县信息
     *
     * @param districtId 区县信息主键
     * @return 区县信息
     */
    @Override
    public BaseRegionDistrict selectBaseRegionDistrictByDistrictId(Long districtId)
    {
        return baseRegionDistrictMapper.selectBaseRegionDistrictByDistrictId(districtId);
    }

    /**
     * 查询区县信息列表
     *
     * @param baseRegionDistrict 区县信息
     * @return 区县信息
     */
    @Override
    public List<BaseRegionDistrict> selectBaseRegionDistrictList(BaseRegionDistrict baseRegionDistrict)
    {
        return baseRegionDistrictMapper.selectBaseRegionDistrictList(baseRegionDistrict);
    }

    /**
     * 新增区县信息
     *
     * @param baseRegionDistrict 区县信息
     * @return 结果
     */
    @Override
    public int insertBaseRegionDistrict(BaseRegionDistrict baseRegionDistrict)
    {
        baseRegionDistrict.setCreateTime(DateUtils.getNowDate());
        return baseRegionDistrictMapper.insertBaseRegionDistrict(baseRegionDistrict);
    }

    /**
     * 修改区县信息
     *
     * @param baseRegionDistrict 区县信息
     * @return 结果
     */
    @Override
    public int updateBaseRegionDistrict(BaseRegionDistrict baseRegionDistrict)
    {
        baseRegionDistrict.setUpdateTime(DateUtils.getNowDate());
        return baseRegionDistrictMapper.updateBaseRegionDistrict(baseRegionDistrict);
    }

    /**
     * 批量删除区县信息
     *
     * @param districtIds 需要删除的区县信息主键
     * @return 结果
     */
    @Override
    public int deleteBaseRegionDistrictByDistrictIds(Long[] districtIds)
    {
        return baseRegionDistrictMapper.deleteBaseRegionDistrictByDistrictIds(districtIds);
    }

    /**
     * 删除区县信息信息
     *
     * @param districtId 区县信息主键
     * @return 结果
     */
    @Override
    public int deleteBaseRegionDistrictByDistrictId(Long districtId)
    {
        return baseRegionDistrictMapper.deleteBaseRegionDistrictByDistrictId(districtId);
    }
}
