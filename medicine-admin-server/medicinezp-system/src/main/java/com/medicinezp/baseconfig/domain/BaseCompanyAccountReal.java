package com.medicinezp.baseconfig.domain;

import com.medicinezp.common.annotation.Excel;
import com.medicinezp.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 公司招聘人员对象 base_company_account_real
 *
 * @author medicinezp
 * @date 2022-09-22
 */
public class BaseCompanyAccountReal extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 公司ID */
    @Excel(name = "公司ID")
    private Long companyId;

    /** 用户ID */
    @Excel(name = "用户ID")
    private Long userId;

    /** 手机号 */
    @Excel(name = "手机号")
    private String userPhone;

    /** 姓名 */
    @Excel(name = "姓名")
    private String userName;

    /** 是否主要Y是N否 */
    @Excel(name = "是否主要Y是N否")
    private String isMain;

    /** 状态 */
    @Excel(name = "状态")
    private String status;

    private String companyName;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setCompanyId(Long companyId)
    {
        this.companyId = companyId;
    }

    public Long getCompanyId()
    {
        return companyId;
    }
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }
    public void setUserPhone(String userPhone)
    {
        this.userPhone = userPhone;
    }

    public String getUserPhone()
    {
        return userPhone;
    }
    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getUserName()
    {
        return userName;
    }
    public void setIsMain(String isMain)
    {
        this.isMain = isMain;
    }

    public String getIsMain()
    {
        return isMain;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("companyId", getCompanyId())
            .append("userId", getUserId())
            .append("userPhone", getUserPhone())
            .append("userName", getUserName())
            .append("remark", getRemark())
            .append("isMain", getIsMain())
            .append("status", getStatus())
            .append("createTime", getCreateTime())
            .append("createBy", getCreateBy())
            .toString();
    }
}
