package com.medicinezp.baseconfig.mapper;

import java.util.List;
import com.medicinezp.baseconfig.domain.BaseJobType;

/**
 * 职位分类Mapper接口
 *
 * @author medicinezp
 * @date 2022-09-22
 */
public interface BaseJobTypeMapper
{
    /**
     * 查询职位分类
     *
     * @param id 职位分类主键
     * @return 职位分类
     */
    public BaseJobType selectBaseJobTypeById(Long id);

    /**
     * 查询职位分类列表
     *
     * @param baseJobType 职位分类
     * @return 职位分类集合
     */
    public List<BaseJobType> selectBaseJobTypeList(BaseJobType baseJobType);

    /**
     * 新增职位分类
     *
     * @param baseJobType 职位分类
     * @return 结果
     */
    public int insertBaseJobType(BaseJobType baseJobType);

    /**
     * 修改职位分类
     *
     * @param baseJobType 职位分类
     * @return 结果
     */
    public int updateBaseJobType(BaseJobType baseJobType);

    /**
     * 删除职位分类
     *
     * @param id 职位分类主键
     * @return 结果
     */
    public int deleteBaseJobTypeById(Long id);

    /**
     * 批量删除职位分类
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseJobTypeByIds(Long[] ids);

    List<BaseJobType> selectJobTypeListByCategroy(BaseJobType baseJobType);

    List<BaseJobType> selectCollectJobTypeList(BaseJobType jobType);

    void updateBaseJobTypeCategory(BaseJobType jobType);
}
