package com.medicinezp.baseconfig.service.impl;

import java.util.List;
import com.medicinezp.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.medicinezp.baseconfig.mapper.BaseArticleMapper;
import com.medicinezp.baseconfig.domain.BaseArticle;
import com.medicinezp.baseconfig.service.IBaseArticleService;

/**
 * 资讯动态Service业务层处理
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@Service
public class BaseArticleServiceImpl implements IBaseArticleService
{
    @Autowired
    private BaseArticleMapper baseArticleMapper;

    /**
     * 查询资讯动态
     *
     * @param id 资讯动态主键
     * @return 资讯动态
     */
    @Override
    public BaseArticle selectBaseArticleById(Long id)
    {
        return baseArticleMapper.selectBaseArticleById(id);
    }

    /**
     * 查询资讯动态列表
     *
     * @param baseArticle 资讯动态
     * @return 资讯动态
     */
    @Override
    public List<BaseArticle> selectBaseArticleList(BaseArticle baseArticle)
    {
        return baseArticleMapper.selectBaseArticleList(baseArticle);
    }

    /**
     * 新增资讯动态
     *
     * @param baseArticle 资讯动态
     * @return 结果
     */
    @Override
    public int insertBaseArticle(BaseArticle baseArticle)
    {
        baseArticle.setCreateTime(DateUtils.getNowDate());
        return baseArticleMapper.insertBaseArticle(baseArticle);
    }

    /**
     * 修改资讯动态
     *
     * @param baseArticle 资讯动态
     * @return 结果
     */
    @Override
    public int updateBaseArticle(BaseArticle baseArticle)
    {
        baseArticle.setUpdateTime(DateUtils.getNowDate());
        return baseArticleMapper.updateBaseArticle(baseArticle);
    }

    /**
     * 批量删除资讯动态
     *
     * @param ids 需要删除的资讯动态主键
     * @return 结果
     */
    @Override
    public int deleteBaseArticleByIds(Long[] ids)
    {
        return baseArticleMapper.deleteBaseArticleByIds(ids);
    }

    /**
     * 删除资讯动态信息
     *
     * @param id 资讯动态主键
     * @return 结果
     */
    @Override
    public int deleteBaseArticleById(Long id)
    {
        return baseArticleMapper.deleteBaseArticleById(id);
    }
}
