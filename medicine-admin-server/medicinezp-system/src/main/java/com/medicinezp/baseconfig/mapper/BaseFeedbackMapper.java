package com.medicinezp.baseconfig.mapper;

import java.util.List;
import com.medicinezp.baseconfig.domain.BaseFeedback;

/**
 * 意见反馈Mapper接口
 * 
 * @author medicinezp
 * @date 2022-09-22
 */
public interface BaseFeedbackMapper 
{
    /**
     * 查询意见反馈
     * 
     * @param id 意见反馈主键
     * @return 意见反馈
     */
    public BaseFeedback selectBaseFeedbackById(Long id);

    /**
     * 查询意见反馈列表
     * 
     * @param baseFeedback 意见反馈
     * @return 意见反馈集合
     */
    public List<BaseFeedback> selectBaseFeedbackList(BaseFeedback baseFeedback);

    /**
     * 新增意见反馈
     * 
     * @param baseFeedback 意见反馈
     * @return 结果
     */
    public int insertBaseFeedback(BaseFeedback baseFeedback);

    /**
     * 修改意见反馈
     * 
     * @param baseFeedback 意见反馈
     * @return 结果
     */
    public int updateBaseFeedback(BaseFeedback baseFeedback);

    /**
     * 删除意见反馈
     * 
     * @param id 意见反馈主键
     * @return 结果
     */
    public int deleteBaseFeedbackById(Long id);

    /**
     * 批量删除意见反馈
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseFeedbackByIds(Long[] ids);
}
