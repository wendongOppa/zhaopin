package com.medicinezp.baseconfig.service.impl;

import java.util.List;
import com.medicinezp.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.medicinezp.baseconfig.mapper.BaseUserBrowserMapper;
import com.medicinezp.baseconfig.domain.BaseUserBrowser;
import com.medicinezp.baseconfig.service.IBaseUserBrowserService;

/**
 * 用户浏览记录Service业务层处理
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@Service
public class BaseUserBrowserServiceImpl implements IBaseUserBrowserService
{
    @Autowired
    private BaseUserBrowserMapper baseUserBrowserMapper;

    /**
     * 查询用户浏览记录
     *
     * @param id 用户浏览记录主键
     * @return 用户浏览记录
     */
    @Override
    public BaseUserBrowser selectBaseUserBrowserById(Long id)
    {
        return baseUserBrowserMapper.selectBaseUserBrowserById(id);
    }

    /**
     * 查询用户浏览记录列表
     *
     * @param baseUserBrowser 用户浏览记录
     * @return 用户浏览记录
     */
    @Override
    public List<BaseUserBrowser> selectBaseUserBrowserList(BaseUserBrowser baseUserBrowser)
    {
        return baseUserBrowserMapper.selectBaseUserBrowserList(baseUserBrowser);
    }

    /**
     * 新增用户浏览记录
     *
     * @param baseUserBrowser 用户浏览记录
     * @return 结果
     */
    @Override
    public int insertBaseUserBrowser(BaseUserBrowser baseUserBrowser)
    {
        baseUserBrowser.setCreateTime(DateUtils.getNowDate());
        return baseUserBrowserMapper.insertBaseUserBrowser(baseUserBrowser);
    }

    /**
     * 修改用户浏览记录
     *
     * @param baseUserBrowser 用户浏览记录
     * @return 结果
     */
    @Override
    public int updateBaseUserBrowser(BaseUserBrowser baseUserBrowser)
    {
        return baseUserBrowserMapper.updateBaseUserBrowser(baseUserBrowser);
    }

    /**
     * 批量删除用户浏览记录
     *
     * @param ids 需要删除的用户浏览记录主键
     * @return 结果
     */
    @Override
    public int deleteBaseUserBrowserByIds(Long[] ids)
    {
        return baseUserBrowserMapper.deleteBaseUserBrowserByIds(ids);
    }

    /**
     * 删除用户浏览记录信息
     *
     * @param id 用户浏览记录主键
     * @return 结果
     */
    @Override
    public int deleteBaseUserBrowserById(Long id)
    {
        return baseUserBrowserMapper.deleteBaseUserBrowserById(id);
    }
}
