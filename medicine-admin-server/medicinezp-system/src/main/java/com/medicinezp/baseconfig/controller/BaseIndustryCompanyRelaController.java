package com.medicinezp.baseconfig.controller;

import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletResponse;

import com.medicinezp.baseconfig.domain.BaseCompany;
import com.medicinezp.baseconfig.service.IBaseCompanyService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.medicinezp.common.annotation.Log;
import com.medicinezp.common.core.controller.BaseController;
import com.medicinezp.common.core.domain.AjaxResult;
import com.medicinezp.common.enums.BusinessType;
import com.medicinezp.baseconfig.domain.BaseIndustryCompanyRela;
import com.medicinezp.baseconfig.service.IBaseIndustryCompanyRelaService;
import com.medicinezp.common.utils.poi.ExcelUtil;
import com.medicinezp.common.core.page.TableDataInfo;

/**
 * 行业推荐企业Controller
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@RestController
@RequestMapping("/baseconfig/industrycompanyrela")
public class BaseIndustryCompanyRelaController extends BaseController
{
    @Autowired
    private IBaseIndustryCompanyRelaService baseIndustryCompanyRelaService;

    @Autowired
    private IBaseCompanyService baseCompanyService;
    /**
     * 查询行业推荐企业列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:industrycompanyrela:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseIndustryCompanyRela baseIndustryCompanyRela)
    {
        startPage();
        List<BaseIndustryCompanyRela> list = baseIndustryCompanyRelaService.selectBaseIndustryCompanyRelaList(baseIndustryCompanyRela);
        return getDataTable(list);
    }

    /**
     * 导出行业推荐企业列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:industrycompanyrela:export')")
    @Log(title = "行业推荐企业", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseIndustryCompanyRela baseIndustryCompanyRela)
    {
        List<BaseIndustryCompanyRela> list = baseIndustryCompanyRelaService.selectBaseIndustryCompanyRelaList(baseIndustryCompanyRela);
        ExcelUtil<BaseIndustryCompanyRela> util = new ExcelUtil<BaseIndustryCompanyRela>(BaseIndustryCompanyRela.class);
        util.exportExcel(response, list, "行业推荐企业数据");
    }

    /**
     * 获取行业推荐企业详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:industrycompanyrela:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(baseIndustryCompanyRelaService.selectBaseIndustryCompanyRelaById(id));
    }

    /**
     * 新增行业推荐企业
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:industrycompanyrela:add')")
    @Log(title = "行业推荐企业", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseIndustryCompanyRela baseIndustryCompanyRela)
    {
        if(baseIndustryCompanyRela.getIndustryId()==null){
            return AjaxResult.error("请选择行业");
        }
        BaseCompany company= baseCompanyService.selectBaseCompanyById(baseIndustryCompanyRela.getCompanyId());
        if(company==null){
            return AjaxResult.error("请选择推荐企业");
        }
        BaseIndustryCompanyRela param=new BaseIndustryCompanyRela();
        param.setIndustryId(baseIndustryCompanyRela.getIndustryId());
        List<BaseIndustryCompanyRela> list=baseIndustryCompanyRelaService.selectBaseIndustryCompanyRelaList(param);
        baseIndustryCompanyRela.setCompanyName(company.getCompanyName());
        baseIndustryCompanyRela.setSortNo(Long.valueOf(list.size()+1));
        List<BaseIndustryCompanyRela> companyList= list.stream().filter(c->c.getCompanyId().intValue()==baseIndustryCompanyRela.getCompanyId().intValue()).collect(Collectors.toList());
        if(companyList.size()>0){
            return AjaxResult.error("公司已经添加");
        }
        return toAjax(baseIndustryCompanyRelaService.insertBaseIndustryCompanyRela(baseIndustryCompanyRela));
    }

    /**
     * 修改行业推荐企业
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:industrycompanyrela:edit')")
    @Log(title = "行业推荐企业", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseIndustryCompanyRela baseIndustryCompanyRela)
    {
        return toAjax(baseIndustryCompanyRelaService.updateBaseIndustryCompanyRela(baseIndustryCompanyRela));
    }

    /**
     * 删除行业推荐企业
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:industrycompanyrela:remove')")
    @Log(title = "行业推荐企业", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseIndustryCompanyRelaService.deleteBaseIndustryCompanyRelaByIds(ids));
    }
}
