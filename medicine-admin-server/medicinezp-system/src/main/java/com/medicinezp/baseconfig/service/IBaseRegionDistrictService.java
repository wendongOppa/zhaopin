package com.medicinezp.baseconfig.service;

import java.util.List;
import com.medicinezp.baseconfig.domain.BaseRegionDistrict;

/**
 * 区县信息Service接口
 * 
 * @author medicinezp
 * @date 2022-09-22
 */
public interface IBaseRegionDistrictService 
{
    /**
     * 查询区县信息
     * 
     * @param districtId 区县信息主键
     * @return 区县信息
     */
    public BaseRegionDistrict selectBaseRegionDistrictByDistrictId(Long districtId);

    /**
     * 查询区县信息列表
     * 
     * @param baseRegionDistrict 区县信息
     * @return 区县信息集合
     */
    public List<BaseRegionDistrict> selectBaseRegionDistrictList(BaseRegionDistrict baseRegionDistrict);

    /**
     * 新增区县信息
     * 
     * @param baseRegionDistrict 区县信息
     * @return 结果
     */
    public int insertBaseRegionDistrict(BaseRegionDistrict baseRegionDistrict);

    /**
     * 修改区县信息
     * 
     * @param baseRegionDistrict 区县信息
     * @return 结果
     */
    public int updateBaseRegionDistrict(BaseRegionDistrict baseRegionDistrict);

    /**
     * 批量删除区县信息
     * 
     * @param districtIds 需要删除的区县信息主键集合
     * @return 结果
     */
    public int deleteBaseRegionDistrictByDistrictIds(Long[] districtIds);

    /**
     * 删除区县信息信息
     * 
     * @param districtId 区县信息主键
     * @return 结果
     */
    public int deleteBaseRegionDistrictByDistrictId(Long districtId);
}
