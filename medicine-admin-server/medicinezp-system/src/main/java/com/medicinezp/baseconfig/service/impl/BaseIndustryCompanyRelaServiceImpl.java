package com.medicinezp.baseconfig.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.medicinezp.baseconfig.mapper.BaseIndustryCompanyRelaMapper;
import com.medicinezp.baseconfig.domain.BaseIndustryCompanyRela;
import com.medicinezp.baseconfig.service.IBaseIndustryCompanyRelaService;

/**
 * 行业推荐企业Service业务层处理
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@Service
public class BaseIndustryCompanyRelaServiceImpl implements IBaseIndustryCompanyRelaService
{
    @Autowired
    private BaseIndustryCompanyRelaMapper baseIndustryCompanyRelaMapper;

    /**
     * 查询行业推荐企业
     *
     * @param id 行业推荐企业主键
     * @return 行业推荐企业
     */
    @Override
    public BaseIndustryCompanyRela selectBaseIndustryCompanyRelaById(Long id)
    {
        return baseIndustryCompanyRelaMapper.selectBaseIndustryCompanyRelaById(id);
    }

    /**
     * 查询行业推荐企业列表
     *
     * @param baseIndustryCompanyRela 行业推荐企业
     * @return 行业推荐企业
     */
    @Override
    public List<BaseIndustryCompanyRela> selectBaseIndustryCompanyRelaList(BaseIndustryCompanyRela baseIndustryCompanyRela)
    {
        return baseIndustryCompanyRelaMapper.selectBaseIndustryCompanyRelaList(baseIndustryCompanyRela);
    }

    /**
     * 新增行业推荐企业
     *
     * @param baseIndustryCompanyRela 行业推荐企业
     * @return 结果
     */
    @Override
    public int insertBaseIndustryCompanyRela(BaseIndustryCompanyRela baseIndustryCompanyRela)
    {
        return baseIndustryCompanyRelaMapper.insertBaseIndustryCompanyRela(baseIndustryCompanyRela);
    }

    /**
     * 修改行业推荐企业
     *
     * @param baseIndustryCompanyRela 行业推荐企业
     * @return 结果
     */
    @Override
    public int updateBaseIndustryCompanyRela(BaseIndustryCompanyRela baseIndustryCompanyRela)
    {
        return baseIndustryCompanyRelaMapper.updateBaseIndustryCompanyRela(baseIndustryCompanyRela);
    }

    /**
     * 批量删除行业推荐企业
     *
     * @param ids 需要删除的行业推荐企业主键
     * @return 结果
     */
    @Override
    public int deleteBaseIndustryCompanyRelaByIds(Long[] ids)
    {
        return baseIndustryCompanyRelaMapper.deleteBaseIndustryCompanyRelaByIds(ids);
    }

    /**
     * 删除行业推荐企业信息
     *
     * @param id 行业推荐企业主键
     * @return 结果
     */
    @Override
    public int deleteBaseIndustryCompanyRelaById(Long id)
    {
        return baseIndustryCompanyRelaMapper.deleteBaseIndustryCompanyRelaById(id);
    }
}
