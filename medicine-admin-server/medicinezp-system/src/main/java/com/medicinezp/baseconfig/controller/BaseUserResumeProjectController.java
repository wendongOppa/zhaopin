package com.medicinezp.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.medicinezp.common.annotation.Log;
import com.medicinezp.common.core.controller.BaseController;
import com.medicinezp.common.core.domain.AjaxResult;
import com.medicinezp.common.enums.BusinessType;
import com.medicinezp.baseconfig.domain.BaseUserResumeProject;
import com.medicinezp.baseconfig.service.IBaseUserResumeProjectService;
import com.medicinezp.common.utils.poi.ExcelUtil;
import com.medicinezp.common.core.page.TableDataInfo;

/**
 * 项目经验Controller
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@RestController
@RequestMapping("/baseconfig/project")
public class BaseUserResumeProjectController extends BaseController
{
    @Autowired
    private IBaseUserResumeProjectService baseUserResumeProjectService;

    /**
     * 查询项目经验列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:project:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseUserResumeProject baseUserResumeProject)
    {
        startPage();
        List<BaseUserResumeProject> list = baseUserResumeProjectService.selectBaseUserResumeProjectList(baseUserResumeProject);
        return getDataTable(list);
    }

    /**
     * 导出项目经验列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:project:export')")
    @Log(title = "项目经验", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseUserResumeProject baseUserResumeProject)
    {
        List<BaseUserResumeProject> list = baseUserResumeProjectService.selectBaseUserResumeProjectList(baseUserResumeProject);
        ExcelUtil<BaseUserResumeProject> util = new ExcelUtil<BaseUserResumeProject>(BaseUserResumeProject.class);
        util.exportExcel(response, list, "项目经验数据");
    }

    /**
     * 获取项目经验详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:project:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(baseUserResumeProjectService.selectBaseUserResumeProjectById(id));
    }

    /**
     * 新增项目经验
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:project:add')")
    @Log(title = "项目经验", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseUserResumeProject baseUserResumeProject)
    {
        return toAjax(baseUserResumeProjectService.insertBaseUserResumeProject(baseUserResumeProject));
    }

    /**
     * 修改项目经验
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:project:edit')")
    @Log(title = "项目经验", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseUserResumeProject baseUserResumeProject)
    {
        return toAjax(baseUserResumeProjectService.updateBaseUserResumeProject(baseUserResumeProject));
    }

    /**
     * 删除项目经验
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:project:remove')")
    @Log(title = "项目经验", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseUserResumeProjectService.deleteBaseUserResumeProjectByIds(ids));
    }
}
