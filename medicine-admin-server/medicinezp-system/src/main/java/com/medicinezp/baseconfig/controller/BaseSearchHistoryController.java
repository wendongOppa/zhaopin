package com.medicinezp.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.medicinezp.common.annotation.Log;
import com.medicinezp.common.core.controller.BaseController;
import com.medicinezp.common.core.domain.AjaxResult;
import com.medicinezp.common.enums.BusinessType;
import com.medicinezp.baseconfig.domain.BaseSearchHistory;
import com.medicinezp.baseconfig.service.IBaseSearchHistoryService;
import com.medicinezp.common.utils.poi.ExcelUtil;
import com.medicinezp.common.core.page.TableDataInfo;

/**
 * 搜索记录Controller
 *
 * @author medicinezp
 * @date 2022-10-24
 */
@RestController
@RequestMapping("/baseconfig/searchhistory")
public class BaseSearchHistoryController extends BaseController
{
    @Autowired
    private IBaseSearchHistoryService baseSearchHistoryService;

    /**
     * 查询搜索记录列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:searchhistory:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseSearchHistory baseSearchHistory)
    {
        startPage();
        List<BaseSearchHistory> list = baseSearchHistoryService.selectBaseSearchHistoryList(baseSearchHistory);
        return getDataTable(list);
    }

    /**
     * 导出搜索记录列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:searchhistory:export')")
    @Log(title = "搜索记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseSearchHistory baseSearchHistory)
    {
        List<BaseSearchHistory> list = baseSearchHistoryService.selectBaseSearchHistoryList(baseSearchHistory);
        ExcelUtil<BaseSearchHistory> util = new ExcelUtil<BaseSearchHistory>(BaseSearchHistory.class);
        util.exportExcel(response, list, "搜索记录数据");
    }

    /**
     * 获取搜索记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:searchhistory:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(baseSearchHistoryService.selectBaseSearchHistoryById(id));
    }

    /**
     * 新增搜索记录
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:searchhistory:add')")
    @Log(title = "搜索记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseSearchHistory baseSearchHistory)
    {
        return toAjax(baseSearchHistoryService.insertBaseSearchHistory(baseSearchHistory));
    }

    /**
     * 修改搜索记录
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:searchhistory:edit')")
    @Log(title = "搜索记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseSearchHistory baseSearchHistory)
    {
        return toAjax(baseSearchHistoryService.updateBaseSearchHistory(baseSearchHistory));
    }

    /**
     * 删除搜索记录
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:searchhistory:remove')")
    @Log(title = "搜索记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseSearchHistoryService.deleteBaseSearchHistoryByIds(ids));
    }
}
