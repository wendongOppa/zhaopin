package com.medicinezp.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.medicinezp.common.annotation.Log;
import com.medicinezp.common.core.controller.BaseController;
import com.medicinezp.common.core.domain.AjaxResult;
import com.medicinezp.common.enums.BusinessType;
import com.medicinezp.baseconfig.domain.BaseRangeConfig;
import com.medicinezp.baseconfig.service.IBaseRangeConfigService;
import com.medicinezp.common.utils.poi.ExcelUtil;
import com.medicinezp.common.core.page.TableDataInfo;

/**
 * 区间设置Controller
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@RestController
@RequestMapping("/baseconfig/rangeconfig")
public class BaseRangeConfigController extends BaseController
{
    @Autowired
    private IBaseRangeConfigService baseRangeConfigService;

    /**
     * 查询区间设置列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:rangeconfig:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseRangeConfig baseRangeConfig)
    {
        startPage();
        List<BaseRangeConfig> list = baseRangeConfigService.selectBaseRangeConfigList(baseRangeConfig);
        return getDataTable(list);
    }

    /**
     * 导出区间设置列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:rangeconfig:export')")
    @Log(title = "区间设置", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseRangeConfig baseRangeConfig)
    {
        List<BaseRangeConfig> list = baseRangeConfigService.selectBaseRangeConfigList(baseRangeConfig);
        ExcelUtil<BaseRangeConfig> util = new ExcelUtil<BaseRangeConfig>(BaseRangeConfig.class);
        util.exportExcel(response, list, "区间设置数据");
    }

    /**
     * 获取区间设置详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:rangeconfig:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(baseRangeConfigService.selectBaseRangeConfigById(id));
    }

    /**
     * 新增区间设置
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:rangeconfig:add')")
    @Log(title = "区间设置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseRangeConfig baseRangeConfig)
    {
        return toAjax(baseRangeConfigService.insertBaseRangeConfig(baseRangeConfig));
    }

    /**
     * 修改区间设置
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:rangeconfig:edit')")
    @Log(title = "区间设置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseRangeConfig baseRangeConfig)
    {
        return toAjax(baseRangeConfigService.updateBaseRangeConfig(baseRangeConfig));
    }

    /**
     * 删除区间设置
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:rangeconfig:remove')")
    @Log(title = "区间设置", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseRangeConfigService.deleteBaseRangeConfigByIds(ids));
    }
}
