package com.medicinezp.baseconfig.service;

import java.util.List;
import com.medicinezp.baseconfig.domain.BaseUserResumeProject;

/**
 * 项目经验Service接口
 * 
 * @author medicinezp
 * @date 2022-09-22
 */
public interface IBaseUserResumeProjectService 
{
    /**
     * 查询项目经验
     * 
     * @param id 项目经验主键
     * @return 项目经验
     */
    public BaseUserResumeProject selectBaseUserResumeProjectById(Long id);

    /**
     * 查询项目经验列表
     * 
     * @param baseUserResumeProject 项目经验
     * @return 项目经验集合
     */
    public List<BaseUserResumeProject> selectBaseUserResumeProjectList(BaseUserResumeProject baseUserResumeProject);

    /**
     * 新增项目经验
     * 
     * @param baseUserResumeProject 项目经验
     * @return 结果
     */
    public int insertBaseUserResumeProject(BaseUserResumeProject baseUserResumeProject);

    /**
     * 修改项目经验
     * 
     * @param baseUserResumeProject 项目经验
     * @return 结果
     */
    public int updateBaseUserResumeProject(BaseUserResumeProject baseUserResumeProject);

    /**
     * 批量删除项目经验
     * 
     * @param ids 需要删除的项目经验主键集合
     * @return 结果
     */
    public int deleteBaseUserResumeProjectByIds(Long[] ids);

    /**
     * 删除项目经验信息
     * 
     * @param id 项目经验主键
     * @return 结果
     */
    public int deleteBaseUserResumeProjectById(Long id);
}
