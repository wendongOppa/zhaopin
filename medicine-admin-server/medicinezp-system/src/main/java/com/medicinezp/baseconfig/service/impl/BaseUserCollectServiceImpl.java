package com.medicinezp.baseconfig.service.impl;

import java.util.List;
import com.medicinezp.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.medicinezp.baseconfig.mapper.BaseUserCollectMapper;
import com.medicinezp.baseconfig.domain.BaseUserCollect;
import com.medicinezp.baseconfig.service.IBaseUserCollectService;

/**
 * 用户收藏Service业务层处理
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@Service
public class BaseUserCollectServiceImpl implements IBaseUserCollectService
{
    @Autowired
    private BaseUserCollectMapper baseUserCollectMapper;

    /**
     * 查询用户收藏
     *
     * @param id 用户收藏主键
     * @return 用户收藏
     */
    @Override
    public BaseUserCollect selectBaseUserCollectById(Long id)
    {
        return baseUserCollectMapper.selectBaseUserCollectById(id);
    }

    /**
     * 查询用户收藏列表
     *
     * @param baseUserCollect 用户收藏
     * @return 用户收藏
     */
    @Override
    public List<BaseUserCollect> selectBaseUserCollectList(BaseUserCollect baseUserCollect)
    {
        return baseUserCollectMapper.selectBaseUserCollectList(baseUserCollect);
    }

    /**
     * 新增用户收藏
     *
     * @param baseUserCollect 用户收藏
     * @return 结果
     */
    @Override
    public int insertBaseUserCollect(BaseUserCollect baseUserCollect)
    {
        baseUserCollect.setCreateTime(DateUtils.getNowDate());
        return baseUserCollectMapper.insertBaseUserCollect(baseUserCollect);
    }

    /**
     * 修改用户收藏
     *
     * @param baseUserCollect 用户收藏
     * @return 结果
     */
    @Override
    public int updateBaseUserCollect(BaseUserCollect baseUserCollect)
    {
        return baseUserCollectMapper.updateBaseUserCollect(baseUserCollect);
    }

    /**
     * 批量删除用户收藏
     *
     * @param ids 需要删除的用户收藏主键
     * @return 结果
     */
    @Override
    public int deleteBaseUserCollectByIds(Long[] ids)
    {
        return baseUserCollectMapper.deleteBaseUserCollectByIds(ids);
    }

    /**
     * 删除用户收藏信息
     *
     * @param id 用户收藏主键
     * @return 结果
     */
    @Override
    public int deleteBaseUserCollectById(Long id)
    {
        return baseUserCollectMapper.deleteBaseUserCollectById(id);
    }
}
