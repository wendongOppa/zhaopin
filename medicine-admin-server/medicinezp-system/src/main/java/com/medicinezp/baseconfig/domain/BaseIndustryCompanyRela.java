package com.medicinezp.baseconfig.domain;

import com.medicinezp.common.annotation.Excel;
import com.medicinezp.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 行业推荐企业对象 base_industry_company_rela
 *
 * @author medicinezp
 * @date 2022-09-22
 */
public class BaseIndustryCompanyRela extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 行业ID */
    @Excel(name = "行业ID")
    private Long industryId;

    /** 公司ID */
    @Excel(name = "公司ID")
    private Long companyId;

    /** 公司名称 */
    @Excel(name = "公司名称")
    private String companyName;

    /** 排序编号 */
    @Excel(name = "排序编号")
    private Long sortNo;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setIndustryId(Long industryId)
    {
        this.industryId = industryId;
    }

    public Long getIndustryId()
    {
        return industryId;
    }
    public void setCompanyId(Long companyId)
    {
        this.companyId = companyId;
    }

    public Long getCompanyId()
    {
        return companyId;
    }
    public void setCompanyName(String companyName)
    {
        this.companyName = companyName;
    }

    public String getCompanyName()
    {
        return companyName;
    }
    public void setSortNo(Long sortNo)
    {
        this.sortNo = sortNo;
    }

    public Long getSortNo()
    {
        return sortNo;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("industryId", getIndustryId())
            .append("companyId", getCompanyId())
            .append("companyName", getCompanyName())
            .append("sortNo", getSortNo())
            .toString();
    }
}
