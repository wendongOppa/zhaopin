package com.medicinezp.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.medicinezp.common.core.domain.entity.SysUser;
import com.medicinezp.common.utils.SecurityUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.medicinezp.common.annotation.Log;
import com.medicinezp.common.core.controller.BaseController;
import com.medicinezp.common.core.domain.AjaxResult;
import com.medicinezp.common.enums.BusinessType;
import com.medicinezp.baseconfig.domain.BaseCompanyAddress;
import com.medicinezp.baseconfig.service.IBaseCompanyAddressService;
import com.medicinezp.common.utils.poi.ExcelUtil;
import com.medicinezp.common.core.page.TableDataInfo;

/**
 * 公司地址Controller
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@RestController
@RequestMapping("/baseconfig/address")
public class BaseCompanyAddressController extends BaseController
{
    @Autowired
    private IBaseCompanyAddressService baseCompanyAddressService;

    /**
     * 查询公司地址列表
     */
    @GetMapping("/list")
    public TableDataInfo list(BaseCompanyAddress baseCompanyAddress)
    {
        startPage();
        List<BaseCompanyAddress> list = baseCompanyAddressService.selectBaseCompanyAddressList(baseCompanyAddress);
        return getDataTable(list);
    }

    /**
     * 导出公司地址列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:address:export')")
    @Log(title = "公司地址", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseCompanyAddress baseCompanyAddress)
    {
        List<BaseCompanyAddress> list = baseCompanyAddressService.selectBaseCompanyAddressList(baseCompanyAddress);
        ExcelUtil<BaseCompanyAddress> util = new ExcelUtil<BaseCompanyAddress>(BaseCompanyAddress.class);
        util.exportExcel(response, list, "公司地址数据");
    }

    /**
     * 获取公司地址详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(baseCompanyAddressService.selectBaseCompanyAddressById(id));
    }

    /**
     * 新增公司地址
     */
    @Log(title = "公司地址", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseCompanyAddress baseCompanyAddress)
    {
        SysUser currentUser= SecurityUtils.getLoginUser().getUser();
        if(currentUser.getCurrentCompanyid()==null){
            return AjaxResult.error("当前用户没有分配公司");
        }
        baseCompanyAddress.setCompanyId(currentUser.getCurrentCompanyid());
        return toAjax(baseCompanyAddressService.insertBaseCompanyAddress(baseCompanyAddress));
    }

    /**
     * 修改公司地址
     */
    @Log(title = "公司地址", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseCompanyAddress baseCompanyAddress)
    {
        return toAjax(baseCompanyAddressService.updateBaseCompanyAddress(baseCompanyAddress));
    }

    /**
     * 删除公司地址
     */
    @Log(title = "公司地址", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseCompanyAddressService.deleteBaseCompanyAddressByIds(ids));
    }
}
