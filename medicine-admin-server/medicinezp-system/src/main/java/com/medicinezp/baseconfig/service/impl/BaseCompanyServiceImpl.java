package com.medicinezp.baseconfig.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.medicinezp.baseconfig.domain.BaseCompanyLogs;
import com.medicinezp.baseconfig.domain.BaseCompanyPhoto;
import com.medicinezp.baseconfig.domain.BaseJobLogs;
import com.medicinezp.baseconfig.mapper.BaseCompanyLogsMapper;
import com.medicinezp.baseconfig.mapper.BaseCompanyPhotoMapper;
import com.medicinezp.common.core.domain.entity.SysUser;
import com.medicinezp.common.utils.DateUtils;
import com.medicinezp.common.utils.SecurityUtils;
import com.medicinezp.common.utils.StringUtils;
import com.medicinezp.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.medicinezp.baseconfig.mapper.BaseCompanyMapper;
import com.medicinezp.baseconfig.domain.BaseCompany;
import com.medicinezp.baseconfig.service.IBaseCompanyService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 待审核公司Service业务层处理
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@Service
public class BaseCompanyServiceImpl implements IBaseCompanyService
{
    @Autowired
    private BaseCompanyMapper baseCompanyMapper;

    @Autowired
    private BaseCompanyLogsMapper baseCompanyLogsMapper;

    @Autowired
    private ISysUserService userService;

    @Autowired
    private BaseCompanyPhotoMapper baseCompanyPhotoMapper;
    /**
     * 查询待审核公司
     *
     * @param id 待审核公司主键
     * @return 待审核公司
     */
    @Override
    public BaseCompany selectBaseCompanyById(Long id)
    {
        return baseCompanyMapper.selectBaseCompanyById(id);
    }

    /**
     * 查询待审核公司列表
     *
     * @param baseCompany 待审核公司
     * @return 待审核公司
     */
    @Override
    public List<BaseCompany> selectBaseCompanyList(BaseCompany baseCompany)
    {
        return baseCompanyMapper.selectBaseCompanyList(baseCompany);
    }

    /**
     * 新增待审核公司
     *
     * @param baseCompany 待审核公司
     * @return 结果
     */
    @Override
    public int insertBaseCompany(BaseCompany baseCompany)
    {
        baseCompany.setCreateTime(DateUtils.getNowDate());
        int result= baseCompanyMapper.insertBaseCompany(baseCompany);
        if(result>0){
            BaseCompanyLogs log=new BaseCompanyLogs();
            log.setCompanyId(baseCompany.getId());
            log.setContent("公司入驻申请");
            log.setCreateTime(DateUtils.getNowDate());
            log.setCreateBy(baseCompany.getContactName());
            //log.setCreateAvatar(SecurityUtils.getLoginUser().getUser().getAvatar());
            baseCompanyLogsMapper.insertBaseCompanyLogs(log);
        }
        return  result;
    }

    /**
     * 修改待审核公司
     *
     * @param baseCompany 待审核公司
     * @return 结果
     */
    @Transactional
    @Override
    public int updateBaseCompany(BaseCompany baseCompany)
    {
        if(StringUtils.isNotEmpty(baseCompany.getCompanyPhotos())){
            baseCompanyPhotoMapper.deleteBaseCompanyPhotoByCompanyId(baseCompany.getId());
            String[] photos=baseCompany.getCompanyPhotos().split(",");
            for(String str:photos){
                BaseCompanyPhoto p=new BaseCompanyPhoto();
                p.setCompanyId(baseCompany.getId());
                p.setFileUrl(str);
                p.setFileName(str.substring(str.lastIndexOf("/")+1));
                p.setType(1L);
                baseCompanyPhotoMapper.insertBaseCompanyPhoto(p);
            }
        }
        int result= baseCompanyMapper.updateBaseCompany(baseCompany);
        if(result>0){
            BaseCompanyLogs log=new BaseCompanyLogs();
            log.setCompanyId(baseCompany.getId());
            log.setContent("修改公司信息");
            log.setCreateTime(DateUtils.getNowDate());
            log.setCreateBy(baseCompany.getContactName());
            log.setCreateAvatar("");
            baseCompanyLogsMapper.insertBaseCompanyLogs(log);
        }
        return  result;
    }


    @Override
    @Transactional
    public int approveBaseCompany(BaseCompany baseCompany) {
        baseCompany.setUpdateBy(SecurityUtils.getUsername());
        baseCompany.setUpdateTime(DateUtils.getNowDate());
        baseCompany.setAuditTime(DateUtils.getNowDate());
        int result= baseCompanyMapper.updateBaseCompany(baseCompany);
        if(result>0){
            BaseCompanyLogs log=new BaseCompanyLogs();
            log.setCompanyId(baseCompany.getId());
            log.setContent("审核"+("02".equals(baseCompany.getAuditStatus())?"通过":"不通过"));
            log.setRemark(baseCompany.getAuditRemark());
            log.setCreateTime(DateUtils.getNowDate());
            log.setCreateBy(SecurityUtils.getUsername());
            log.setCreateAvatar(SecurityUtils.getLoginUser().getUser().getAvatar());
            baseCompanyLogsMapper.insertBaseCompanyLogs(log);
        }
        BaseCompany company= baseCompanyMapper.selectBaseCompanyById(baseCompany.getId());
        if("02".equals(baseCompany.getAuditStatus())){
            userService.updateUserAuthEnsable(company.getId());
        }
        return  result;
    }

    @Override
    public List<BaseCompany> selectBaseCompanyExtistsList(BaseCompany param) {
        return baseCompanyMapper.selectBaseCompanyExtistsList(param);
    }

    /**
     * 批量删除待审核公司
     *
     * @param ids 需要删除的待审核公司主键
     * @return 结果
     */
    @Override
    public int deleteBaseCompanyByIds(Long[] ids)
    {
        return baseCompanyMapper.deleteBaseCompanyByIds(ids);
    }

    /**
     * 删除待审核公司信息
     *
     * @param id 待审核公司主键
     * @return 结果
     */
    @Override
    public int deleteBaseCompanyById(Long id)
    {
        return baseCompanyMapper.deleteBaseCompanyById(id);
    }

    @Override
    public List<BaseCompany> selectBrenchCompanyList(Long id) {
        return baseCompanyMapper.selectBrenchCompanyList(id);
    }

    @Override
    public List<BaseCompany> selectCompanyDataList(BaseCompany company) {
        if(company.getScaleLabels()!=null && company.getScaleLabels().size()>0 && company.getScaleLabels().contains("不限")){
            company.setScaleLabels(new ArrayList<>());
        }
        return baseCompanyMapper.selectCompanyDataList(company);
    }

    @Override
    public List<BaseCompany> selectCollectCompanyList(BaseCompany company) {
        return baseCompanyMapper.selectCollectCompanyList(company);
    }

    @Override
    public List<BaseCompany> selectBrenchmarkCompanyList(BaseCompany company) {
        return baseCompanyMapper.selectBrenchmarkCompanyList(company);
    }

    @Override
    public List<BaseCompany> selectAvailableCompanyList(String name) {
        return baseCompanyMapper.selectAvailableCompanyList(name);
    }


}
