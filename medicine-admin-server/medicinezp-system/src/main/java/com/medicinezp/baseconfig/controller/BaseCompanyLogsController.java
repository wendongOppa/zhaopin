package com.medicinezp.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.medicinezp.common.annotation.Log;
import com.medicinezp.common.core.controller.BaseController;
import com.medicinezp.common.core.domain.AjaxResult;
import com.medicinezp.common.enums.BusinessType;
import com.medicinezp.baseconfig.domain.BaseCompanyLogs;
import com.medicinezp.baseconfig.service.IBaseCompanyLogsService;
import com.medicinezp.common.utils.poi.ExcelUtil;
import com.medicinezp.common.core.page.TableDataInfo;

/**
 * 公司修改日志Controller
 *
 * @author medicinezp
 * @date 2022-10-14
 */
@RestController
@RequestMapping("/baseconfig/companylogs")
public class BaseCompanyLogsController extends BaseController
{
    @Autowired
    private IBaseCompanyLogsService baseCompanyLogsService;

    /**
     * 查询公司修改日志列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:companylogs:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseCompanyLogs baseCompanyLogs)
    {
        startPage();
        List<BaseCompanyLogs> list = baseCompanyLogsService.selectBaseCompanyLogsList(baseCompanyLogs);
        return getDataTable(list);
    }

    @GetMapping("/companyLogListById")
    public AjaxResult getCompanyLogsById(BaseCompanyLogs baseCompanyLogs)
    {
        List<BaseCompanyLogs> list = baseCompanyLogsService.selectBaseCompanyLogsList(baseCompanyLogs);
        return AjaxResult.success(list);
    }


    /**
     * 导出公司修改日志列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:companylogs:export')")
    @Log(title = "公司修改日志", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseCompanyLogs baseCompanyLogs)
    {
        List<BaseCompanyLogs> list = baseCompanyLogsService.selectBaseCompanyLogsList(baseCompanyLogs);
        ExcelUtil<BaseCompanyLogs> util = new ExcelUtil<BaseCompanyLogs>(BaseCompanyLogs.class);
        util.exportExcel(response, list, "公司修改日志数据");
    }

    /**
     * 获取公司修改日志详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:companylogs:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(baseCompanyLogsService.selectBaseCompanyLogsById(id));
    }

    /**
     * 新增公司修改日志
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:companylogs:add')")
    @Log(title = "公司修改日志", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseCompanyLogs baseCompanyLogs)
    {
        return toAjax(baseCompanyLogsService.insertBaseCompanyLogs(baseCompanyLogs));
    }

    /**
     * 修改公司修改日志
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:companylogs:edit')")
    @Log(title = "公司修改日志", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseCompanyLogs baseCompanyLogs)
    {
        return toAjax(baseCompanyLogsService.updateBaseCompanyLogs(baseCompanyLogs));
    }

    /**
     * 删除公司修改日志
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:companylogs:remove')")
    @Log(title = "公司修改日志", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseCompanyLogsService.deleteBaseCompanyLogsByIds(ids));
    }
}
