package com.medicinezp.baseconfig.service;

import java.util.List;
import com.medicinezp.baseconfig.domain.BaseJobLogs;

/**
 * 职位修改日志Service接口
 * 
 * @author medicinezp
 * @date 2022-10-13
 */
public interface IBaseJobLogsService 
{
    /**
     * 查询职位修改日志
     * 
     * @param id 职位修改日志主键
     * @return 职位修改日志
     */
    public BaseJobLogs selectBaseJobLogsById(Long id);

    /**
     * 查询职位修改日志列表
     * 
     * @param baseJobLogs 职位修改日志
     * @return 职位修改日志集合
     */
    public List<BaseJobLogs> selectBaseJobLogsList(BaseJobLogs baseJobLogs);

    /**
     * 新增职位修改日志
     * 
     * @param baseJobLogs 职位修改日志
     * @return 结果
     */
    public int insertBaseJobLogs(BaseJobLogs baseJobLogs);

    /**
     * 修改职位修改日志
     * 
     * @param baseJobLogs 职位修改日志
     * @return 结果
     */
    public int updateBaseJobLogs(BaseJobLogs baseJobLogs);

    /**
     * 批量删除职位修改日志
     * 
     * @param ids 需要删除的职位修改日志主键集合
     * @return 结果
     */
    public int deleteBaseJobLogsByIds(Long[] ids);

    /**
     * 删除职位修改日志信息
     * 
     * @param id 职位修改日志主键
     * @return 结果
     */
    public int deleteBaseJobLogsById(Long id);
}
