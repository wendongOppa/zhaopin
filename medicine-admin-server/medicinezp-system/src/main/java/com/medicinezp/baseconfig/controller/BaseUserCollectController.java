package com.medicinezp.baseconfig.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.medicinezp.common.annotation.Log;
import com.medicinezp.common.core.controller.BaseController;
import com.medicinezp.common.core.domain.AjaxResult;
import com.medicinezp.common.enums.BusinessType;
import com.medicinezp.baseconfig.domain.BaseUserCollect;
import com.medicinezp.baseconfig.service.IBaseUserCollectService;
import com.medicinezp.common.utils.poi.ExcelUtil;
import com.medicinezp.common.core.page.TableDataInfo;

/**
 * 用户收藏Controller
 *
 * @author medicinezp
 * @date 2022-09-22
 */
@RestController
@RequestMapping("/baseconfig/usercollect")
public class BaseUserCollectController extends BaseController
{
    @Autowired
    private IBaseUserCollectService baseUserCollectService;

    /**
     * 查询用户收藏列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:usercollect:list')")
    @GetMapping("/list")
    public TableDataInfo list(BaseUserCollect baseUserCollect)
    {
        startPage();
        List<BaseUserCollect> list = baseUserCollectService.selectBaseUserCollectList(baseUserCollect);
        return getDataTable(list);
    }

    /**
     * 导出用户收藏列表
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:usercollect:export')")
    @Log(title = "用户收藏", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BaseUserCollect baseUserCollect)
    {
        List<BaseUserCollect> list = baseUserCollectService.selectBaseUserCollectList(baseUserCollect);
        ExcelUtil<BaseUserCollect> util = new ExcelUtil<BaseUserCollect>(BaseUserCollect.class);
        util.exportExcel(response, list, "用户收藏数据");
    }

    /**
     * 获取用户收藏详细信息
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:usercollect:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(baseUserCollectService.selectBaseUserCollectById(id));
    }

    /**
     * 新增用户收藏
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:usercollect:add')")
    @Log(title = "用户收藏", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BaseUserCollect baseUserCollect)
    {
        return toAjax(baseUserCollectService.insertBaseUserCollect(baseUserCollect));
    }

    /**
     * 修改用户收藏
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:usercollect:edit')")
    @Log(title = "用户收藏", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BaseUserCollect baseUserCollect)
    {
        return toAjax(baseUserCollectService.updateBaseUserCollect(baseUserCollect));
    }

    /**
     * 删除用户收藏
     */
    @PreAuthorize("@ss.hasPermi('baseconfig:usercollect:remove')")
    @Log(title = "用户收藏", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(baseUserCollectService.deleteBaseUserCollectByIds(ids));
    }
}
