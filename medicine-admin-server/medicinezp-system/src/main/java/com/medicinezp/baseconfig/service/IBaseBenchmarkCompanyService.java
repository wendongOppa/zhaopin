package com.medicinezp.baseconfig.service;

import java.util.List;
import com.medicinezp.baseconfig.domain.BaseBenchmarkCompany;

/**
 * 标杆企业Service接口
 * 
 * @author medicinezp
 * @date 2022-09-22
 */
public interface IBaseBenchmarkCompanyService 
{
    /**
     * 查询标杆企业
     * 
     * @param id 标杆企业主键
     * @return 标杆企业
     */
    public BaseBenchmarkCompany selectBaseBenchmarkCompanyById(Long id);

    /**
     * 查询标杆企业列表
     * 
     * @param baseBenchmarkCompany 标杆企业
     * @return 标杆企业集合
     */
    public List<BaseBenchmarkCompany> selectBaseBenchmarkCompanyList(BaseBenchmarkCompany baseBenchmarkCompany);

    /**
     * 新增标杆企业
     * 
     * @param baseBenchmarkCompany 标杆企业
     * @return 结果
     */
    public int insertBaseBenchmarkCompany(BaseBenchmarkCompany baseBenchmarkCompany);

    /**
     * 修改标杆企业
     * 
     * @param baseBenchmarkCompany 标杆企业
     * @return 结果
     */
    public int updateBaseBenchmarkCompany(BaseBenchmarkCompany baseBenchmarkCompany);

    /**
     * 批量删除标杆企业
     * 
     * @param ids 需要删除的标杆企业主键集合
     * @return 结果
     */
    public int deleteBaseBenchmarkCompanyByIds(Long[] ids);

    /**
     * 删除标杆企业信息
     * 
     * @param id 标杆企业主键
     * @return 结果
     */
    public int deleteBaseBenchmarkCompanyById(Long id);
}
