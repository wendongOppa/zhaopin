package com.medicinezp.baseconfig.service;

import java.util.List;
import com.medicinezp.baseconfig.domain.BaseJobs;

/**
 * 岗位Service接口
 *
 * @author medicinezp
 * @date 2022-09-22
 */
public interface IBaseJobsService
{
    /**
     * 查询岗位
     *
     * @param id 岗位主键
     * @return 岗位
     */
    public BaseJobs selectBaseJobsById(Long id);

    /**
     * 查询岗位列表
     *
     * @param baseJobs 岗位
     * @return 岗位集合
     */
    public List<BaseJobs> selectBaseJobsList(BaseJobs baseJobs);

    /**
     * 新增岗位
     *
     * @param baseJobs 岗位
     * @return 结果
     */
    public int insertBaseJobs(BaseJobs baseJobs);

    /**
     * 修改岗位
     *
     * @param baseJobs 岗位
     * @return 结果
     */
    public int updateBaseJobs(BaseJobs baseJobs);

    /**
     * 批量删除岗位
     *
     * @param ids 需要删除的岗位主键集合
     * @return 结果
     */
    public int deleteBaseJobsByIds(Long[] ids);

    /**
     * 删除岗位信息
     *
     * @param id 岗位主键
     * @return 结果
     */
    public int deleteBaseJobsById(Long id);

    List<BaseJobs> selectJobsData(BaseJobs jobParam);

    List<BaseJobs> selectQueryJobsList(BaseJobs jobVo);

    List<BaseJobs> selectUserCollectJobsList(BaseJobs param);

    List<BaseJobs> selectUserBrowserJobsList(BaseJobs param);

    List<BaseJobs> selectMyApplyJobsList(BaseJobs param);

    List<BaseJobs> selectMyInviteJobsList(BaseJobs param);

    List<BaseJobs> selectMyAllJobsList(BaseJobs param);

    List<BaseJobs> selectQueryNewJobsList(BaseJobs param);

    List<BaseJobs> selectMyCompanyJobsList(BaseJobs baseJobs);

    List<BaseJobs> selectBaseJobsCountList(BaseJobs baseJobs);

    List<BaseJobs> selectAllJobsList(BaseJobs baseJobs);

    int approveBaseJobs(BaseJobs baseJobs);

    void refreshBaseJobs(Long currentCompanyid);

    int updateBaseJobsInfo(BaseJobs job);
}
