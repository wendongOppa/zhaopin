package com.medicinezp.baseconfig.mapper;

import java.util.List;
import com.medicinezp.baseconfig.domain.BaseRegionProvince;

/**
 * 省份信息Mapper接口
 * 
 * @author medicinezp
 * @date 2022-09-22
 */
public interface BaseRegionProvinceMapper 
{
    /**
     * 查询省份信息
     * 
     * @param provinceId 省份信息主键
     * @return 省份信息
     */
    public BaseRegionProvince selectBaseRegionProvinceByProvinceId(Long provinceId);

    /**
     * 查询省份信息列表
     * 
     * @param baseRegionProvince 省份信息
     * @return 省份信息集合
     */
    public List<BaseRegionProvince> selectBaseRegionProvinceList(BaseRegionProvince baseRegionProvince);

    /**
     * 新增省份信息
     * 
     * @param baseRegionProvince 省份信息
     * @return 结果
     */
    public int insertBaseRegionProvince(BaseRegionProvince baseRegionProvince);

    /**
     * 修改省份信息
     * 
     * @param baseRegionProvince 省份信息
     * @return 结果
     */
    public int updateBaseRegionProvince(BaseRegionProvince baseRegionProvince);

    /**
     * 删除省份信息
     * 
     * @param provinceId 省份信息主键
     * @return 结果
     */
    public int deleteBaseRegionProvinceByProvinceId(Long provinceId);

    /**
     * 批量删除省份信息
     * 
     * @param provinceIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseRegionProvinceByProvinceIds(Long[] provinceIds);
}
