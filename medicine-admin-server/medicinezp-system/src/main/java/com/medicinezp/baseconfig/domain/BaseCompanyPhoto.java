package com.medicinezp.baseconfig.domain;

import com.medicinezp.common.annotation.Excel;
import com.medicinezp.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 公司相册对象 base_company_photo
 *
 * @author medicinezp
 * @date 2022-09-22
 */
public class BaseCompanyPhoto extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 公司ID */
    @Excel(name = "公司ID")
    private Long companyId;

    /** 文件地址 */
    @Excel(name = "文件地址")
    private String fileUrl;

    /** 文件名称 */
    @Excel(name = "文件名称")
    private String fileName;

    /** 类型1图片2视频 */
    @Excel(name = "类型1图片2视频")
    private Long type;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setCompanyId(Long companyId)
    {
        this.companyId = companyId;
    }

    public Long getCompanyId()
    {
        return companyId;
    }
    public void setFileUrl(String fileUrl)
    {
        this.fileUrl = fileUrl;
    }

    public String getFileUrl()
    {
        return fileUrl;
    }
    public void setFileName(String fileName)
    {
        this.fileName = fileName;
    }

    public String getFileName()
    {
        return fileName;
    }
    public void setType(Long type)
    {
        this.type = type;
    }

    public Long getType()
    {
        return type;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("companyId", getCompanyId())
            .append("fileUrl", getFileUrl())
            .append("fileName", getFileName())
            .append("type", getType())
            .toString();
    }
}
