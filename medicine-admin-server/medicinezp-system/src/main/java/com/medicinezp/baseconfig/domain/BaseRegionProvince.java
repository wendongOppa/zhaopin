package com.medicinezp.baseconfig.domain;

import com.medicinezp.common.annotation.Excel;
import com.medicinezp.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.List;

/**
 * 省份信息对象 base_region_province
 *
 * @author medicinezp
 * @date 2022-09-22
 */
public class BaseRegionProvince extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 省份ID */
    private Long provinceId;

    /** 省份名称 */
    @Excel(name = "省份名称")
    private String provinceName;

    private String label;
    private String value;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    private List<BaseRegionCity> children;

    public List<BaseRegionCity> getChildren() {
        return children;
    }

    public void setChildren(List<BaseRegionCity> children) {
        this.children = children;
    }

    public void setProvinceId(Long provinceId)
    {
        this.provinceId = provinceId;
    }

    public Long getProvinceId()
    {
        return provinceId;
    }
    public void setProvinceName(String provinceName)
    {
        this.provinceName = provinceName;
    }

    public String getProvinceName()
    {
        return provinceName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("provinceId", getProvinceId())
            .append("provinceName", getProvinceName())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
