package com.medicinezp.baseconfig.mapper;

import java.util.List;
import com.medicinezp.baseconfig.domain.BaseUserJobApply;

/**
 * 用户投递简历申请Mapper接口
 *
 * @author medicinezp
 * @date 2022-09-22
 */
public interface BaseUserJobApplyMapper
{
    /**
     * 查询用户投递简历申请
     *
     * @param id 用户投递简历申请主键
     * @return 用户投递简历申请
     */
    public BaseUserJobApply selectBaseUserJobApplyById(Long id);

    /**
     * 查询用户投递简历申请列表
     *
     * @param baseUserJobApply 用户投递简历申请
     * @return 用户投递简历申请集合
     */
    public List<BaseUserJobApply> selectBaseUserJobApplyList(BaseUserJobApply baseUserJobApply);

    /**
     * 新增用户投递简历申请
     *
     * @param baseUserJobApply 用户投递简历申请
     * @return 结果
     */
    public int insertBaseUserJobApply(BaseUserJobApply baseUserJobApply);

    /**
     * 修改用户投递简历申请
     *
     * @param baseUserJobApply 用户投递简历申请
     * @return 结果
     */
    public int updateBaseUserJobApply(BaseUserJobApply baseUserJobApply);

    /**
     * 删除用户投递简历申请
     *
     * @param id 用户投递简历申请主键
     * @return 结果
     */
    public int deleteBaseUserJobApplyById(Long id);

    /**
     * 批量删除用户投递简历申请
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseUserJobApplyByIds(Long[] ids);

    List<BaseUserJobApply> selectCompanyUserJobApplyList(BaseUserJobApply jobApply);

    List<BaseUserJobApply> selectAllUserJobApplyList(BaseUserJobApply baseUserJobApply);
}
