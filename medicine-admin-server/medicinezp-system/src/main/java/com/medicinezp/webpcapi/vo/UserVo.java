package com.medicinezp.webpcapi.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.medicinezp.common.annotation.Excel;
import com.medicinezp.common.annotation.Excel.ColumnType;
import com.medicinezp.common.annotation.Excel.Type;
import com.medicinezp.common.annotation.Excels;
import com.medicinezp.common.core.domain.BaseEntity;
import com.medicinezp.common.core.domain.entity.SysDept;
import com.medicinezp.common.core.domain.entity.SysRole;
import com.medicinezp.common.xss.Xss;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

/**
 * 用户对象 sys_user
 *
 * @author medicinezp
 */
public class UserVo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    private Long typeId;
    private Long jobId;

    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    private String name;

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
