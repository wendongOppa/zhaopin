package com.medicinezp.framework.web.service;

import javax.annotation.Resource;

import com.medicinezp.baseconfig.domain.BaseUserResume;
import com.medicinezp.baseconfig.mapper.BaseUserResumeMapper;
import com.medicinezp.common.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import com.medicinezp.common.constant.CacheConstants;
import com.medicinezp.common.constant.Constants;
import com.medicinezp.common.core.domain.entity.SysUser;
import com.medicinezp.common.core.domain.model.LoginUser;
import com.medicinezp.common.core.redis.RedisCache;
import com.medicinezp.common.exception.ServiceException;
import com.medicinezp.common.exception.user.CaptchaException;
import com.medicinezp.common.exception.user.CaptchaExpireException;
import com.medicinezp.common.exception.user.UserPasswordNotMatchException;
import com.medicinezp.common.utils.ip.IpUtils;
import com.medicinezp.framework.manager.AsyncManager;
import com.medicinezp.framework.manager.factory.AsyncFactory;
import com.medicinezp.framework.security.context.AuthenticationContextHolder;
import com.medicinezp.system.service.ISysConfigService;
import com.medicinezp.system.service.ISysUserService;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * 登录校验方法
 *
 * @author medicinezp
 */
@Component
public class SysLoginService
{
    @Autowired
    private TokenService tokenService;

    @Resource
    private AuthenticationManager authenticationManager;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private ISysUserService userService;

    @Autowired
    private ISysConfigService configService;

    @Autowired
    private BaseUserResumeMapper baseUserResumeMapper;
    /**
     * 登录验证
     *
     * @param username 用户名
     * @param password 密码
     * @param code 验证码
     * @param uuid 唯一标识
     * @return 结果
     */
    public String login(String username, String password, String code, String uuid)
    {
        boolean captchaEnabled = configService.selectCaptchaEnabled();
        // 验证码开关
        if (captchaEnabled)
        {
            validateCaptcha(username, code, uuid);
        }
        // 用户验证
        Authentication authentication = null;
        try
        {
            UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username, password);
            AuthenticationContextHolder.setContext(authenticationToken);
            // 该方法会去调用UserDetailsServiceImpl.loadUserByUsername
            authentication = authenticationManager.authenticate(authenticationToken);
        }
        catch (Exception e)
        {
            if (e instanceof BadCredentialsException)
            {
                AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.password.not.match")));
                throw new UserPasswordNotMatchException();
            }
            else
            {
                AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, e.getMessage()));
                throw new ServiceException(e.getMessage());
            }
        }
        finally
        {
            AuthenticationContextHolder.clearContext();
        }
        AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_SUCCESS, MessageUtils.message("user.login.success")));
        LoginUser loginUser = (LoginUser) authentication.getPrincipal();
        recordLoginInfo(loginUser.getUserId());
        // 生成token
        return tokenService.createToken(loginUser);
    }

    public String login(String username, String password, String uuid)
    {
        // 用户验证
        Authentication authentication = null;
        try
        {
            UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username, password);
            AuthenticationContextHolder.setContext(authenticationToken);
            // 该方法会去调用UserDetailsServiceImpl.loadUserByUsername
            authentication = authenticationManager.authenticate(authenticationToken);
        }
        catch (Exception e)
        {
            if (e instanceof BadCredentialsException)
            {
                AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.password.not.match")));
                throw new UserPasswordNotMatchException();
            }
            else
            {
                AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, e.getMessage()));
                throw new ServiceException(e.getMessage());
            }
        }
        finally
        {
            AuthenticationContextHolder.clearContext();
        }
        AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_SUCCESS, MessageUtils.message("user.login.success")));
        LoginUser loginUser = (LoginUser) authentication.getPrincipal();
        recordLoginInfo(loginUser.getUserId());
        // 生成token
        return tokenService.createToken(loginUser);
    }

    /**
     * 校验验证码
     *
     * @param username 用户名
     * @param code 验证码
     * @param uuid 唯一标识
     * @return 结果
     */
    public void validateCaptcha(String username, String code, String uuid)
    {
        String verifyKey = CacheConstants.CAPTCHA_CODE_KEY + StringUtils.nvl(uuid, "");
        String captcha = redisCache.getCacheObject(verifyKey);
        redisCache.deleteObject(verifyKey);
        if (captcha == null)
        {
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.jcaptcha.expire")));
            throw new CaptchaExpireException();
        }
        if (!code.equalsIgnoreCase(captcha))
        {
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.jcaptcha.error")));
            throw new CaptchaException();
        }
    }

    /**
     * 记录登录信息
     *
     * @param userId 用户ID
     */
    public void recordLoginInfo(Long userId)
    {
        SysUser sysUser = new SysUser();
        sysUser.setUserId(userId);
        sysUser.setLoginIp(IpUtils.getIpAddr(ServletUtils.getRequest()));
        sysUser.setLoginDate(DateUtils.getNowDate());
        userService.updateUserProfile(sysUser);
    }

    /**
     * 微信登录
     *
     * @param userInfo
     * @return
     */
    public Map<String, Object> loginWeiXin(Map<String, Object> userInfo) {
        Map<String, Object> result = new HashMap();
        String openid = String.valueOf(userInfo.get("openid"));
        String unionid = null;
        if (StringUtils.isNotNull(userInfo.get("unionid")) && !"null".equals(userInfo.get("unionid"))) {
            unionid = (String) userInfo.get("unionid");
        }
        String sessionKey = String.valueOf(userInfo.get("session_key"));
        String avatar = String.valueOf(userInfo.get("avatar"));
        String sex = String.valueOf(userInfo.get("sex"));
        String nickName = null;
        if (StringUtils.isNotNull(userInfo.get("nickName")) && !"null".equals(userInfo.get("nickName"))) {
            nickName = String.valueOf(userInfo.get("nickName"));
        }

        //根据openid查询用户
        SysUser user = userService.selectUserByOpenId(openid);
        if (StringUtils.isNull(user)) {
            //用户不存在 ，增加用户
            user = new SysUser();
            if(StringUtils.isEmpty(nickName)){
                user.setUserName("微信用户");
            }else{
                user.setUserName(nickName);
            }
            System.out.println("=========================");
            System.out.println(openid);
            System.out.println(nickName);
            user.setOpenId(openid);
            user.setUnionId(Optional.ofNullable(unionid).orElse(null));
            user.setNickName(nickName);
            user.setSex(sex);
            user.setAvatar(avatar);
            String password = configService.selectConfigByKey("sys.user.initPassword");
            user.setPassword(SecurityUtils.encryptPassword(password));
            //正常
            user.setStatus("0");
            user.setUserType("02");
            int rowNum=userService.insertUser(user);
            if(rowNum>0){
                BaseUserResume resume=new BaseUserResume();
                resume.setUserId(user.getUserId());
                resume.setTitle("在线简历");
                resume.setType(1);
                resume.setCreateTime(DateUtils.getNowDate());
                baseUserResumeMapper.insertBaseUserResume(resume);
            }
        } else {
            if (StringUtils.isEmpty(user.getSex())) {
                user.setSex(sex);
            }
            if (StringUtils.isEmpty(user.getNickName())) {
                user.setNickName(nickName);
            }
            if (StringUtils.isEmpty(user.getAvatar())) {
                user.setAvatar(avatar);
            }
            if (StringUtils.isEmpty(user.getUnionId()) || "null".equals(user.getUnionId())) {
                user.setUnionId(unionid);
            }
            userService.updateUser(user);
        }

        AsyncManager.me().execute(AsyncFactory.recordLogininfor(openid, Constants.LOGIN_SUCCESS, MessageUtils.message("user.login.success")));


        LoginUser loginUser = new LoginUser();
        loginUser.setUserId(user.getUserId());
        loginUser.setUser(user);
        recordLoginInfo(loginUser.getUserId());
        result.put(Constants.TOKEN, tokenService.createToken(loginUser));
        result.put("userInfo", loginUser.getUser());
        result.put("sessionKey", sessionKey);
        return result;
    }
}
