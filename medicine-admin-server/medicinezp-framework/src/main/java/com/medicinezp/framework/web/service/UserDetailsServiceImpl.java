package com.medicinezp.framework.web.service;

import com.medicinezp.baseconfig.domain.BaseCompanyAccountReal;
import com.medicinezp.baseconfig.service.IBaseCompanyAccountRealService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.medicinezp.common.core.domain.entity.SysUser;
import com.medicinezp.common.core.domain.model.LoginUser;
import com.medicinezp.common.enums.UserStatus;
import com.medicinezp.common.exception.ServiceException;
import com.medicinezp.common.utils.StringUtils;
import com.medicinezp.system.service.ISysUserService;

/**
 * 用户验证处理
 *
 * @author medicinezp
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService
{
    private static final Logger log = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

    @Autowired
    private ISysUserService userService;

    @Autowired
    private SysPasswordService passwordService;

    @Autowired
    private SysPermissionService permissionService;

    @Autowired
    private IBaseCompanyAccountRealService baseCompanyAccountRealService;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException
    {
        SysUser user = userService.selectUserByUserName(username);
        if (StringUtils.isNull(user))
        {
            log.info("登录用户：{} 不存在.", username);
            throw new ServiceException("登录用户：" + username + " 不存在");
        }
        else if (UserStatus.DELETED.getCode().equals(user.getDelFlag()))
        {
            log.info("登录用户：{} 已被删除.", username);
            throw new ServiceException("对不起，您的账号：" + username + " 已被删除");
        }
        else if (UserStatus.DISABLE.getCode().equals(user.getStatus()))
        {
            log.info("登录用户：{} 已被停用.", username);
            throw new ServiceException("对不起，您的账号：" + username + " 未被启用，请联系管理员");
        }

        BaseCompanyAccountReal account= baseCompanyAccountRealService.selectBaseCompanyAccountRealByUserId(user.getUserId());
        if(account!=null){
            user.setCurrentCompanyid(account.getCompanyId());
        }

        passwordService.validate(user);

        return createLoginUser(user);
    }

    public UserDetails createLoginUser(SysUser user)
    {
        return new LoginUser(user.getUserId(), user.getDeptId(), user, permissionService.getMenuPermission(user));
    }
}
