package com.medicinezp.common.core.domain.model;

/**
 * 用户注册对象
 *
 * @author medicinezp
 */
public class RegisterBody
{
    private static final long serialVersionUID = 1L;

    private String companyName;
    private String shortName;
    private String uniformSocialCreditCode;
    private String businessLicense;
    private String contactName;;
    private String contactPhone;
    private String evidentiaryFile;
    private String industry;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getUniformSocialCreditCode() {
        return uniformSocialCreditCode;
    }

    public void setUniformSocialCreditCode(String uniformSocialCreditCode) {
        this.uniformSocialCreditCode = uniformSocialCreditCode;
    }

    public String getBusinessLicense() {
        return businessLicense;
    }

    public void setBusinessLicense(String businessLicense) {
        this.businessLicense = businessLicense;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getEvidentiaryFile() {
        return evidentiaryFile;
    }

    public void setEvidentiaryFile(String evidentiaryFile) {
        this.evidentiaryFile = evidentiaryFile;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }
}
