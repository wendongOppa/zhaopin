package com.medicinezp.common.enums;

/**
 * 操作状态
 *
 * @author medicinezp
 *
 */
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
