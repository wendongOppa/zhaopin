package com.medicinezp.common.exception;

/**
 * 演示模式异常
 *
 * @author medicinezp
 */
public class DemoModeException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    public DemoModeException()
    {
    }
}
