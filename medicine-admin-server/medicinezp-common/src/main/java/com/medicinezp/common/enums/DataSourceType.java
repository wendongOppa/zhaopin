package com.medicinezp.common.enums;

/**
 * 数据源
 *
 * @author medicinezp
 */
public enum DataSourceType
{
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
