package com.medicinezp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * 启动程序
 *
 * @author medicinezp
 */
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
public class MedicinezpApplication
{
    public static void main(String[] args)
    {
        // System.setProperty("spring.devtools.restart.enabled", "false");
        SpringApplication.run(MedicinezpApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  小象招聘启动成功   ლ(´ڡ`ლ)ﾞ");
    }
}
