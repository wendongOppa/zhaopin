package com.medicinezp.api.vo;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * 微信登录
 * @author zl
 */
@Data
public class WxLoginVo {
    /**
     * code
     */
    @NotEmpty(message = "code is empty")
    private String code;
    /**
     * 昵称
     */
    private String nickName;
    /**
     * 性别
     */
    private String gender;
    /**
     * 头像
     */
    private String avatarUrl;
    private String city;
    private String country;
    private String province;

}
